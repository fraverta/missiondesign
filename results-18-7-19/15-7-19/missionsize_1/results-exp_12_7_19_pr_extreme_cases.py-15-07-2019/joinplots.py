import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.30,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.70,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.90,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.30,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.70,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.90,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.30,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.70,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.90,rep:35,seed:245.svg',

]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,50,100],easy_propotion[0.1,0.3,0.5,0.7,0.9],varying_offeredload'
combine_into_table_and_save(3,5,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,mission_num:100,mission_per_sat:10,easy_proportion0.90,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,mission_num:500,mission_per_sat:10,easy_proportion0.90,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,mission_num:1000,mission_per_sat:10,easy_proportion0.90,rep:35,seed:245.svg',

]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,50,100],easy_propotion[0.1,0.5,0.9],varying_offeredload'
combine_into_table_and_save(3,3,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))