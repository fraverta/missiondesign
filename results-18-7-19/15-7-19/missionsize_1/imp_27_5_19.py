from typing import Dict, List
import random
import networkx as nx
from abc import abstractmethod
from pulp import *
import time
from math import ceil, floor

class Scheme_27_5_19:

    def __init__(self, sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, mission_generator:'MissionGenerator'=None, seed=0):
        self._sat_num = sat_num
        self._mission_num = mission_num
        self._mission_per_sat = mission_per_sat
        self._pr_sat_to_do_mission = pr_sat_to_do_mission
        self._random = random.Random(seed)
        if mission_generator is None:
            self.__mission_generator = GenericMissionGenerator(sat_num, mission_num, pr_sat_to_do_mission, seed=seed)
        else:
            self.__mission_generator = mission_generator

    def _generate_scenario(self) -> Dict[(int, List[int])]:
        return self.__mission_generator.generate_scenario()

    @abstractmethod
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        pass

    @staticmethod
    @abstractmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float,number_of_runs, mission_generator:'MissionGenerator'=None, seed=0) -> List[int]:
        pass

    @staticmethod
    def _run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float,
                        number_of_runs, exp: 'Scheme_27_5_19') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario = exp._generate_scenario()
            result.append(exp._compute_mission_assignment(scenario)[0])

        return result


class Offline(Scheme_27_5_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        g = self._generate_bipartite_graph(scenario)
        assert nx.is_bipartite(g)
        matching = nx.bipartite.maximum_matching(g, {n for n, d in g.nodes(data=True) if d['bipartite']==0})
        mission_assigment = dict((sat, []) for sat in range(self._sat_num))
        for sat_str in list(matching.keys())[:len(matching.keys())//2]:
            mission_assigment[int(sat_str.split('_')[0])].append(matching[sat_str])

        elapsed_time = time.time() - start_time
        print("Offline:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-MissionsPerSat:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._mission_per_sat, self._pr_sat_to_do_mission))
        return len(matching)//2, mission_assigment

    def _generate_bipartite_graph(self, scenario: Dict[(int, List[int])]):
        bigraph = nx.Graph()
        bigraph.add_nodes_from(['%d_%d' % (n, m) for n in range(self._sat_num) for m in range(self._mission_per_sat)],
                               bipartite=0)
        bigraph.add_nodes_from([m for m in range(self._mission_num)], bipartite=1)

        for n in range(self._sat_num):
            for mission in scenario[n]:
                for m in range(self._mission_per_sat):
                    bigraph.add_edge('%d_%d' % (n, m), mission)

        return bigraph

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, number_of_runs:int, mission_generator:'MissionGenerator'=None, seed=0) -> List[int]:
        exp = Offline(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, mission_generator=mission_generator, seed=seed)
        return exp._run_experiment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, number_of_runs, exp)


class Online(Scheme_27_5_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, []) for sat in range(self._sat_num))
        assigned_missions = 0
        for m in range(self._mission_num):
            sat_ordered_by_busy_buffer = sorted([(sat, len(sat_assignment[sat])) for sat in range(self._sat_num)], key=lambda x: x[1])
            for sat, num_of_assigned_mission in sat_ordered_by_busy_buffer:
                if num_of_assigned_mission < self._mission_per_sat and m in scenario[sat]:
                    sat_assignment[sat].append(m)
                    assigned_missions += 1
                    break

        assert all(len(sat_assignment[k]) <= self._mission_per_sat for k in range(self._sat_num))
        elapsed_time = time.time() - start_time
        print("Online:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-MissionsPerSat:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._mission_per_sat, self._pr_sat_to_do_mission))
        return assigned_missions, sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, number_of_runs:int, mission_generator:'MissionGenerator' = None,  seed=0) -> List[int]:
        exp = Online(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, mission_generator=mission_generator, seed=seed)
        return exp._run_experiment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, number_of_runs, exp)


class Offline_LP(Scheme_27_5_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_mission_idx = ['Sat%d_M%d' % (sat, m) for sat in range(self._sat_num) for m in scenario[sat]]

        lp_problem = LpProblem("The Mission Problem", LpMaximize)
        sat_capacity_vars = LpVariable.dicts("S_Sat", list(range(self._sat_num)), 0, self._mission_per_sat, LpInteger)
        sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)
        mission_vars = LpVariable.dicts("M", list(range(self._mission_num)), cat=LpBinary)

        lp_problem += lpSum([mission_vars[m] for m in range(self._mission_num)]), "Total number of assigned missions"
        for sat in range(self._sat_num):
            lp_problem += sat_capacity_vars[sat] == lpSum([sat_mission_vars['Sat%d_M%d'%(sat, m)] for m in scenario[sat]]), 'Capacity assigned Sat %d equal to mission assigned'%(sat)
        for m in range(self._mission_num):
            sat_able_to_fulfill_m = [sat for sat in range(self._sat_num) if m in scenario[sat]]
            lp_problem += mission_vars[m] == lpSum([sat_mission_vars['Sat%d_M%d'%(sat, m)] for sat in sat_able_to_fulfill_m]), 'Capacity assigned equal to mission %d assigned'%(m)

        lp_problem.solve(CPLEX())
        assert lp_problem.status == LpStatusOptimal

        sat_assignment = dict((sat, []) for sat in range(self._sat_num))
        for v in sat_mission_vars.values():
            if v.varValue:
                sat = int(v.name[4: v.name.rindex('_')])
                mission = int(v.name[v.name.rindex('M') + 1:])
                sat_assignment[sat].append(mission)

        elapsed_time = time.time() - start_time
        print("Offline_LP:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-MissionsPerSat:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._mission_per_sat, self._pr_sat_to_do_mission))
        return value(lp_problem.objective) if value(lp_problem.objective) is not None else 0, sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, number_of_runs:int, mission_generator:'MissionGenerator' = None, seed=0) -> List[int]:
        exp = Offline_LP(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, mission_generator = mission_generator, seed=seed)
        return exp._run_experiment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, number_of_runs, exp)


class Online_Random_Assignment(Scheme_27_5_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat,[]) for sat in range(self._sat_num))
        assigned_missions = 0
        for m in range(self._mission_num):
            eligible_sats = [sat for sat in range(self._sat_num) if m in scenario[sat] and len(sat_assignment[sat]) < self._mission_per_sat]

            if len(eligible_sats) > 0:
                sat_assignment[random.choice(eligible_sats)].append(m)
                assigned_missions += 1

        assert all(len(sat_assignment[k]) <= self._mission_per_sat for k in range(self._sat_num)), 'Each satellite must have atmost %d missions assigned' % (self._mission_per_sat)
        assert all(m in scenario[sat] for sat in range(self._sat_num) for m in sat_assignment[sat]), 'Each satellite must be able to perform an assigned mission in current scenario'

        elapsed_time = time.time() - start_time
        print("Online_Random_Assignment:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-MissionsPerSat:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._mission_per_sat, self._pr_sat_to_do_mission))
        return assigned_missions, sat_assignment

    @staticmethod
    def _run_experiment(number_of_runs:int, number_of_runs_per_scenario: int, exp: 'Online_Random_Assignment') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario = exp._generate_scenario()
            scenario_result = 0.
            for j in range(number_of_runs_per_scenario):
                scenario_result += exp._compute_mission_assignment(scenario)[0]
            result.append(scenario_result / number_of_runs_per_scenario)

        return result

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, number_of_runs:int, number_of_runs_per_scenario: int, mission_generator:'MissionGenerator' = None, seed=0) -> List[int]:
        exp = Online_Random_Assignment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, mission_generator=mission_generator, seed=seed)
        return exp._run_experiment(number_of_runs, number_of_runs_per_scenario, exp)


class FirstFit(Scheme_27_5_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, []) for sat in range(self._sat_num))
        assigned_missions = 0
        for m in range(self._mission_num):
            for sat in range(self._sat_num):
                if len(sat_assignment[sat]) < self._mission_per_sat and m in scenario[sat]:
                    sat_assignment[sat].append(m)
                    assigned_missions += 1
                    break

        assert all(len(sat_assignment[k]) <= self._mission_per_sat for k in range(self._sat_num))
        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-MissionsPerSat:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._mission_per_sat, self._pr_sat_to_do_mission))
        return assigned_missions, sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, number_of_runs:int, mission_generator:'MissionGenerator' = None, seed=0) -> List[int]:
        exp = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, mission_generator=mission_generator, seed=seed)
        return exp._run_experiment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, number_of_runs, exp)

class BestFit(Scheme_27_5_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, []) for sat in range(self._sat_num))
        assigned_missions = 0
        for m in range(self._mission_num):
            sat_ordered_by_busy_buffer = sorted([(sat, len(sat_assignment[sat])) for sat in range(self._sat_num)], key=lambda x: x[1], reverse=True)
            for sat, num_of_assigned_mission in sat_ordered_by_busy_buffer:
                if num_of_assigned_mission < self._mission_per_sat and m in scenario[sat]:
                    sat_assignment[sat].append(m)
                    assigned_missions += 1
                    break

        assert all(len(sat_assignment[k]) <= self._mission_per_sat for k in range(self._sat_num))
        elapsed_time = time.time() - start_time
        print("BestFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-MissionsPerSat:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._mission_per_sat, self._pr_sat_to_do_mission))
        return assigned_missions, sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float, number_of_runs:int, mission_generator:'MissionGenerator' = None, seed=0) -> List[int]:
        exp = BestFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, mission_generator=mission_generator, seed=seed)
        return exp._run_experiment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission, number_of_runs, exp)


class MissionGenerator():
    def __init__(self, sat_num, mission_num, seed=0):
        self._sat_num = sat_num
        self._mission_num = mission_num

        self._random = random.Random(seed)

    @abstractmethod
    def generate_scenario(self) -> Dict[(int, List[int])]:
        pass


class GenericMissionGenerator(MissionGenerator):

    def __init__(self, sat_num, mission_num, pr_sat_to_do_mission, seed=0):
        super(GenericMissionGenerator, self).__init__(sat_num, mission_num, seed)
        self._pr_sat_to_do_mission = pr_sat_to_do_mission

    def generate_scenario(self) -> Dict[(int, List[int])]:
        '''

        :return: Sat -> missions it is able to handle
        '''
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in range(self._mission_num):
                if self._random.random() < self._pr_sat_to_do_mission:
                    result[s].append(m)

        return result

class EasyHardMissionGenerator(MissionGenerator):

    def __init__(self, sat_num, mission_num, easy_proportion, seed=0):
        assert 0 <= easy_proportion <= 1, 'easy_proportion must be a float number in [0,1]'
        super(EasyHardMissionGenerator, self).__init__(sat_num, mission_num, seed)
        self._easy_proportion = round(easy_proportion, 2)
        self._hard_proportion = round(1 - self._easy_proportion, 2)

        assert round(self._easy_proportion * self._mission_num) + round(self._hard_proportion * self._mission_num) == self._mission_num


    def generate_scenario(self) -> Dict[(int, List[int])]:
        '''

        :return: Sat -> missions it is able to handle
        '''
        aux = [self._sat_num for i in range(round(self._easy_proportion * self._mission_num))] + [1 for i in range(round(self._hard_proportion * self._mission_num))]
        assert len(aux) == self._mission_num
        aux = self._random.sample(aux, self._mission_num)
        assert len(aux) == self._mission_num

        result = dict((i, []) for i in range(self._sat_num))
        for mission, capable_sat in zip(range(self._mission_num), aux):
            if capable_sat == self._sat_num:
                for sat in range(self._sat_num):
                    result[sat].append(mission)
            else:
                result[self._random.choice(range(self._sat_num))].append(mission)

        return result


class EasyHardMissionGenerator2(MissionGenerator):

    def __init__(self, sat_num, mission_num, easy_proportion, pr_easy, pr_hard, seed=0):
        assert 0 <= easy_proportion <= 1, 'easy_proportion must be a float number in [0,1]'
        assert 0 <= pr_easy <= 1, 'pr_easy must be a probability (ie, float number in [0,1])'
        assert 0 <= pr_hard <= 1, 'pr_hard must be a probability (ie, float number in [0,1])'

        super(EasyHardMissionGenerator2, self).__init__(sat_num, mission_num, seed)
        self._easy_proportion = round(easy_proportion, 2)
        self._hard_proportion = round(1 - self._easy_proportion, 2)
        self._pr_easy = pr_easy
        self._pr_hard = pr_hard

        assert round(self._easy_proportion * self._mission_num) + round(self._hard_proportion * self._mission_num) == self._mission_num

    def generate_scenario(self) -> Dict[(int, List[int])]:
        '''

        :return: Sat -> missions it is able to handle
        '''

        aux = [self._pr_easy for i in range(round(self._easy_proportion * self._mission_num))] + [self._pr_hard for i in range(round(self._hard_proportion * self._mission_num))]
        aux = self._random.sample(aux, self._mission_num)
        assert len(aux) == self._mission_num

        result = dict((i, []) for i in range(self._sat_num))
        for mission, pr in zip(range(self._mission_num), aux):
            for sat in range(self._sat_num):
                if self._random.random() < pr:
                    result[sat].append(mission)

        return result

