import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'svg/Exp-Comparison-sat_num:10,pr:0.90,mission_num:19,mission_per_sat:10,low_proportion0.10,max_mission_size:10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,pr:0.90,mission_num:55,mission_per_sat:10,low_proportion0.50,max_mission_size:10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,pr:0.90,mission_num:91,mission_per_sat:10,low_proportion0.90,max_mission_size:10,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:50,pr:0.90,mission_num:95,mission_per_sat:10,low_proportion0.10,max_mission_size:10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,pr:0.90,mission_num:275,mission_per_sat:10,low_proportion0.50,max_mission_size:10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,pr:0.90,mission_num:455,mission_per_sat:10,low_proportion0.90,max_mission_size:10,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:100,pr:0.90,mission_num:190,mission_per_sat:10,low_proportion0.10,max_mission_size:10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,pr:0.90,mission_num:550,mission_per_sat:10,low_proportion0.50,max_mission_size:10,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,pr:0.90,mission_num:910,mission_per_sat:10,low_proportion0.90,max_mission_size:10,rep:35,seed:245.svg',

]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,50,100],pr:0.9,easy_propotion[0.1,0.5,0.9],varying_offeredload,extremmecase'
combine_into_table_and_save(3,3,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))