import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from varying_mission_size.imp_24_6_19 import OnlineRandomAssignment, Offline_LP, Online, FirstFit, BestFit
import time
import random
from fractions import gcd
start_time = time.time()


OUTPUT_DIR = 'results-%s-%s'%(os.path.basename(__file__), date.today().strftime("%d-%m-%Y"))
SAT_NUM_RANGE = [10, 50, 100]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
SCENARIO_PROPORTION = [{'HIGH_PR':.50, 'LOW_PR':.50}, {'HIGH_PR':.90, 'LOW_PR':.10}, {'HIGH_PR':.10, 'LOW_PR':.90}, {'HIGH_PR':.70, 'LOW_PR':.30}, {'HIGH_PR':.30, 'LOW_PR':.70}]
PR_SAT_DO_MISSION = [0.9]
REPETITIONS = 35
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + OUTPUT_DIR + '/png')
os.system('mkdir ' + OUTPUT_DIR + '/svg')
os.system('mkdir ' + OUTPUT_DIR + '/txt')
for mission_proportion in SCENARIO_PROPORTION:
    for sat_num in SAT_NUM_RANGE:
        system_capacity = sat_num * SAT_CAPACITY
        for pr in PR_SAT_DO_MISSION:
            results_offline_lp = []
            results_online_greedy = []
            results_online_random = []
            results_firstfit = []
            results_bestfit = []
            for offered_load in MISSION_PROPORTION:
                print(round(offered_load * system_capacity * mission_proportion['HIGH_PR']))
                max_mission_size = max(i for i in range(10, 0, -1) if round(offered_load * system_capacity * mission_proportion['HIGH_PR']) % i == 0) #gcd(SAT_CAPACITY, offered_load * system_capacity * mission_proportion['HIGH_PR'])
                print("[Info] max_mission_size = %d"%max_mission_size)
                mission_cost_values = [1] * round(offered_load * system_capacity * mission_proportion['LOW_PR']) + [max_mission_size] * round(offered_load * system_capacity * mission_proportion['HIGH_PR'] / max_mission_size)
                assert sum(mission_cost_values) == system_capacity * offered_load
                mission_num = len(mission_cost_values);
                mission_cost_dict = dict(zip(range(mission_num), random.sample(mission_cost_values, mission_num)))
                f_mission_cost = lambda m: mission_cost_dict[m]
                print("[Running] sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,offeredload:%.2f,low_proportion%.2f,rep:%d " % (sat_num, pr, mission_num, SAT_CAPACITY, offered_load, mission_proportion['LOW_PR'], REPETITIONS))

                results_offline_lp.append(Offline_LP.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))
                results_online_greedy.append(Online.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))
                results_online_random.append(OnlineRandomAssignment.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS,RUNS_PER_SCENARIO, f_mission_cost=f_mission_cost, seed=SEED))
                results_firstfit.append(FirstFit.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))
                results_bestfit.append(BestFit.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None, REPETITIONS,f_mission_cost=f_mission_cost, seed=SEED))


            lprom_offline_lp = [(MISSION_PROPORTION[i], average(results_offline_lp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_online_greedy = [(MISSION_PROPORTION[i], average(results_online_greedy[i])/ system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_online_random =[(MISSION_PROPORTION[i], average(results_online_random[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_firstfit = [(MISSION_PROPORTION[i], average(results_firstfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_bestfit = [(MISSION_PROPORTION[i], average(results_bestfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]

            fname = 'Exp-OfflineLP-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
            print_str_to_file(str(lprom_offline_lp), os.path.join(OUTPUT_DIR, 'txt' , fname + '.txt'))
            #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')


            fname = 'Exp-OnlineGreedy-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,low_proportion%.2f,max_mission_size:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, mission_proportion['LOW_PR'], max_mission_size, REPETITIONS, SEED)
            print_str_to_file(str(lprom_online_greedy), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')

            fname = 'Exp-OnlineRandom-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,low_proportion%.2f,max_mission_size:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, mission_proportion['LOW_PR'], max_mission_size, REPETITIONS, SEED)
            print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')

            fname = 'Exp-firstfit-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,low_proportion%.2f,max_mission_size:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, mission_proportion['LOW_PR'], max_mission_size, REPETITIONS, SEED)
            print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')

            fname = 'Exp-bestfit-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,low_proportion%.2f,max_mission_size:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, mission_proportion['LOW_PR'], max_mission_size, REPETITIONS, SEED)
            print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')


            fname = 'Exp-Comparison-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,low_proportion%.2f,max_mission_size:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, mission_proportion['LOW_PR'], max_mission_size, REPETITIONS, SEED)
            plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['Offline LP assignation', 'Online Greedy assignation', 'Online Random assignation', 'FirstFit', 'BestFit'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='png')
            plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['Offline LP assignation', 'Online Greedy assignation', 'Online Random assignation', 'FirstFit', 'BestFit'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg')


print("Elapsed time: %f seconds"%(time.time() - start_time))
