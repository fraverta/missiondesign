import setuptools

setuptools.setup(name='svg_stack',
      version='0.0.1', # keep in sync with svg_stack.py
      author='Andrew Straw <strawman@astraw.com>',
      url='http://github.com/astraw/svg_stack',
      py_modules=['svg_stack', 'svgtogrid'],
      scripts=['svg_stack.py', 'svgtogrid.py'],
      install_requires='lxml==4.3.4'
      )
