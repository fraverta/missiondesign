import pulp
import random
import pprint

times = list(range(0,4))
cells = list(range(0,5))
sats = list(range(0,8))

physical_access = {}
for k in times:
    physical_access[k] = {}

for k in times:
    for i in sats:
        cell = random.randint(0,len(cells))
        physical_access[k].setdefault(cell, []).append(i)

pprint.pprint(physical_access)


# ---model indices denominations----------------------------------------------------------------------------------------
TIME_STEP = "k"
"""
Time step index denomination.
"""
CELL_ID = "j"
"""
Cell ID index denomination.
"""
SAT_ID = "i"
"""
SAT ID index denomination.
"""
#-----------------------------------------------------------------------------------------------------------------------

# Specify if a satellite is in a cell in a given time step
VARIABLES_NAME = "X(%s,%s,%s)" % (TIME_STEP, CELL_ID, SAT_ID)

prob1 = pulp.LpProblem("Cell Asignation Problem", pulp.CPLEX())

xp_tuples = []
for k, cells_sats in sorted(physical_access.items()):
    for j, sats in sorted(cells_sats.items()):
        for i in sats:
            xp_tuples.append((k,j,i))

xps = pulp.LpVariable.dicts(VARIABLES_NAME, xp_tuples, lowBound=0, upBound=1, cat=pulp.LpInteger)

#pprint.pprint(xps)

# Objective Funtion
prob1 += pulp.lpSum(xps[(k,j,i)] for k, cells_sats in sorted(physical_access.items()) for j, sats in sorted(cells_sats.items()) for i in sats), "Throughput"

# Constraint
for k, cells_sats in sorted(physical_access.items()):
    for j, sats in sorted(cells_sats.items()):
            prob1 += pulp.lpSum(xps[(k,j,i)] for i in sats) == 1, "time=" + str(k) + "_" + "cell=" + str(j)

prob1.writeLP("cell_assignation_model.lp")
prob1.solve(solver=pulp.CPLEX())
print("Status:", pulp.LpStatus[prob1.status])

for v in prob1.variables():
    print(v.name, " = ", v.varValue)