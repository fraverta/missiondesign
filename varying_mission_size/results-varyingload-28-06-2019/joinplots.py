import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
'svg/Exp-Comparison-sat_num:10,pr:0.10,mission_num:60,mission_per_sat:10,rep:35,seed:245.svg',
'svg/Exp-Comparison-sat_num:100,pr:0.10,mission_num:600,mission_per_sat:10,rep:35,seed:245.svg',
'svg/Exp-Comparison-sat_num:300,pr:0.10,mission_num:1800,mission_per_sat:10,rep:35,seed:245.svg',
]

fname = 'comparison-sat_num:[10,100,300],pr=0.1'
combine_into_table_and_save(1,3,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('[ -f %s ] || inkscape --export-png=%s %s'%(fname + '.png', fname + '.png', fname + '.svg'))

files_m1 = [
'svg/Exp-Comparison-sat_num:10,pr:0.50,mission_num:60,mission_per_sat:10,rep:35,seed:245.svg',
'svg/Exp-Comparison-sat_num:100,pr:0.50,mission_num:600,mission_per_sat:10,rep:35,seed:245.svg',
'svg/Exp-Comparison-sat_num:300,pr:0.50,mission_num:1800,mission_per_sat:10,rep:35,seed:245.svg',
]

fname = 'comparison-sat_num:[10,100,300],pr=0.5'
combine_into_table_and_save(1,3,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('[ -f %s ] || inkscape --export-png=%s %s'%(fname + '.png', fname + '.png', fname + '.svg'))

files_m1 = [
'svg/Exp-Comparison-sat_num:10,pr:1.00,mission_num:60,mission_per_sat:10,rep:35,seed:245.svg',
'svg/Exp-Comparison-sat_num:100,pr:1.00,mission_num:600,mission_per_sat:10,rep:35,seed:245.svg',
'svg/Exp-Comparison-sat_num:300,pr:1.00,mission_num:1800,mission_per_sat:10,rep:35,seed:245.svg',
]

fname = 'comparison-sat_num:[10,100,300],pr=1.00'
combine_into_table_and_save(1,3,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('[ -f %s ] || inkscape --export-png=%s %s'%(fname + '.png', fname + '.png', fname + '.svg'))