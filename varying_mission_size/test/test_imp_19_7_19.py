import unittest
from varying_mission_size.imp_19_7_19 import Scheme_19_7_19, Online, Offline_LP, OnlineRandomAssignment, FirstFit, BestFit, GenericScenarioGenerator, UserScenarioGenerator, ExtremmeMissionGenerator, SmoothExtremmeMissionGenerator, MissionAwareFit
from functools import reduce
import operator

class Test_19_7_19(unittest.TestCase):

    def test_mission_categories(self):
        sat_num = 3; mission_num = 50; sat_capacity=9;  pr_sat_to_do_mission = 0.5; mission_categories_num = 5;
        scenario_generator = GenericScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, mission_categories_num)
        scenario, mission_weight = scenario_generator.generate_scenario()
        mission_by_capacity = [0] * mission_categories_num
        for m in mission_weight:
            mission_by_capacity[m - 1] += 1

        self.assertEqual([10] * mission_categories_num, mission_by_capacity, "Missions were missdistributed into categories")


    '''
        ONLINE ASSIGNATION
    '''

    def test_online_assignement1(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  mission_categories_num = 5;
        online = Online(sat_num, sat_capacity, None)
        expected_assigned_missions = {0: [0,3], 1:[1,5,6], 2:[2,10]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    def test_online_assignement2(self):
        sat_num = 9; mission_num = 15; sat_capacity=5; mission_categories_num = 5;
        online = Online(sat_num, sat_capacity, None)
        expected_assigned_missions = {0: [0,10,12], 1:[1], 2:[2], 3:[3], 4:[4], 5:[5,11], 6:[6], 7:[7], 8:[8]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(31, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_online_assignement3(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  mission_categories_num = 5;
        online = Online(sat_num, sat_capacity, None)
        scenario = {0:[0,1,2,8,9], 1:[3,4,5,1,7,9], 2:[5,6,7,8,9]}
        expected_assigned_missions = {0: [0,2], 1:[1,7], 2:[5,6]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(12, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    def test_online_run_experiment(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        scenario_generator = GenericScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, mission_categories_num)
        online = Online.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs=10)
        self.assertEqual([14] *10, online, '')

    def test_online_empty_scenario(self):
        sat_num = 10; mission_num = 50; sat_capacity=10; mission_categories_num=5;
        online = Online(sat_num, sat_capacity, None)
        expected_assigned_missions = dict((sat, []) for sat in range(sat_num))
        assigned_capacity, mission_assignation = online._compute_mission_assignment(dict((sat_id, []) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(0, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    '''
        OFFLINE ASSIGNATION
    '''

    def test_offline_assignement1(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 2;
        online = Offline_LP(sat_num, sat_capacity, None)
        scenario = {0:[0,1,3], 1:[2,5,7], 2:[4,6,8,9]}
        expected_assigned_missions = scenario
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(15, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_offline_assignement2(self):
        sat_num = 3; mission_num = 12; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 3;
        online = Offline_LP(sat_num, sat_capacity, None)
        scenario = {0:[0,1,2,3,7], 1:[0,3,5], 2:[0,3,8,7]}
        expected_assigned_missions = {0:[1,2], 1:[0,3,5], 2:[7,8]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])
        for k in mission_assignation.keys(): mission_assignation[k].sort();
        self.assertEqual(15, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_offline_empty_scenario(self):
        sat_num = 10; mission_num = 50; sat_capacity=10; mission_categories_num=5;
        offline = Offline_LP(sat_num, sat_capacity, None)
        expected_assigned_missions = dict((sat, []) for sat in range(sat_num))
        assigned_capacity, mission_assignation = offline._compute_mission_assignment(dict((sat_id, []) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(0, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')



    '''
        RANDOM ASSIGNATION
    '''

    def test_online_Random_Assignment_1(self):
        sat_num = 2; mission_num = 4; sat_capacity = 2; pr_sat_to_do_mission = 0.5; mission_categories_num=4;
        cost = lambda x: x + 1
        random_assignation = OnlineRandomAssignment(sat_num, sat_capacity, None)
        scenario = {0: [0,1], 1: [1,2,3]}
        num, assignation = random_assignation._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])

        self.assertEqual(num, sum(cost(x) for x in reduce(lambda x,y: x + y, assignation.values())))
        assert all( sum( cost(m) for m in assignation[k]) <= sat_capacity for k in range(sat_num)), 'Each satellite must have atmost %d missions assigned'%(sat_capacity)
        assert all(m in scenario[sat] for sat in range(sat_num) for m in assignation[sat]), 'Each satellite must be able to perform an assigned mission in current scenario'

    def test_online_Random_empty_scenario(self):
        sat_num = 10; mission_num = 50; sat_capacity=10; mission_categories_num=5;
        random = OnlineRandomAssignment(sat_num, sat_capacity, None)
        expected_assigned_missions = dict((sat, []) for sat in range(sat_num))
        assigned_capacity, mission_assignation = random._compute_mission_assignment(dict((sat_id, []) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(0, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    '''
        FIRSTFIT
    '''

    def test_firstfit_assignement1(self):
        sat_num = 3; mission_num = 15; sat_capacity=5; mission_categories_num = 5;
        ff = FirstFit(sat_num, sat_capacity, None)
        expected_assigned_missions = {0: [0,1,5,10], 1:[2, 6], 2:[3]}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    def test_firstfit_assignement2(self):
        sat_num = 9; mission_num = 15; sat_capacity=5; mission_categories_num = 5;
        ff = FirstFit(sat_num, sat_capacity, None)
        expected_assigned_missions = {0: [0,1,5,10], 1:[2,6], 2:[3], 3:[4], 4:[7,11], 5:[8], 6:[9], 7:[12], 8:[13]}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(40, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_firstfit_assignement3(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 5;
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {0:[0,1,2,8,9], 1:[3,4,5,1,7,9], 2:[5,6,7,8,9]}
        expected_assigned_missions = {0: [0,1], 1:[3,5], 2:[6,7]}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(13, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_firstfit_empty_scenario(self):
        sat_num = 10; mission_num = 50; sat_capacity=10; mission_categories_num=5;
        ff = FirstFit(sat_num, sat_capacity, None)
        expected_assigned_missions = dict((sat, []) for sat in range(sat_num))
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(dict((sat_id, []) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(0, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')



    '''
        BESTFIT
    '''

    def test_bestfit_assignement1(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        bf = BestFit(sat_num, sat_capacity, None)
        expected_assigned_missions = {0: [0,1,6], 1:[2, 10], 2:[3,5]}
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_bestfit_assignement2(self):
        sat_num = 9; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        bf = BestFit(sat_num, sat_capacity, None)
        expected_assigned_missions = {0: [0,1,6], 1:[2,11], 2:[3,5], 3:[4], 4:[7], 5:[8,10], 6:[9], 7:[12], 8:[13]}
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(40, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_bestfit_assignement3(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 5;
        bf = BestFit(sat_num, sat_capacity, None)
        scenario = {0:[0,1,2,8,9], 1:[3,4,5,1,7,9], 2:[5,6,7,8,9]}
        expected_assigned_missions = {0: [0,1], 1:[3,5], 2:[6,7]}
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(13, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_bestfit_empty_scenario(self):
        sat_num = 10; mission_num = 50; sat_capacity=10; mission_categories_num=5;
        bf = BestFit(sat_num, sat_capacity, None)
        expected_assigned_missions = dict((sat, []) for sat in range(sat_num))
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(dict((sat_id, []) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
        self.assertEqual(0, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    '''
        MissionAwareFit 
    '''

    def test_MissionAwareFit_assignement1(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 2;
        online = MissionAwareFit(sat_num, sat_capacity, None)
        scenario = {0:[0,1,3], 1:[1,2,5,7], 2:[2,4,6,8,9]}
        expected_assigned_missions = {0:[0,1,3], 1:[2,5,7], 2:[4,6,8,9]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario, [1 for m in range(mission_num)])
        self.assertEqual(10, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_MissionAwareFit_assignement2(self):
        sat_num = 3; mission_num = 12; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 3;
        online = MissionAwareFit(sat_num, sat_capacity, None)
        scenario = {0:[0,1,2,3,7], 1:[0,3,5], 2:[0,3,8,7]}
        expected_assigned_missions = {0:[1,2], 1:[0,3,5], 2:[7,8]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario, [m % mission_categories_num + 1 for m in range(mission_num)])
        for k in mission_assignation.keys(): mission_assignation[k].sort();
        #self.assertEqual(15, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')
    #
    # def test_MissionAwareFit_empty_scenario(self):
    #     sat_num = 10; mission_num = 50; sat_capacity=10; mission_categories_num=5;
    #     offline = MissionAwareFit(sat_num, sat_capacity, None)
    #     expected_assigned_missions = dict((sat, []) for sat in range(sat_num))
    #     assigned_capacity, mission_assignation = offline._compute_mission_assignment(dict((sat_id, []) for sat_id in range(sat_num)), [m % mission_categories_num + 1 for m in range(mission_num)])
    #     self.assertEqual(0, assigned_capacity, '')
    #     self.assertEqual(expected_assigned_missions, mission_assignation, '')


    '''
    Test MissionGenerators
    '''

    def test_GenericMissionGenerator(self):
        mgen1 = GenericScenarioGenerator(10, 100, 0.5, 5, seed=10)
        mgen2 = GenericScenarioGenerator(10, 100, 0.5, 5, seed=10)

        mgen3 = GenericScenarioGenerator(10, 100, 0.5, 5, seed=20, randomize_mission_cost=True)
        mgen4 = GenericScenarioGenerator(10, 100, 0.5, 5, seed=20, randomize_mission_cost=True)

        self.assertEqual([mgen1.generate_scenario() for i in range(10)], [mgen2.generate_scenario() for i in range(10)])
        self.assertEqual([mgen3.generate_scenario() for i in range(10)], [mgen4.generate_scenario() for i in range(10)])
        self.assertFalse([mgen1.generate_scenario() for i in range(10)] == [mgen3.generate_scenario() for i in range(10)])

    def test_UserScenarioGenerator(self):
        mgen1 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], seed=10)
        mgen2 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], seed=10)

        mgen3 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100,0, -1)], seed=10, randomize_mission_cost=True)
        mgen4 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], seed=10, randomize_mission_cost=True)

        self.assertEqual([mgen1.generate_scenario() for i in range(10)], [mgen2.generate_scenario() for i in range(10)])
        self.assertEqual([mgen3.generate_scenario() for i in range(10)], [mgen4.generate_scenario() for i in range(10)])
        self.assertFalse([mgen1.generate_scenario() for i in range(10)] == [mgen3.generate_scenario() for i in range(10)])


    def test_ExtremmeMissionGenator_OnlyA(self):
        sat_num = 10; sat_capacity = 10; volumen_to_generate = sat_num * sat_capacity;

        mgen1 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.0, seed=10)
        for i in range(10):
            scenario, m_weight = mgen1.generate_scenario()
            self.assertTrue(all(all(0 <= m < volumen_to_generate // sat_capacity for m in scenario[sat]) for sat in range(sat_num)))
            self.assertTrue(all( x == sat_capacity for x in m_weight))

    def test_ExtremmeMissionGenator_OnlyB(self):
        sat_num = 10; sat_capacity = 10; volumen_to_generate = sat_num * sat_capacity;

        mgen1 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=1.0, seed=10)
        for i in range(10):
            scenario, m_weight = mgen1.generate_scenario()
            self.assertTrue(all(all(0 <= m < volumen_to_generate for m in scenario[sat]) for sat in range(sat_num)))
            self.assertTrue(all( x == 1 for x in m_weight))

    def test_ExtremmeMissionGenator_OnlyC(self):
        sat_num = 10; sat_capacity = 10; volumen_to_generate = sat_num * sat_capacity;

        mgen1 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_C=1.0, seed=10)
        for i in range(10):
            scenario, m_weight = mgen1.generate_scenario()
            self.assertTrue(all(all(0 <= m < volumen_to_generate for m in scenario[sat]) for sat in range(sat_num)))
            self.assertTrue(all( x == 1 for x in m_weight))


    def test_ExtremmeMissionGenator_OnlyD(self):
        sat_num = 10; sat_capacity = 10; volumen_to_generate = sat_num * sat_capacity;

        mgen1 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_D=1.0, seed=10)
        for i in range(10):
            scenario, m_weight = mgen1.generate_scenario()
            self.assertTrue(all(all(0 <= m < volumen_to_generate // sat_capacity for m in scenario[sat]) for sat in range(sat_num)))
            self.assertTrue(all( x == sat_capacity for x in m_weight))


    def test_ExtremmeMissionGenator1(self):
        sat_num = 20; sat_capacity = 10; volumen_to_generate = sat_num * sat_capacity;
        mgen1 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=0.25, prop_B=0.25, prop_C=0.25, prop_D=.25, seed=10)
        mgen2 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=0.25, prop_B=0.25, prop_C=0.25, prop_D=.25, seed=10)

        scenarios1 = [mgen1.generate_scenario() for i in range(10)]
        scenarios2 = [mgen2.generate_scenario() for i in range(10)]

        self.assertEqual(scenarios1, scenarios2)
        for scenario, mission_weights in scenarios1:
            self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), volumen_to_generate * 0.5)
            self.assertEqual(sum(filter(lambda x: x == sat_capacity, mission_weights)), volumen_to_generate * 0.5)

    def test_ExtremmeMissionGenator2(self):
        sat_num = 10; sat_capacity = 10; volumen_to_generate = sat_num * sat_capacity;
        mgen1 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=0.25, prop_B=0.25, prop_C=0.25, prop_D=.25, seed=10)
        mgen2 = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=0.25, prop_B=0.25, prop_C=0.25, prop_D=.25, seed=10)

        scenarios1 = [mgen1.generate_scenario() for i in range(10)]
        scenarios2 = [mgen2.generate_scenario() for i in range(10)]

        self.assertEqual(scenarios1, scenarios2)
        for scenario, mission_weights in scenarios1:
            self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), volumen_to_generate * 0.5)
            max_mission_size = max(i for i in range(1, sat_capacity + 1) if volumen_to_generate * 0.25 % i == 0 )
            self.assertEqual(sum(filter(lambda x: x == max_mission_size, mission_weights)), volumen_to_generate * 0.5)

    '''
    Test SmoothExtremmeMissionGenerator
    '''
    SmoothExtremmeMissionGenerator

    def test_SmoothExtremmeMissionGenerator_OnlyA(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate // sat_capacity + 1 for m in scenario[sat]) for sat in range(sat_num))) # An test for mission identifier.
                self.assertTrue(all( x == sat_capacity for x in m_weight))
                self.assertTrue(volumen_to_generate - sat_capacity < sum(m_weight) < volumen_to_generate + sat_capacity)

    def test_SmoothExtremmeMissionGenerator_OnlyB(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == 1 for x in m_weight))
                self.assertEqual(sum(m_weight), volumen_to_generate)

    def test_SmoothExtremmeMissionGenerator_OnlyC(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_C=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == 1 for x in m_weight))
                self.assertEqual(sum(m_weight), volumen_to_generate)


    def test_SmoothExtremmeMissionGenerator_OnlyD(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_D=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate // sat_capacity + 1 for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == sat_capacity for x in m_weight))
                self.assertTrue(volumen_to_generate - sat_capacity < sum(m_weight) < volumen_to_generate + sat_capacity)

    def test_SmoothExtremmeMissionGenerator_OnlyI(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity; mission_size = sat_capacity // 2

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_I=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate // mission_size + 1 for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == mission_size for x in m_weight))
                self.assertTrue(volumen_to_generate - mission_size < sum(m_weight) < volumen_to_generate + mission_size)

    def test_SmoothExtremmeMissionGenerator_OnlyAB(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity; mission_size = sat_capacity // 2

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_AB=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate // mission_size + 1 for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == mission_size for x in m_weight))
                self.assertTrue(volumen_to_generate - mission_size < sum(m_weight) < volumen_to_generate + mission_size)

    def test_SmoothExtremmeMissionGenerator_OnlyBC(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity; mission_size = 1

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_BC=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == mission_size for x in m_weight))
                self.assertEqual(sum(m_weight), volumen_to_generate)

    def test_SmoothExtremmeMissionGenerator_OnlyCD(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity; mission_size = sat_capacity // 2

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_CD=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate // mission_size + 1 for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == mission_size for x in m_weight))
                self.assertTrue(volumen_to_generate - mission_size < sum(m_weight) < volumen_to_generate + mission_size)

    def test_SmoothExtremmeMissionGenerator_OnlyAD(self):
        sat_num = 10; sat_capacity = 10; system_capacity = sat_num * sat_capacity; mission_size = sat_capacity

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_AD=1.0, seed=10)
            for i in range(10):
                scenario, m_weight = mgen1.generate_scenario()
                self.assertTrue(all(all(0 <= m < volumen_to_generate for m in scenario[sat]) for sat in range(sat_num)))
                self.assertTrue(all( x == mission_size for x in m_weight))
                self.assertTrue(volumen_to_generate - mission_size < sum(m_weight) < volumen_to_generate + mission_size)


    def test_SmoothExtremmeMissionGeneratorABCD(self):
        sat_num = 20; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=0.25, prop_B=0.25, prop_C=0.25, prop_D=.25, seed=10)
            mgen2 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=0.25, prop_B=0.25, prop_C=0.25, prop_D=.25, seed=10)

            scenarios1 = [mgen1.generate_scenario() for i in range(10)]
            scenarios2 = [mgen2.generate_scenario() for i in range(10)]

            self.assertEqual(scenarios1, scenarios2)
            for scenario, mission_weights in scenarios1:
                self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), 2 * round(volumen_to_generate * 0.25))
                self.assertTrue(volumen_to_generate * 0.5  - 2 * sat_capacity < sum(filter(lambda x: x == sat_capacity, mission_weights)) < volumen_to_generate * 0.5 + 2 * sat_capacity)

    def test_SmoothExtremmeMissionGeneratorABbc(self):
        sat_num = 20; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_BC=.50, seed=10)
            mgen2 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_BC=.50, seed=10)

            scenarios1 = [mgen1.generate_scenario() for i in range(10)]
            scenarios2 = [mgen2.generate_scenario() for i in range(10)]

            self.assertEqual(scenarios1, scenarios2)
            for scenario, mission_weights in scenarios1:
                self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), 2 * round(volumen_to_generate * 0.25) + round(volumen_to_generate * .5))

    def test_SmoothExtremmeMissionGeneratorABad(self):
        sat_num = 20; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_AD=.50, seed=10)
            mgen2 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_AD=.50, seed=10)

            scenarios1 = [mgen1.generate_scenario() for i in range(10)]
            scenarios2 = [mgen2.generate_scenario() for i in range(10)]

            self.assertEqual(scenarios1, scenarios2)
            for scenario, mission_weights in scenarios1:
                self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), 2 * round(volumen_to_generate * 0.25))
                self.assertTrue(volumen_to_generate * 0.5 - sat_capacity < sum(filter(lambda x: x == sat_capacity, mission_weights)) < volumen_to_generate * 0.5 + sat_capacity)


    def test_SmoothExtremmeMissionGeneratorABI(self):
        sat_num = 20; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_I=.50, seed=10)
            mgen2 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_I=.50, seed=10)

            scenarios1 = [mgen1.generate_scenario() for i in range(10)]
            scenarios2 = [mgen2.generate_scenario() for i in range(10)]

            self.assertEqual(scenarios1, scenarios2)
            for scenario, mission_weights in scenarios1:
                self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), 2 * round(volumen_to_generate * 0.25))
                self.assertTrue(volumen_to_generate * 0.5 - sat_capacity // 2 < sum(filter(lambda x: x == sat_capacity // 2, mission_weights)) < volumen_to_generate * 0.5 + sat_capacity // 2)

    def test_SmoothExtremmeMissionGeneratorABcd(self):
        sat_num = 20; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_CD=.50, seed=10)
            mgen2 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_CD=.50, seed=10)

            scenarios1 = [mgen1.generate_scenario() for i in range(10)]
            scenarios2 = [mgen2.generate_scenario() for i in range(10)]

            self.assertEqual(scenarios1, scenarios2)
            for scenario, mission_weights in scenarios1:
                self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), 2 * round(volumen_to_generate * 0.25))
                self.assertTrue(volumen_to_generate * 0.5 - sat_capacity // 2 < sum(filter(lambda x: x == sat_capacity // 2, mission_weights)) < volumen_to_generate * 0.5 + sat_capacity // 2)


    def test_SmoothExtremmeMissionGeneratorABad(self):
        sat_num = 20; sat_capacity = 10; system_capacity = sat_num * sat_capacity;

        for volumen_to_generate in [round(i / 100 * system_capacity) for i in range(10, 110, 5)]:
            mgen1 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_AD=.50, seed=10)
            mgen2 = SmoothExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_B=0.25, prop_C=0.25, prop_AD=.50, seed=10)

            scenarios1 = [mgen1.generate_scenario() for i in range(10)]
            scenarios2 = [mgen2.generate_scenario() for i in range(10)]

            self.assertEqual(scenarios1, scenarios2)
            for scenario, mission_weights in scenarios1:
                self.assertEqual(sum(filter(lambda x: x == 1, mission_weights)), 2 * round(volumen_to_generate * 0.25))
                self.assertTrue(volumen_to_generate * 0.5 - sat_capacity < sum(filter(lambda x: x == sat_capacity, mission_weights)) < volumen_to_generate * 0.5 + sat_capacity)

    '''
    Integration test
    '''

    def test_run_experiment_online_assignement(self):
        sat_num = 10; sat_capacity = 100; mission_categories_num = 5; volumen_to_generate = sat_num * 10;
        mgen = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.)
        l = Online.run_experiment(sat_num,sat_capacity,mgen, 100)
        self.assertEqual(100, len(l))

    def test_run_experiment_onlinerandom_assignement(self):
        sat_num = 10; sat_capacity = 100; volumen_to_generate = sat_num * 10;
        mgen = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.)
        l = OnlineRandomAssignment.run_experiment(sat_num,sat_capacity,mgen, 100, 10, seed=10)
        self.assertEqual(100, len(l))

    def test_run_experiment_offlinelp_assignement(self):
        sat_num = 10; sat_capacity = 100; volumen_to_generate = sat_num * 10;
        mgen = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.)
        l = Offline_LP.run_experiment(sat_num,sat_capacity,mgen, 100)
        self.assertEqual(100, len(l))

    def test_run_experiment_firstfit_assignement(self):
        sat_num = 10; sat_capacity = 100; volumen_to_generate = sat_num * 10;
        mgen = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.)
        l = FirstFit.run_experiment(sat_num,sat_capacity,mgen, 100)
        self.assertEqual(100, len(l))


    def test_run_experiment_BestFit_assignement(self):
        sat_num = 10; sat_capacity = 100; volumen_to_generate = sat_num * 10;
        mgen = ExtremmeMissionGenerator(sat_num, sat_capacity, volumen_to_generate, prop_A=1.)
        l = BestFit.run_experiment(sat_num,sat_capacity,mgen, 100)
        self.assertEqual(100, len(l))
