import unittest
from varying_mission_size.imp_24_6_19 import Scheme_24_6_19, Online, Offline_LP, OnlineRandomAssignment, FirstFit, BestFit
from functools import reduce

class Test_24_6_19(unittest.TestCase):

    def test_mission_categories(self):
        sat_num = 3; mission_num = 50; sat_capacity=9;  pr_sat_to_do_mission = 0.5; mission_categories_num = 5;
        test = Scheme_24_6_19(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        mission_by_capacity = [0] * mission_categories_num
        for m in range(mission_num):
            mission_by_capacity[test.mission_capacity(m) - 1] += 1

        self.assertEqual([10] * mission_categories_num, mission_by_capacity, "Missions were missdistributed into categories")


    '''
        ONLINE ASSIGNATION
    '''

    def test_online_assignement1(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        online = Online(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        expected_assigned_missions = {0: [0,3], 1:[1,5,6], 2:[2,10]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)))
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    def test_online_assignement2(self):
        sat_num = 9; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        online = Online(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        expected_assigned_missions = {0: [0,10,12], 1:[1], 2:[2], 3:[3], 4:[4], 5:[5,11], 6:[6], 7:[7], 8:[8]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)))
        self.assertEqual(31, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_online_assignement3(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 5;
        online = Online(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        scenario = {0:[0,1,2,8,9], 1:[3,4,5,1,7,9], 2:[5,6,7,8,9]}
        expected_assigned_missions = {0: [0,2], 1:[1,7], 2:[5,6]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario)
        self.assertEqual(12, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    def test_online_run_experiment(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        online = Online.run_experiment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num, number_of_runs=10)
        self.assertEqual([14] *10, online, '')


    '''
        OFFLINE ASSIGNATION
    '''

    def test_offline_assignement1(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 2;
        online = Offline_LP(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        scenario = {0:[0,1,3], 1:[2,5,7], 2:[4,6,8,9]}
        expected_assigned_missions = scenario
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario)
        self.assertEqual(15, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_offline_assignement2(self):
        sat_num = 3; mission_num = 12; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 3;
        online = Offline_LP(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        scenario = {0:[0,1,2,3,7], 1:[0,3,5], 2:[0,3,8,7]}
        expected_assigned_missions = {0:[1,2], 1:[0,3,5], 2:[7,8]}
        assigned_capacity, mission_assignation = online._compute_mission_assignment(scenario)
        for k in mission_assignation.keys(): mission_assignation[k].sort();
        self.assertEqual(15, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')



    '''
        RANDOM ASSIGNATION
    '''

    def test_offline_Random_Assignment_1(self):
        sat_num = 2; mission_num = 4; sat_capacity = 2; pr_sat_to_do_mission = 0.5; mission_categories_num=4;
        cost = lambda x: x + 1
        random_assignation = OnlineRandomAssignment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission,mission_categories_num)
        scenario = {0: [0,1], 1: [1,2,3]}
        num, assignation = random_assignation._compute_mission_assignment(scenario)

        self.assertEqual(num, sum(cost(x) for x in reduce(lambda x,y: x + y, assignation.values())))
        assert all( sum( cost(m) for m in assignation[k]) <= sat_capacity for k in range(sat_num)), 'Each satellite must have atmost %d missions assigned'%(sat_capacity)
        assert all(m in scenario[sat] for sat in range(sat_num) for m in assignation[sat]), 'Each satellite must be able to perform an assigned mission in current scenario'

    '''
        FIRSTFIT
    '''

    def test_firstfit_assignement1(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        ff = FirstFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        expected_assigned_missions = {0: [0,1,5,10], 1:[2, 6], 2:[3]}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)))
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    def test_firstfit_assignement2(self):
        sat_num = 9; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        ff = FirstFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        expected_assigned_missions = {0: [0,1,5,10], 1:[2,6], 2:[3], 3:[4], 4:[7,11], 5:[8], 6:[9], 7:[12], 8:[13]}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)))
        self.assertEqual(40, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_firstfit_assignement3(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 5;
        ff = FirstFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        scenario = {0:[0,1,2,8,9], 1:[3,4,5,1,7,9], 2:[5,6,7,8,9]}
        expected_assigned_missions = {0: [0,1], 1:[3,5], 2:[6,7]}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(13, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    '''
        BESTFIT
    '''

    def test_bestfit_assignement1(self):
        sat_num = 3; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        bf = BestFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        expected_assigned_missions = {0: [0,1,6], 1:[2, 10], 2:[3,5]}
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)))
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_bestfit_assignement2(self):
        sat_num = 9; mission_num = 15; sat_capacity=5;  pr_sat_to_do_mission = 1.; mission_categories_num = 5;
        bf = BestFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        expected_assigned_missions = {0: [0,1,6], 1:[2,11], 2:[3,5], 3:[4], 4:[7], 5:[8,10], 6:[9], 7:[12], 8:[13]}
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(dict((sat_id, list(range(mission_num))) for sat_id in range(sat_num)))
        self.assertEqual(40, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_bestfit_assignement3(self):
        sat_num = 3; mission_num = 10; sat_capacity=5;  pr_sat_to_do_mission = 0.; mission_categories_num = 5;
        bf = BestFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num)
        scenario = {0:[0,1,2,8,9], 1:[3,4,5,1,7,9], 2:[5,6,7,8,9]}
        expected_assigned_missions = {0: [0,1], 1:[3,5], 2:[6,7]}
        assigned_capacity, mission_assignation = bf._compute_mission_assignment(scenario)
        self.assertEqual(13, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')