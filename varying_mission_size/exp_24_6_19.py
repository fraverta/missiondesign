import sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
from varying_mission_size.imp_24_6_19 import Online, Offline_LP, OnlineRandomAssignment, FirstFit, BestFit
from datetime import date
import os

OUTPUT_DIR = 'results-%s'%(date.today().strftime("%d-%m-%Y"))
SAT_NUM_RANGE = [10, 100, 300]
MISSION_X_RANGE = range(2, 4)
PR_SAT_DO_MISSION = [x/100 for x in range(10,110,10)]
REPETITIONS = 25
#SEED = random.randint(0, 1000)
SEED = 245
#MISSION_PER_SAT_RANGE = range(1, )
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT
CATEGORIES = 5

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + OUTPUT_DIR + '/normalized-NONRandomized')
for sat_num in SAT_NUM_RANGE:
    for xmission in MISSION_X_RANGE:
        mission_num = sat_num * xmission
        sat_capacity = sum( c * (sat_num * xmission / CATEGORIES) for c in range(1, CATEGORIES + 1)) / sat_num # sum of capacity of all missions
        results_offline = []
        results_online = []
        results_random = []
        results_firstfit = []
        results_bestfit = []
        for pr in PR_SAT_DO_MISSION:
            print("[Running] sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d - pr:%.2f"% (sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, pr))


            results_offline.append(Offline_LP.run_experiment(sat_num, mission_num, sat_capacity, pr, CATEGORIES,  REPETITIONS, seed=SEED))
            results_online.append(Online.run_experiment(sat_num, mission_num, sat_capacity, pr, CATEGORIES,  REPETITIONS, seed=SEED))
            results_random.append(OnlineRandomAssignment.run_experiment(sat_num, mission_num, sat_capacity, pr, CATEGORIES, REPETITIONS, RUNS_PER_SCENARIO, seed=SEED))
            results_firstfit.append(FirstFit.run_experiment(sat_num, mission_num, sat_capacity, pr, CATEGORIES,  REPETITIONS, seed=SEED))
            results_bestfit.append(BestFit.run_experiment(sat_num, mission_num, sat_capacity, pr, CATEGORIES, REPETITIONS, seed=SEED))

        lprom_offline = [(i*10/100, average(results_offline[i])) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_online = [(i*10/100, average(results_online[i])) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_ramdom = [(i*10/100, average(results_random[i])) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_firstfit = [(i*10/100, average(results_firstfit[i])) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_bestfit = [(i * 10 / 100, average(results_bestfit[i])) for i in range(len(PR_SAT_DO_MISSION))]

        lprom_normalized_offline = [(i*10/100, average(results_offline[i]) / (sat_capacity * sat_num)) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_normalized_online = [(i*10/100, average(results_online[i]) / (sat_capacity * sat_num)) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_normalized_random = [(i*10/100, average(results_random[i]) / (sat_capacity * sat_num)) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_normalized_firstfit = [(i * 10 / 100, average(lprom_firstfit[i]) / (sat_capacity * sat_num)) for i in range(len(PR_SAT_DO_MISSION))]
        lprom_normalized_bestfit = [(i * 10 / 100, average(lprom_bestfit[i]) / (sat_capacity * sat_num)) for i in range(len(PR_SAT_DO_MISSION))]

        fname = 'Exp-Offline-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        print_str_to_file(str(lprom_offline), os.path.join(OUTPUT_DIR, fname + '.txt'))
        #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, fname), ftype='png')


        fname = 'Exp-Online-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        print_str_to_file(str(lprom_online), os.path.join(OUTPUT_DIR, fname + '.txt'))
        #plot_function(lprom_online, os.path.join(OUTPUT_DIR, fname), ftype='png')

        fname = 'Exp-OnlineRandom-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        print_str_to_file(str(lprom_ramdom), os.path.join(OUTPUT_DIR, fname + '.txt'))
        #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, fname), ftype='png')

        fname = 'Exp-FirstFit-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, fname + '.txt'))
        #plot_function(lprom_online, os.path.join(OUTPUT_DIR, fname), ftype='png')

        fname = 'Exp-BestFit-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, fname + '.txt'))
        #plot_function(lprom_online, os.path.join(OUTPUT_DIR, fname), ftype='png')


        fname = 'Exp-Comparison-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        plot_nfunctions([lprom_offline, lprom_online, lprom_ramdom, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, fname),
                        labels=['Offline LP', 'Online Greedy', 'Online Random', 'FirstFit', 'BestFit'],
                        xlabel='Probability of each satellite is able to fulfill a given mission',
                        ylabel='Number of total assigned capacity', colors=['red', 'blue', 'lightblue', 'green', 'magenta'], ftype='png')
        plot_nfunctions([lprom_offline, lprom_online, lprom_ramdom, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, fname),
                        labels=['Offline LP', 'Online Greedy', 'Online Random', 'FirstFit', 'BestFit'],
                        xlabel='Probability of each satellite is able to fulfill a given mission',
                        ylabel='Number of total assigned capacity', colors=['red', 'blue', 'lightblue', 'green', 'magenta'], ftype='svg')

        fname = 'Exp-Comparison-sat_num:%d,mission_num:%d,sat_capacity:%d,categories:%d,rep:%d,seed:%d'%(sat_num, mission_num, sat_capacity, CATEGORIES, REPETITIONS, SEED)
        plot_nfunctions([lprom_normalized_offline, lprom_normalized_online, lprom_normalized_random, lprom_normalized_firstfit, lprom_normalized_bestfit], os.path.join(OUTPUT_DIR,'normalized-NONRandomized', fname),
                        labels=['Offline LP', 'Online Greedy', 'Online Random', 'FirstFit', 'BestFit'],
                        xlabel='Probability of each satellite is able to fulfill a given mission',
                        ylabel='Normalized network capacity assignation', colors=['red', 'blue', 'lightblue', 'green', 'magenta'], ftype='png')
        plot_nfunctions([lprom_normalized_offline, lprom_normalized_online,lprom_normalized_random, lprom_normalized_firstfit, lprom_normalized_bestfit], os.path.join(OUTPUT_DIR,'normalized-NONRandomized', fname),
                        labels=['Offline LP', 'Online Greedy', 'Online Random', 'FirstFit', 'BestFit'],
                        xlabel='Probability of each satellite is able to fulfill a given mission',
                        ylabel='Normalized network capacity assignation', colors=['red', 'blue', 'lightblue', 'green', 'magenta'], ftype='svg')