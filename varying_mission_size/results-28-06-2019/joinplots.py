import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'normalized-NONRandomized/Exp-Comparison-sat_num:10,mission_num:20,sat_capacity:6,categories:5,rep:25,seed:245.svg',
    'normalized-NONRandomized/Exp-Comparison-sat_num:10,mission_num:30,sat_capacity:9,categories:5,rep:25,seed:245.svg',
    'normalized-NONRandomized/Exp-Comparison-sat_num:100,mission_num:200,sat_capacity:6,categories:5,rep:25,seed:245.svg',
    'normalized-NONRandomized/Exp-Comparison-sat_num:100,mission_num:300,sat_capacity:9,categories:5,rep:25,seed:245.svg',
]

fname = 'comparison-sat_num:[10,100]'
#combine_into_table_and_save(2,2,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('[ -f %s ] || inkscape --export-png=%s %s'%(fname + '.png', fname + '.png', fname + '.svg'))