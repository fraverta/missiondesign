import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'Exp-Comparison-sat_num:10,pr:0.10,mission_num:60,mission_per_sat:10,rep:35,seed:245.svg',
    'Exp-Comparison-sat_num:10,pr:0.50,mission_num:60,mission_per_sat:10,rep:35,seed:245.svg',
    'Exp-Comparison-sat_num:10,pr:1.00,mission_num:60,mission_per_sat:10,rep:35,seed:245.svg',

    'Exp-Comparison-sat_num:100,pr:0.10,mission_num:600,mission_per_sat:10,rep:35,seed:245.svg',
    'Exp-Comparison-sat_num:100,pr:0.50,mission_num:600,mission_per_sat:10,rep:35,seed:245.svg',
    'Exp-Comparison-sat_num:100,pr:1.00,mission_num:600,mission_per_sat:10,rep:35,seed:245.svg',

    'Exp-Comparison-sat_num:300,pr:0.10,mission_num:1800,mission_per_sat:10,rep:35,seed:245.svg',
    'Exp-Comparison-sat_num:300,pr:0.50,mission_num:1800,mission_per_sat:10,rep:35,seed:245.svg',
    'Exp-Comparison-sat_num:300,pr:1.00,mission_num:1800,mission_per_sat:10,rep:35,seed:245.svg',
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,100,300],pr:[0.1,0.5,1.0]-varyingload-orderedlowfirst'
combine_into_table_and_save(3,3,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
