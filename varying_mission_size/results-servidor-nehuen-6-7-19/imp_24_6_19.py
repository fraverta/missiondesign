from typing import Dict, List
import random
import networkx as nx
from abc import abstractmethod
from pulp import *
import time


class Scheme_24_6_19:

    def __init__(self, sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float, mission_categories_num: int, f_mission_cost=None, seed=0):
        assert mission_categories_num is None or mission_num % mission_categories_num == 0, 'The number of missions must be divided by the number of mission categories'

        self._sat_num = sat_num
        self._mission_num = mission_num
        self._sat_capacity = sat_capacity
        self._pr_sat_to_do_mission = pr_sat_to_do_mission
        self._random = random.Random(seed)
        self._mission_categories_num = mission_categories_num

        if f_mission_cost is None:
            self.mission_capacity = lambda m_id: m_id % self._mission_categories_num + 1
        else:
            self.mission_capacity = f_mission_cost


    def _generate_scenario(self) -> Dict[(int, List[int])]:
        '''

        :return: Sat -> missions it is able to handle
        '''
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in range(self._mission_num):
                if self._random.random() < self._pr_sat_to_do_mission:
                    result[s].append(m)

        return result

    @abstractmethod
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        pass

    @staticmethod
    @abstractmethod
    def run_experiment(sat_num: int, mission_num: int, mission_per_sat: int, pr_sat_to_do_mission: float,number_of_runs, seed=0) -> List[int]:
        pass

    @staticmethod
    def _run_experiment(sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float, mission_categories_num: int,
                        number_of_runs, exp: 'Scheme_27_5_19') -> List[int]:

        result = []
        for i in range(number_of_runs):
            scenario = exp._generate_scenario()
            result.append(exp._compute_mission_assignment(scenario)[0])

        return result

class Online(Scheme_24_6_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(self._mission_num):
            sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1], reverse=True)
            for sat, remained_capacity in sat_ordered_by_busy_buffer:
                if remained_capacity - self.mission_capacity(m) >= 0 and m in scenario[sat]:
                    sat_assignment[sat]['assigned_missions'].append(m)
                    sat_assignment[sat]['remained_capacity'] -= self.mission_capacity(m)
                    assigned_capacity += self.mission_capacity(m)
                    break

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("Online_Assignment:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d-Pr:%.2f" % (elapsed_time, self._sat_num, self._mission_num, self._sat_capacity, self._pr_sat_to_do_mission))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float, mission_categories_num, number_of_runs:int, f_mission_cost=None, seed=0) -> List[int]:
        exp = Online(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission,mission_categories_num, f_mission_cost=f_mission_cost, seed=seed)
        return exp._run_experiment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num, number_of_runs, exp)


class OnlineRandomAssignment(Scheme_24_6_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(self._mission_num):
            eligible_sats = [sat for sat in range(self._sat_num) if sat_assignment[sat]['remained_capacity'] >= self.mission_capacity(m) and m in scenario[sat]]

            if len(eligible_sats) > 0:
                sat = random.choice(eligible_sats)
                sat_assignment[sat]['assigned_missions'].append(m)
                sat_assignment[sat]['remained_capacity'] -= self.mission_capacity(m)
                assigned_capacity += self.mission_capacity(m)

        assert all(sum([self.mission_capacity(m) for m in sat_assignment[k]['assigned_missions']]) <= self._sat_capacity for k in range(self._sat_num)), 'Each satellite must have atmost %d missions assigned' % (self._sat_capacity)
        assert all(m in scenario[sat] for sat in range(self._sat_num) for m in sat_assignment[sat]['assigned_missions']), 'Each satellite must be able to perform an assigned mission in current scenario'

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("Online_Random_Assignment:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d-Pr:%.2f"%(elapsed_time, self._sat_num, self._mission_num, self._sat_capacity, self._pr_sat_to_do_mission))

        return assigned_capacity, res

    @staticmethod
    def _run_experiment(number_of_runs: int, number_of_runs_per_scenario: int, exp: 'OnlineRandomAssignment') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario = exp._generate_scenario()
            scenario_result = 0.
            for j in range(number_of_runs_per_scenario):
                scenario_result += exp._compute_mission_assignment(scenario)[0]
            result.append(scenario_result / number_of_runs_per_scenario)

        return result

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float, mission_categories_num, number_of_runs:int, runs_per_scenario:int, f_mission_cost=None, seed=0) -> List[int]:
        exp = OnlineRandomAssignment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num, f_mission_cost=f_mission_cost, seed=seed)
        return exp._run_experiment(number_of_runs, runs_per_scenario, exp)


class Offline_LP(Scheme_24_6_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_mission_idx = ['Sat%d_M%d' % (sat, m) for sat in range(self._sat_num) for m in scenario[sat]]

        lp_problem = LpProblem("The Mission Problem", LpMaximize)
        sat_capacity_vars = LpVariable.dicts("S_Sat", list(range(self._sat_num)), 0, self._sat_capacity, LpInteger)
        sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)
        mission_vars = LpVariable.dicts("M", list(range(self._mission_num)), cat=LpBinary)

        lp_problem += lpSum([ self.mission_capacity(m) * mission_vars[m] for m in range(self._mission_num)]), "Total assigned capacity"
        for sat in range(self._sat_num):
            lp_problem += sat_capacity_vars[sat] == lpSum([self.mission_capacity(m) * sat_mission_vars['Sat%d_M%d'%(sat, m)] for m in scenario[sat]]), 'Capacity assigned Sat %d equal to mission assigned'%(sat)
        for m in range(self._mission_num):
            sat_able_to_fulfill_m = [sat for sat in range(self._sat_num) if m in scenario[sat]]
            lp_problem += mission_vars[m] == lpSum([sat_mission_vars['Sat%d_M%d'%(sat, m)] for sat in sat_able_to_fulfill_m]), 'Capacity assigned equal to mission %d assigned'%(m)

        lp_problem.solve(CPLEX())
        assert lp_problem.status == LpStatusOptimal

        sat_assignment = dict((sat, []) for sat in range(self._sat_num))
        for v in sat_mission_vars.values():
            if v.varValue:
                sat = int(v.name[4: v.name.rindex('_')])
                mission = int(v.name[v.name.rindex('M') + 1:])
                sat_assignment[sat].append(mission)

        elapsed_time = time.time() - start_time
        print("Offline_LP:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d-Pr:%.2f" % (elapsed_time, self._sat_num, self._mission_num, self._sat_capacity, self._pr_sat_to_do_mission))
        return value(lp_problem.objective), sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float, mission_categories_num, number_of_runs:int, f_mission_cost=None, seed=0) -> List[int]:
        exp = Offline_LP(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num, f_mission_cost=f_mission_cost, seed=seed)
        return exp._run_experiment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num, number_of_runs, exp)

class FirstFit(Scheme_24_6_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(self._mission_num):
            for sat in range(self._sat_num):
                if sat_assignment[sat]['remained_capacity'] - self.mission_capacity(m) >= 0 and m in scenario[sat]:
                    sat_assignment[sat]['assigned_missions'].append(m)
                    sat_assignment[sat]['remained_capacity'] -= self.mission_capacity(m)
                    assigned_capacity += self.mission_capacity(m)
                    break

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d-Pr:%.2f" % (elapsed_time, self._sat_num, self._mission_num, self._sat_capacity, self._pr_sat_to_do_mission))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float, mission_categories_num, number_of_runs:int, f_mission_cost=None, seed=0) -> List[int]:
        exp = FirstFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission,mission_categories_num, f_mission_cost=f_mission_cost, seed=seed)
        return exp._run_experiment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num, number_of_runs, exp)



class BestFit(Scheme_24_6_19):
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(self._mission_num):
            sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1])
            for sat, remained_capacity in sat_ordered_by_busy_buffer:
                if remained_capacity - self.mission_capacity(m) >= 0 and m in scenario[sat]:
                    sat_assignment[sat]['assigned_missions'].append(m)
                    sat_assignment[sat]['remained_capacity'] -= self.mission_capacity(m)
                    assigned_capacity += self.mission_capacity(m)
                    break

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d-Pr:%.2f" % (
        elapsed_time, self._sat_num, self._mission_num, self._sat_capacity, self._pr_sat_to_do_mission))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, mission_num: int, sat_capacity: int, pr_sat_to_do_mission: float,
                       mission_categories_num, number_of_runs: int, f_mission_cost=None, seed=0) -> List[int]:
        exp = BestFit(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num,
                       f_mission_cost=f_mission_cost, seed=seed)
        return exp._run_experiment(sat_num, mission_num, sat_capacity, pr_sat_to_do_mission, mission_categories_num,
                                   number_of_runs, exp)