import sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from varying_mission_size.imp_24_6_19 import OnlineRandomAssignment, Offline_LP, Online, FirstFit, BestFit
import time
start_time = time.time()


#OUTPUT_DIR = 'results-%s'%(date.today().strftime("%d-%m-%Y"))
OUTPUT_DIR = 'results-varyingload-ordered_missions-06-07-2019'#'results-varyingload-ordered_missions-%s'%(date.today().strftime("%d-%m-%Y"))
SAT_NUM_RANGE = [10, 100, 300]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
PR_SAT_DO_MISSION = [0.1,0.5,1.0]
REPETITIONS = 35
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + OUTPUT_DIR + '/png')
os.system('mkdir ' + OUTPUT_DIR + '/svg')
os.system('mkdir ' + OUTPUT_DIR + '/txt')
for sat_num in SAT_NUM_RANGE:
    system_capacity = sat_num * SAT_CAPACITY
    for pr in PR_SAT_DO_MISSION:
        results_offline_lp = []
        results_online_greedy = []
        results_online_random = []
        results_firstfit = []
        results_bestfit = []
        for offered_load in MISSION_PROPORTION:
            mission_cost_values = [5] * int(0.5 * offered_load * system_capacity // 5) + [1] * int(offered_load * system_capacity * 0.5)
            mission_num = len(mission_cost_values);
            mission_cost_dict = dict(zip(range(mission_num), mission_cost_values))
            f_mission_cost = lambda m: mission_cost_dict[m]; assert sum(f_mission_cost(m) for m in range(mission_num)) == offered_load * system_capacity, "Sum(mission cost) different of the required offered load"
            print("[Running] sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,offeredload:%.2f,rep:%d " % (sat_num, pr, mission_num, SAT_CAPACITY, offered_load, REPETITIONS))

            # results_offline_lp.append(Offline_LP.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))
            # results_online_greedy.append(Online.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))
            # results_online_random.append(OnlineRandomAssignment.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None, REPETITIONS,  RUNS_PER_SCENARIO, f_mission_cost=f_mission_cost, seed=SEED))
            # results_firstfit.append(FirstFit.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None,  REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))
            # results_bestfit.append(BestFit.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, None, REPETITIONS, f_mission_cost=f_mission_cost, seed=SEED))

        # lprom_offline_lp = [(MISSION_PROPORTION[i], average(results_offline_lp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_online_greedy = [(MISSION_PROPORTION[i], average(results_online_greedy[i])/ system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_online_random =[(MISSION_PROPORTION[i], average(results_online_random[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_firstfit = [(MISSION_PROPORTION[i], average(results_firstfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_bestfit = [(MISSION_PROPORTION[i], average(results_bestfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]


        fname = 'Exp-OfflineLP-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_offline_lp), os.path.join(OUTPUT_DIR, 'txt' , fname + '.txt'))
        lprom_offline_lp = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')


        fname = 'Exp-OnlineGreedy-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_online_greedy), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        lprom_online_greedy = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')

        fname = 'Exp-OnlineRandom-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')


        fname = 'Exp-firstfit-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt' , fname + '.txt'))
        lprom_firstfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(firstfit, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')

        fname = 'Exp-bestfit-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt' , fname + '.txt'))
        lprom_bestfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(bestfit, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')

        fname = 'Exp-Comparison-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
                        labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                        xlabel='Offered Load',
                        ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='png')
        plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
                        labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                        xlabel='Offered Load',
                        ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg')


print("Elapsed time: %f seconds"%(time.time() - start_time))