import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    #'Exp-Comparison-sat_num:10,mission_num:10,sat_capacity:3,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:10,mission_num:20,sat_capacity:6,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:10,mission_num:30,sat_capacity:9,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:10,mission_num:40,sat_capacity:12,categories:5,rep:25,seed:245.svg',

    #'Exp-Comparison-sat_num:50,mission_num:50,sat_capacity:3,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:50,mission_num:100,sat_capacity:6,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:50,mission_num:150,sat_capacity:9,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:50,mission_num:200,sat_capacity:12,categories:5,rep:25,seed:245.svg',

    #'Exp-Comparison-sat_num:100,mission_num:100,sat_capacity:3,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:100,mission_num:200,sat_capacity:6,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:100,mission_num:300,sat_capacity:9,categories:5,rep:25,seed:245.svg',
    'Exp-Comparison-sat_num:100,mission_num:400,sat_capacity:12,categories:5,rep:25,seed:245.svg',
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,50,100],mission_categories=5,mission_num[2x,3x,4x]-varyingpr'
combine_into_table_and_save(3,3,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%( fname + '.png', fname + '.svg'))
