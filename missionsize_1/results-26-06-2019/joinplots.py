import os
from svgtogrid import combine_into_table_and_save

# files_m1 = [
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:10,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:20,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:30,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:40,mission_per_sat:1,rep:35,seed:245.svg',
#
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:50,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:100,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:150,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:200,mission_per_sat:1,rep:35,seed:245.svg',
#
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:100,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:200,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:300,mission_per_sat:1,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:400,mission_per_sat:1,rep:35,seed:245.svg',
# ]
#
# fname = 'ComparisonNormalized-sat_num:[10,50,100],missions=1'
# combine_into_table_and_save(3,4,0,0,files_m1, fname + '.svg')
# #os.system('inkscape %s &'% (fname + '.svg') )
# os.system('inkscape --export-png=%s %s'%( fname + '.png', fname + '.svg'))
#
#
# files_m1 = [
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:10,mission_per_sat:10,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:20,mission_per_sat:20,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:30,mission_per_sat:30,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:40,mission_per_sat:40,rep:35,seed:245.svg',
#
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:50,mission_per_sat:50,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:100,mission_per_sat:100,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:150,mission_per_sat:150,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:200,mission_per_sat:200,rep:35,seed:245.svg',
#
#
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:100,mission_per_sat:100,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:200,mission_per_sat:200,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:300,mission_per_sat:300,rep:35,seed:245.svg',
# 'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:400,mission_per_sat:400,rep:35,seed:245.svg',
# ]
#
# fname = 'ComparisonNormalized-sat_num:[10,50,100],missions=M'
# combine_into_table_and_save(3,4,0,0,files_m1, fname + '.svg')
# #os.system('inkscape %s &'%(fname + '.svg'))
# os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))



files_m1 = [
'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:10,mission_per_sat:1,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:20,mission_per_sat:2,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:30,mission_per_sat:3,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:10,mission_num:40,mission_per_sat:4,rep:35,seed:245.svg',

'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:50,mission_per_sat:1,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:100,mission_per_sat:2,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:150,mission_per_sat:3,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:50,mission_num:200,mission_per_sat:4,rep:35,seed:245.svg',

'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:100,mission_per_sat:1,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:200,mission_per_sat:2,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:300,mission_per_sat:3,rep:35,seed:245.svg',
'svg/Exp-ComparisonNormalized-sat_num:100,mission_num:400,mission_per_sat:4,rep:35,seed:245.svg',
]

fname = 'ComparisonNormalized-AdjustedYTicks-sat_num:[10,50,100],missions=MoverN'
combine_into_table_and_save(3,4,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'%(fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

'''

files_m1 = [
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:10,pr:0.10,mission_num:100,mission_per_sat:10,rep:50,seed:245.svg',
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:100,pr:0.10,mission_num:1000,mission_per_sat:10,rep:50,seed:245.svg',
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:1000,pr:0.10,mission_num:10000,mission_per_sat:10,rep:25,seed:245.svg',
]

fname = '../results-24-06-2019/ComparisonNormalized-sat_num:[10,100,100],missions=10,pr:0.1'
combine_into_table_and_save(1,3,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'%(fname + 'svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

files_m1 = [
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:10,pr:0.50,mission_num:100,mission_per_sat:10,rep:50,seed:245.svg',
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:100,pr:0.50,mission_num:1000,mission_per_sat:10,rep:50,seed:245.svg',
]

fname = '../results-24-06-2019/ComparisonNormalized-sat_num:[10,100],missions=10,pr:0.5'
combine_into_table_and_save(1,2,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'%(fname + 'svg'))
os.system(' inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

files_m1 = [
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:10,pr:1.00,mission_num:100,mission_per_sat:10,rep:50,seed:245.svg',
'../results-24-06-2019/svg/Exp-ComparisonNormalized-sat_num:100,pr:1.00,mission_num:1000,mission_per_sat:10,rep:50,seed:245.svg',
]

fname = '../results-24-06-2019/ComparisonNormalized-sat_num:[10,100],missions=10,pr:1.00'
combine_into_table_and_save(1,2,0,0,files_m1, fname + '.svg')
#os.system('inkscape %s &'%(fname + 'svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
'''