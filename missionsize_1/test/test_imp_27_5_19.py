import unittest
from missionsize_1.imp_27_5_19 import Offline, Online, Offline_LP, Online_Random_Assignment, FirstFit, BestFit, GenericMissionGenerator, EasyHardMissionGenerator, EasyHardMissionGenerator2
from functools import reduce

class Test_imp_27_5_19(unittest.TestCase):

    def test_seed_generate_same_set_of_experiments(self):
        for seed in range(10):
            exp1 = Offline(50, 100, 2, 0.5, seed=seed)
            exp2 = Online(50, 100, 2, 0.5, seed=seed)

            for exp in range(100):
                self.assertEqual(exp1._generate_scenario(), exp2._generate_scenario(), 'Same seed but, Scenario %d are differents'%exp)

    def test_offline_assignement1(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        offline = Offline(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[1]}
        num, matching = offline._compute_mission_assignment(scenario)
        self.assertEqual(2, num, 'Matching fail')
        self.assertEqual({0:[0], 1:[1]}, matching, 'The matching that was found is not correct')


    def test_offline_assignement2(self):
        sat_num = 3; mission_num = 6; mission_per_sat = 2; pr_sat_to_do_mission = 0.5;
        offline = Offline(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0,1], 1: [1,2,3], 2: [3,4,5]}
        num, matching = offline._compute_mission_assignment(scenario)
        for k in matching.keys(): matching[k].sort();
        self.assertEqual(6, num, 'Matching fail')
        self.assertEqual({0:[0,1], 1:[2,3], 2:[4,5]}, matching, 'The matching that was found is not correct')


    def test_offline_assignement3(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        offline = Offline(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0,1,2,4], 1: [3,4,5], 2: [2,5,6,7,8]}
        num, matching = offline._compute_mission_assignment(scenario)
        for k in matching.keys(): matching[k].sort();
        self.assertEqual(9, num, 'Matching fail')
        self.assertEqual({0:[0,1,2], 1:[3,4,5], 2:[6,7,8]}, matching, 'The matching that was found is not correct')

    def test_online_assignement1(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        online = Online(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[1]}
        num, assignation = online._compute_mission_assignment(scenario)
        self.assertEqual(2, num, 'Online assignment fail')
        self.assertEqual({0:[0], 1:[1]}, assignation, 'The online assignement that was found is not correct')

    def test_online_assignement2(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        online = Online(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[0]}
        num, assignation = online._compute_mission_assignment(scenario)
        self.assertEqual(1, num, 'Online assignment fail')
        self.assertEqual({0:[0], 1:[]}, assignation, 'The online assignement that was found is not correct')

    def test_online_assignement3(self):
        sat_num = 3; mission_num = 6; mission_per_sat = 2; pr_sat_to_do_mission = 0.5;
        online = Online(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 1], 1: [1, 2, 3], 2: [3, 4, 5]}
        num, assignation = online._compute_mission_assignment(scenario)
        self.assertEqual(5, num, 'Online assignment fail')
        self.assertEqual({0: [0], 1: [1,2], 2: [3, 4]}, assignation, 'The online assignement that was found is not correct')

    def test_online_assignement4(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        online = Online(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0,1,2,4], 1: [3,4,5], 2: [2,5,6,7,8]}
        num, assignation = online._compute_mission_assignment(scenario)
        self.assertEqual(7, num, 'Online assignment fail')
        self.assertEqual({0: [0, 1], 1: [3,4], 2: [2,5,6]}, assignation, 'The online assignement that was found is not correct')


    def test_offline_LPassignement_1(self):
        sat_num = 2; mission_num = 4; mission_per_sat = 2; pr_sat_to_do_mission = 0.5;
        offline_lp = Offline_LP(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0,1], 1: [1,2,3]}
        num, assignation = offline_lp._compute_mission_assignment(scenario)
        self.assertEqual(4, num, 'Online assignment fail')
        self.assertEqual({0: [0, 1], 1: [2,3]}, assignation, 'The lp assignement that was found is not correct')


    def test_offline_Random_Assignment_1(self):
        sat_num = 2; mission_num = 4; mission_per_sat = 2; pr_sat_to_do_mission = 0.5;
        offline_lp = Online_Random_Assignment(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0,1], 1: [1,2,3]}
        num, assignation = offline_lp._compute_mission_assignment(scenario)

        self.assertEqual(num, len(reduce(lambda x,y: x + y, assignation.values())))
        assert all(len(assignation[k]) <= mission_per_sat for k in range(sat_num)), 'Each satellite must have atmost %d missions assigned'%(self.mission_per_sat)
        assert all(m in scenario[sat] for sat in range(sat_num) for m in assignation[sat]), 'Each satellite must be able to perform an assigned mission in current scenario'


    '''
        TEST FIRSTFIT
    '''

    def test_firstfit_assignement1(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        ff = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[1]}
        num, assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(2, num, 'FirstFit assignment fail')
        self.assertEqual({0:[0], 1:[1]}, assignation, 'The firstfit assignment that was found is not correct')

    def test_firstfit_assignement2(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        ff = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[0]}
        num, assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(1, num, 'FirstFit assignment fail')
        self.assertEqual({0:[0], 1:[]}, assignation, 'The FirstFit assignment that was found is not correct')

    def test_firstfit_assignement3(self):
        sat_num = 3; mission_num = 6; mission_per_sat = 2; pr_sat_to_do_mission = 0.5;
        ff = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 1], 1: [1, 2, 3], 2: [3, 4, 5]}
        num, assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(6, num, 'FirstFit assignment fail')
        self.assertEqual({0: [0,1], 1: [2,3], 2: [4,5]}, assignation, 'The FirstFit assignment that was found is not correct')

    def test_firstfit_assignement4(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        ff = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 1, 2, 4], 1: [3, 4, 5], 2: [2, 5, 6, 7, 8]}
        num, assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(9, num, 'FirstFit assignment fail')
        self.assertEqual({0: [0, 1, 2], 1: [3,4,5], 2: [6,7, 8]}, assignation, 'The FirstFit assignement that was found is not correct')

    def test_firstfit_assignement5(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        ff = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 7, 8], 1: [3, 5, 6, 8], 2: [1, 2, 5, 6, 7, 8]}
        num, assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(8, num, 'FirstFit assignment fail')
        self.assertEqual({0: [0, 7, 8], 1: [3, 5, 6], 2: [1, 2]}, assignation, 'The FirstFit assignement that was found is not correct')

    '''
        TEST BESTTFIT
    '''

    def test_bestfit_assignement1(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        ff = FirstFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[1]}
        num, assignation = ff._compute_mission_assignment(scenario)
        self.assertEqual(2, num, 'BestFit assignment fail')
        self.assertEqual({0:[0], 1:[1]}, assignation, 'The BestFit assignment that was found is not correct')

    def test_bestfit_assignement2(self):
        sat_num = 2; mission_num = 2; mission_per_sat = 1; pr_sat_to_do_mission = 0.5;
        bf = BestFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0:[0,1], 1:[0]}
        num, assignation = bf._compute_mission_assignment(scenario)
        self.assertEqual(1, num, 'BestFit assignment fail')
        self.assertEqual({0:[0], 1:[]}, assignation, 'The BestFit assignment that was found is not correct')

    def test_bestfit_assignement3(self):
        sat_num = 3; mission_num = 6; mission_per_sat = 2; pr_sat_to_do_mission = 0.5;
        bf = BestFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 1], 1: [1, 2, 3], 2: [3, 4, 5]}
        num, assignation = bf._compute_mission_assignment(scenario)
        self.assertEqual(6, num, 'BestFit assignment fail')
        self.assertEqual({0: [0, 1], 1: [2, 3], 2: [4, 5]}, assignation, 'The BestFit assignment that was found is not correct')

    def test_bestfit_assignement4(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        bf = BestFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 1, 2, 4], 1: [3, 4, 5], 2: [2, 5, 6, 7, 8]}
        num, assignation = bf._compute_mission_assignment(scenario)
        self.assertEqual(9, num, 'BestFit assignment fail')
        self.assertEqual({0: [0, 1, 2], 1: [3,4,5], 2: [6,7, 8]}, assignation, 'The BestFit assignement that was found is not correct')

    def test_bestfit_assignement5(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        bf = BestFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 7], 1: [3, 5, 6, 8], 2: [2, 5, 6, 7, 8]}
        num, assignation = bf._compute_mission_assignment(scenario)
        self.assertEqual(7, num, 'BestFit assignment fail')
        self.assertEqual({0: [0, 7], 1: [3, 5, 6], 2: [2, 8]}, assignation, 'The BestFit assignement that was found is not correct')

    def test_bestfit_assignement6(self):
        sat_num = 3; mission_num = 9; mission_per_sat = 3; pr_sat_to_do_mission = 0.5;
        bf = BestFit(sat_num, mission_num, mission_per_sat, pr_sat_to_do_mission)
        scenario = {0: [0, 8], 1: [3, 5, 6, 8], 2: [2, 5, 6, 7, 8]}
        num, assignation = bf._compute_mission_assignment(scenario)
        self.assertEqual(7, num, 'BestFit assignment fail')
        self.assertEqual({0: [0], 1: [3, 5, 6], 2: [2, 7, 8]}, assignation, 'The BestFit assignement that was found is not correct')

    '''
    Test MissionGenerators
    '''
    def test_GenericMissionGenerator(self):
        mgen1 = GenericMissionGenerator(10, 100, 0.5, seed=10)
        mgen2 = GenericMissionGenerator(10, 100, 0.5, seed=10)

        self.assertEqual([mgen1.generate_scenario() for i in range(10)], [mgen2.generate_scenario() for i in range(10)])

    def test_EasyHardMissionGenerator(self):
        sat_num = 10; mission_num=100; easy_proportion = 0.3;
        mgen1 = EasyHardMissionGenerator(sat_num, mission_num, easy_proportion, seed=10)
        mgen2 = EasyHardMissionGenerator(sat_num, mission_num, easy_proportion, seed=10)

        esc_1 = [mgen1.generate_scenario() for i in range(10)]
        esc_2 = [mgen2.generate_scenario() for i in range(10)]
        self.assertEqual(esc_1, esc_2)


        for scenario in esc_1:
            easy = 0
            hard = 0
            for m in range(mission_num):
                capable_sat_num = len(list(filter(lambda k: m in scenario[k], scenario.keys())))
                if capable_sat_num == 1:
                    hard += 1
                elif capable_sat_num == 10:
                    easy += 1
                else:
                    self.assertTrue(False)

            self.assertEqual(easy, 30)
            self.assertEqual(hard, 70)


    def test_EasyHardMissionGenerator2(self):
        sat_num = 10; mission_num=100; easy_proportion = 0.3;
        mgen1 = EasyHardMissionGenerator2(sat_num, mission_num, easy_proportion, 0.9, 0.1, seed=10)
        mgen2 = EasyHardMissionGenerator2(sat_num, mission_num, easy_proportion, 0.9, 0.1, seed=10)

        esc_1 = [mgen1.generate_scenario() for i in range(10)]
        esc_2 = [mgen2.generate_scenario() for i in range(10)]
        self.assertEqual(esc_1, esc_2)




