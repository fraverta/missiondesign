from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from missionsize_1.imp_27_5_19 import Online_Random_Assignment, Offline_LP, Offline, Online, FirstFit, BestFit
import time

start_time = time.time()


OUTPUT_DIR = 'results-04-07-2019' #'results-%s'%(date.today().strftime("%d-%m-%Y"))
SAT_NUM_RANGE = [10,50,100]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(0,110,10)]
PR_SAT_DO_MISSION = [0.1, 0.5, 1.0]
REPETITIONS = 50
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 20 #VALID FOR RANDOM ASSIGNMENT

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + OUTPUT_DIR + '/png')
os.system('mkdir ' + OUTPUT_DIR + '/svg')
os.system('mkdir ' + OUTPUT_DIR + '/txt')
for sat_num in SAT_NUM_RANGE:
    system_capacity = sat_num * SAT_CAPACITY
    for pr in PR_SAT_DO_MISSION:
        results_offline_lp = []
        results_offline_matching = []
        results_online_greedy = []
        results_online_random = []
        results_firstfit = []
        results_bestfit = []

        for offered_load in MISSION_PROPORTION:
            mission_num = int(sat_num * SAT_CAPACITY * offered_load); assert mission_num == sat_num * SAT_CAPACITY * offered_load;
            print("[Running] sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,offeredload:%.2f,rep:%d " % (sat_num, pr, mission_num, SAT_CAPACITY, offered_load, REPETITIONS))

            # results_offline_lp.append(Offline_LP.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, REPETITIONS, seed=SEED))
            # #results_offline_matching.append(Offline.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, REPETITIONS, seed=SEED))
            # results_online_greedy.append(Online.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, REPETITIONS, seed=SEED))
            # results_online_random.append(Online_Random_Assignment.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, REPETITIONS, RUNS_PER_SCENARIO, seed=SEED))
            # results_firstfit.append(FirstFit.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, REPETITIONS, seed=SEED))
            # results_bestfit.append(BestFit.run_experiment(sat_num, mission_num, SAT_CAPACITY, pr, REPETITIONS, seed=SEED))


        # lprom_offline_lp = [(MISSION_PROPORTION[i], average(results_offline_lp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # #lprom_offline_matching = [(MISSION_PROPORTION[i], average(results_offline_matching[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_online_greedy = [(MISSION_PROPORTION[i], average(results_online_greedy[i])/ system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_online_random =[(MISSION_PROPORTION[i], average(results_online_random[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_firstfit = [(MISSION_PROPORTION[i], average(results_firstfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        # lprom_bestfit = [(MISSION_PROPORTION[i], average(results_bestfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]

        fname = 'Exp-OfflineLP-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_offline_lp), os.path.join(OUTPUT_DIR, 'txt' , fname + '.txt'))
        #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')
        lprom_offline_lp = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

        # fname = 'Exp-OfflineMatching-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        # print_str_to_file(str(lprom_offline_matching), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        # #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')

        fname = 'Exp-OnlineGreedy-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_online_greedy), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
        lprom_online_greedy = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

        fname = 'Exp-OnlineRandom-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
        lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

        fname = 'Exp-FirstFit-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
        lprom_firstfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

        fname = 'Exp-BestFit-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        #print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
        lprom_bestfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))


        fname = 'Exp-Comparison-sat_num:%d,pr:%.2f,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, pr, mission_num, SAT_CAPACITY, REPETITIONS, SEED)
        plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
                        labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                        xlabel='Offered Load',
                        ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='png')
        plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
                        labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                        xlabel='Offered Load',
                        ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg')


print("Elapsed time: %f seconds"%(time.time() - start_time))