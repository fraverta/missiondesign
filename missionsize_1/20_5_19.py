import networkx as nx
import itertools
import matplotlib.pyplot as plt
import numpy as np

def get_cost(mission_id, sat_id, mission_num, max_mission_cost):
    return (mission_id + (mission_num - sat_id)) % mission_num % max_mission_cost + 1

def generate_bipartite_graph(sat_num, mission_num, missions_per_sat, max_mission_cost, TTL=None):
    bigraph = nx.Graph()
    bigraph.add_nodes_from(['%d_%d'%(n,m) for n in range(sat_num) for m in range(missions_per_sat)], bipartite=0)
    bigraph.add_nodes_from([m for m in range(mission_num)], bipartite=1)

    for n in range(sat_num):
        for mission in range(mission_num):
            for m in range(missions_per_sat):
                #cost = (mission + (mission_num - n)) % mission_num % max_mission_cost + 1
                cost = get_cost(mission, n, mission_num, max_mission_cost)
                if TTL is None or cost <= TTL:
                    bigraph.add_edge('%d_%d' % (n, m), mission, weight = -cost)

    return bigraph

def online_mission_assignement_del_time(missions, sat_num, missions_per_sat, max_mission_cost):
    sat_assignement = [[] for sat in range(sat_num)]
    total_cost = 0
    assigned_missions = 0
    for m in missions:
        cost = sorted([(sat, get_cost(m, sat, len(missions), max_mission_cost)) for sat in range(sat_num)], key=lambda x: x[1])
        i = 0
        while i<sat_num and len(sat_assignement[cost[i][0]]) >= missions_per_sat:
            i+=1

        if i<sat_num:
            sat_assignement[cost[i][0]].append(m)
            total_cost += cost[i][1]
            assigned_missions += 1

    return sat_assignement, total_cost, assigned_missions

def online_mission_assignement_min_busy_storage(missions, sat_num, missions_per_sat, max_mission_cost):
    sat_assignement = [[] for sat in range(sat_num)]
    total_cost = 0
    assigned_missions = 0
    for m in missions:
        cost = sorted([(sat, len(sat_assignement[sat])) for sat in range(sat_num)], key=lambda x: x[1])
        i = 0
        while i<sat_num and len(sat_assignement[cost[i][0]]) >= missions_per_sat:
            i += 1

        if i < sat_num:
            sat_assignement[cost[i][0]].append(m)
            total_cost += get_cost(m, cost[i][0], len(missions), max_mission_cost)
            assigned_missions += 1


    return sat_assignement, total_cost, assigned_missions


# g = generate_bipartite_graph(3, 4, 2, 4)
# for e in g.edges:
#     print('%s-> %.2f'%(e, g[e[0]][e[1]]['weight']))
#
# matching = nx.max_weight_matching(g, maxcardinality=True, weight='weight')
# print("Mathcing cost: %.2f"%sum(-g[e[0]][e[1]]['weight'] for e in matching))


SAT_NUM = 2
MISSION_PER_SAT = 2
MISSION_NUM = SAT_NUM * MISSION_PER_SAT
MAX_MISSION_COST = MISSION_NUM
TTL = MAX_MISSION_COST

g = generate_bipartite_graph(SAT_NUM, MISSION_NUM, MISSION_PER_SAT, MAX_MISSION_COST,TTL=TTL)
for e in g.edges:
    print('%s-> %.2f'%(e, g[e[0]][e[1]]['weight']))
matching = nx.max_weight_matching(g, maxcardinality=True, weight='weight')
print("Min Weighted Matching: %.2f"%sum(-g[e[0]][e[1]]['weight'] for e in matching))

#perms = itertools.permutations(range(MISSION_NUM))
#perms = [next(perms) for i in range(10000)]
cost_online_deltime = []
mission_num_deltime = []
for missions_list in itertools.permutations(range(MISSION_NUM)):
    assignement, cost, num_assigned_missions = online_mission_assignement_del_time(missions_list, SAT_NUM, MISSION_PER_SAT, MAX_MISSION_COST)
    cost_online_deltime.append(cost)
    mission_num_deltime.append(num_assigned_missions)

cost_online_min_busy_storage = []
mission_num_min_busy_storage = []
for missions_list in itertools.permutations(range(MISSION_NUM)):
    assignement, cost, num_assigned_missions = online_mission_assignement_min_busy_storage(missions_list, SAT_NUM, MISSION_PER_SAT, MAX_MISSION_COST)
    cost_online_min_busy_storage.append(cost)
    mission_num_min_busy_storage.append(num_assigned_missions)

counts, bins = np.histogram(cost_online_min_busy_storage)
plt.hist(bins[:-1], bins, weights=counts)
plt.title('Online assignement - Min Buffer Used')
plt.xlabel('Mission Sum(Cost)')
plt.ylabel('Times')
plt.show()

counts, bins = np.histogram(cost_online_deltime)
plt.hist(bins[:-1], bins, weights=counts)
plt.title('Online assignement - Delivery Time')
plt.xlabel('Mission Sum(Cost)')
plt.ylabel('Times')
plt.show()

counts, bins = np.histogram(mission_num_deltime)
plt.hist(bins[:-1], bins, weights=counts)
plt.title('Online assignement - Delivery Time')
plt.xlabel('Mission Number')
plt.ylabel('Times')
plt.show()

counts, bins = np.histogram(mission_num_min_busy_storage)
plt.hist(bins[:-1], bins, weights=counts)
plt.title('Online assignement - Min Buffer used')
plt.xlabel('Mission Number')
plt.ylabel('Times')
plt.show()

print("End")


'''
SAT_NUM = 20
MISSION_PER_SAT = 3
MISSION_NUM = SAT_NUM * MISSION_PER_SAT
MAX_MISSION_COST = MISSION_NUM

f = []
fcost = []
for ttl in range(0, 2 * MAX_MISSION_COST):
    g = generate_bipartite_graph(SAT_NUM, MISSION_NUM, MISSION_PER_SAT, MAX_MISSION_COST, TTL=ttl)
    # for e in g.edges:
    #     print('%s-> %.2f' % (e, g[e[0]][e[1]]['weight']))
    matching = nx.max_weight_matching(g, maxcardinality=True, weight='weight')
    fcost.append(sum(-g[e[0]][e[1]]['weight'] for e in matching))
    f.append(len(matching))

print(f)
print(fcost)
'''