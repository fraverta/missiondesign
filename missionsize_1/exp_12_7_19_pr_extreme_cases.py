#[Running] sat_num:10,mission_num:10,mission_per_sat:10,offeredload:0.10,easy_proportion0.90,rep:35

import sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from missionsize_1.imp_27_5_19 import Online_Random_Assignment, Offline_LP, Online, FirstFit, BestFit, EasyHardMissionGenerator

OUTPUT_DIR = 'results-%s-%s'%(os.path.basename(__file__), date.today().strftime("%d-%m-%Y"))
SCENARIO_PROPORTION = [{'HIGH_PR':.50, 'LOW_PR':.50}, {'HIGH_PR':.90, 'LOW_PR':.10}, {'HIGH_PR':.10, 'LOW_PR':.90}, {'HIGH_PR':.70, 'LOW_PR':.30}, {'HIGH_PR':.30, 'LOW_PR':.70}]
SAT_NUM_RANGE = [10, 50, 100]
OFFERED_LOAD = [x/100 for x in range(10,110,10)]

REPETITIONS = 1
#SEED = random.randint(0, 1000)
SEED = 245
MISSION_PER_SAT = 10
RUNS_PER_SCENARIO = 1 #VALID FOR RANDOM ASSIGNMENT

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + os.path.join(OUTPUT_DIR, 'svg'))
os.system('mkdir ' + os.path.join(OUTPUT_DIR, 'png'))
os.system('mkdir ' + os.path.join(OUTPUT_DIR, 'txt'))
for mission_proportion in SCENARIO_PROPORTION:
    for sat_num in SAT_NUM_RANGE:
            # results_offline = []
            results_online = []
            results_offlinelp = []
            results_online_random = []
            results_firstfit = []
            results_bestfit = []
            system_capacity = sat_num * MISSION_PER_SAT
            for offered_load in OFFERED_LOAD:
                mission_num = int(system_capacity * offered_load);
                assert mission_num == system_capacity * offered_load;
                print("[Running] sat_num:%d,mission_num:%d,mission_per_sat:%d,offeredload:%.2f,easy_proportion%.2f,rep:%d " % (sat_num, mission_num, MISSION_PER_SAT, offered_load, mission_proportion['LOW_PR'], REPETITIONS))

                scenario_generator = EasyHardMissionGenerator(sat_num, mission_num, mission_proportion['LOW_PR'], seed=SEED)
                results_online.append(Online.run_experiment(sat_num, mission_num, MISSION_PER_SAT, 0, REPETITIONS, mission_generator=scenario_generator, seed=SEED))

                scenario_generator = EasyHardMissionGenerator(sat_num, mission_num, mission_proportion['LOW_PR'], seed=SEED)
                results_offlinelp.append(Offline_LP.run_experiment(sat_num, mission_num, MISSION_PER_SAT, 0, REPETITIONS, mission_generator=scenario_generator, seed=SEED))

                scenario_generator = EasyHardMissionGenerator(sat_num, mission_num, mission_proportion['LOW_PR'], seed=SEED)
                results_online_random.append(Online_Random_Assignment.run_experiment(sat_num, mission_num, MISSION_PER_SAT, 1., REPETITIONS, RUNS_PER_SCENARIO, mission_generator=scenario_generator, seed=SEED))

                scenario_generator = EasyHardMissionGenerator(sat_num, mission_num, mission_proportion['LOW_PR'], seed=SEED)
                results_firstfit.append(FirstFit.run_experiment(sat_num, mission_num, MISSION_PER_SAT, 0., REPETITIONS, mission_generator=scenario_generator, seed=SEED))

                scenario_generator = EasyHardMissionGenerator(sat_num, mission_num, mission_proportion['LOW_PR'], seed=SEED)
                results_bestfit.append(BestFit.run_experiment(sat_num, mission_num, MISSION_PER_SAT, 0., REPETITIONS, mission_generator=scenario_generator, seed=SEED))


            lprom_online = [(OFFERED_LOAD[i], average(results_online[i])/ system_capacity) for i in range(len(OFFERED_LOAD))]
            lprom_offlinelp = [(OFFERED_LOAD[i], average(results_offlinelp[i]) / system_capacity) for i in range(len(OFFERED_LOAD))]
            lprom_online_random =[(OFFERED_LOAD[i], average(results_online_random[i]) / system_capacity) for i in range(len(OFFERED_LOAD))]
            lprom_firstfit = [(OFFERED_LOAD[i], average(results_firstfit[i]) / system_capacity) for i in range(len(OFFERED_LOAD))]
            lprom_bestfit = [(OFFERED_LOAD[i], average(results_bestfit[i])/ system_capacity) for i in range(len(OFFERED_LOAD))]



            fname = 'Exp-Online-sat_num:%d,mission_num:%d,mission_per_sat:%d,easy_proportion:%.2f,rep:%d,seed:%d'%(sat_num, mission_num, MISSION_PER_SAT, mission_proportion['LOW_PR'], REPETITIONS, SEED)
            print_str_to_file(str(lprom_online), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            #lprom_online = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-OnlineLP-sat_num:%d,mission_num:%d,mission_per_sat:%d,easy_proportion%.2f,rep:%d,seed:%d'%(sat_num, mission_num, MISSION_PER_SAT, mission_proportion['LOW_PR'], REPETITIONS, SEED)
            print_str_to_file(str(lprom_offlinelp), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            #lprom_offlinelp = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-OnlineRandom-sat_num:%d,mission_num:%d,mission_per_sat:%d,easy_proportion%.2f,rep:%d,seed:%d,runs_per_scenario:%d'%(sat_num, mission_num, MISSION_PER_SAT, mission_proportion['LOW_PR'], REPETITIONS, SEED, RUNS_PER_SCENARIO)
            print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_online_random, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            #lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-FirstFit-sat_num:%d,mission_num:%d,mission_per_sat:%d,easy_proportion%.2f,rep:%d,seed:%d'%(sat_num, mission_num, MISSION_PER_SAT, mission_proportion['LOW_PR'], REPETITIONS, SEED)
            print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            #lprom_firstfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))


            fname = 'Exp-BestFit-sat_num:%d,mission_num:%d,mission_per_sat:%d,easy_proportion%.2f,rep:%d,seed:%d'%(sat_num, mission_num, MISSION_PER_SAT, mission_proportion['LOW_PR'], REPETITIONS, SEED)
            print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            #lprom_bestfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))


            fname = 'Exp-Comparison-sat_num:%d,mission_num:%d,mission_per_sat:%d,easy_proportion%.2f,rep:%d,seed:%d'%(sat_num, mission_num, MISSION_PER_SAT, mission_proportion['LOW_PR'], REPETITIONS, SEED)
            plot_nfunctions([lprom_offlinelp, lprom_online, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                            xlabel='Probability of each satellite is able to fulfill a given mission',
                            ylabel='Number of total assigned missions', colors=['red', 'blue', 'lightblue','green', 'magenta'], ftype='png')
            plot_nfunctions([lprom_offlinelp, lprom_online, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                            xlabel='Probability of each satellite is able to fulfill a given mission',
                            ylabel='Number of total assigned missions', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg')