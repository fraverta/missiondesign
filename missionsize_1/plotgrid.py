import matplotlib
matplotlib.use("TkAgg")  # Do this before importing pyplot!
import matplotlib.pyplot as plt
from utilities.utils import getListFromFile
import os

SOURCE_DIR = '/home/fraverta/PycharmProjects/main/missionsize_1/txt_results'
TARGET_DIR = '/home/fraverta/Desktop/'

SAT_NUM =  [10,25,50]
MISSION_NUM_X = [1,2,3,4]
ALGORITHM = ['OnlineLP','Offline','Online','OnlineRandom']
COLORS =['red', 'blue','lightblue', 'orange']
LABELS = ['Offline LP', 'Offline Matching', 'Online Greedy', 'Online Random']
STYLES = ['--^', '--o', '--s', '--v', '--p', '--x', '--+']


# fig, axs = plt.subplots(nrows=3, ncols=4, sharex=True, sharey=True, tight_layout=True, figsize=(50, 25))
# for i, sat_num in enumerate(SAT_NUM):
#     for j,mission_x in enumerate(MISSION_NUM_X):
#         mission_num = sat_num * mission_x
#         mission_per_sat = mission_num / sat_num
#         lines = []
#         for idx, algorithm in enumerate(ALGORITHM):
#             try:
#                 fname = os.path.join(SOURCE_DIR,'Exp-%s-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:100,seed:245,runs_per_scenario:20.txt'%(algorithm,sat_num, mission_num, mission_per_sat))
#                 print("[Oppening] " + fname)
#                 f = getListFromFile(fname)
#             except:
#                 fname = os.path.join(SOURCE_DIR,'Exp-%s-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:100,seed:245.txt'%(algorithm,sat_num, mission_num, mission_per_sat))
#                 print("[Oppening] " + fname)
#                 f = getListFromFile(fname)
#             label = LABELS[idx]
#             color = COLORS[idx]
#             style = STYLES[idx]
#             line, = axs[i][j].plot([x[0] for x in f[1:]], [y[1] for y in f[1:]], style, label=label, color=color)
#             lines.append(line)
#             axs[i][j].grid(color='gray', linestyle='dashed')
#             if i==0 and j == 0:
#                 axs[i][j].legend(handles=lines, loc='upper left')
#
#             # Set common labels
#             axs[i][j].set_xlabel('Pr of fulfilling a mission')
#             axs[i][j].set_ylabel('Number of assigned missions')
#
#
#
#
# plt.savefig('/home/fraverta/Desktop/MoverSAT-MxSat.svg', papertype='a0')
# #plt.show()

fig, axs = plt.subplots(nrows=1, ncols=4, sharex=True, sharey=True, tight_layout=True, figsize=(50, 4))
for i, sat_num in enumerate(SAT_NUM):
    for j,mission_x in enumerate(MISSION_NUM_X):
        mission_num = sat_num * mission_x
        mission_per_sat = mission_num / sat_num
        lines = []
        for idx, algorithm in enumerate(ALGORITHM):
            try:
                fname = os.path.join(SOURCE_DIR,'Exp-%s-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:100,seed:245,runs_per_scenario:20.txt'%(algorithm,sat_num, mission_num, mission_per_sat))
                print("[Oppening] " + fname)
                f = getListFromFile(fname)
            except:
                fname = os.path.join(SOURCE_DIR,'Exp-%s-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:100,seed:245.txt'%(algorithm,sat_num, mission_num, mission_per_sat))
                print("[Oppening] " + fname)
                f = getListFromFile(fname)
            label = LABELS[idx]
            color = COLORS[idx]
            style = STYLES[idx]
            line, = axs[j].plot([x[0] for x in f[1:]], [y[1] for y in f[1:]], style, label=label, color=color)
            lines.append(line)
            axs[j].grid(color='gray', linestyle='dashed')
            if i==0 and j == 0:
                axs[j].legend(handles=lines, loc='upper left')

            # Set common labels
            axs[j].set_xlabel('Pr of fulfilling a mission')
            axs[j].set_ylabel('Number of assigned missions')




#plt.savefig('/home/fraverta/Desktop/Alltogheter.svg', papertype='a0')
plt.show()