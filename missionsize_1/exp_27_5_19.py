import sys
sys.path.insert(0,'/home/fraverta/PycharmProjects/main')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from missionsize_1.imp_27_5_19 import Online_Random_Assignment, Offline_LP, Online, FirstFit, BestFit

#OUTPUT_DIR = 'results-%s'%(date.today().strftime("%d-%m-%Y"))
OUTPUT_DIR = 'results-26-06-2019'
SAT_NUM_RANGE = [10,50,100]
MISSION_X_RANGE = range(1, 5)
PR_SAT_DO_MISSION = [x/100 for x in range(10,110,10)]
REPETITIONS = 35
#SEED = random.randint(0, 1000)
SEED = 245
#MISSION_PER_SAT_RANGE = range(1, )
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT
YTICKS = [(.5,1.01,0.1), (.85,1.01,0.05), (.92,1.01,0.02)]
os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + os.path.join(OUTPUT_DIR, 'svg'))
os.system('mkdir ' + os.path.join(OUTPUT_DIR, 'png'))
os.system('mkdir ' + os.path.join(OUTPUT_DIR, 'txt'))
for sat_num_index, sat_num in enumerate(SAT_NUM_RANGE):
    for xmission in MISSION_X_RANGE:
        mission_num = sat_num * xmission
        for mission_per_sat in [1, mission_num//sat_num, mission_num]:
            #results_offline = []
            # results_online = []
            # results_offlinelp = []
            # results_online_random = []
            # results_firstfit = []
            # results_bestfit = []
            for pr in PR_SAT_DO_MISSION:
                print("[Running] sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d - pr:%.2f"% (sat_num, mission_num, mission_per_sat, REPETITIONS, pr))
                #results_offline.append(Offline.run_experiment(sat_num, mission_num, mission_per_sat, pr, REPETITIONS, seed=SEED))
                # results_online.append(Online.run_experiment(sat_num, mission_num, mission_per_sat, pr, REPETITIONS, seed=SEED))
                # results_offlinelp.append(Offline_LP.run_experiment(sat_num, mission_num, mission_per_sat, pr, REPETITIONS, seed=SEED))
                # results_online_random.append(Online_Random_Assignment.run_experiment(sat_num, mission_num, mission_per_sat, pr, REPETITIONS, RUNS_PER_SCENARIO, seed=SEED))
                # results_firstfit.append(FirstFit.run_experiment(sat_num, mission_num, mission_per_sat, pr, REPETITIONS, seed=SEED))
                # results_bestfit.append(BestFit.run_experiment(sat_num, mission_num, mission_per_sat, pr, REPETITIONS, seed=SEED))


            #lprom_offline = [(PR_SAT_DO_MISSION[i], average(results_offline[i])) for i in range(len(PR_SAT_DO_MISSION))]
            # lprom_online = [(PR_SAT_DO_MISSION[i], average(results_online[i])) for i in range(len(PR_SAT_DO_MISSION))]
            # lprom_offlinelp = [(PR_SAT_DO_MISSION[i], average(results_offlinelp[i])) for i in range(len(PR_SAT_DO_MISSION))]
            # lprom_online_random =[(PR_SAT_DO_MISSION[i], average(results_online_random[i])) for i in range(len(PR_SAT_DO_MISSION))]
            # lprom_firstfit = [(PR_SAT_DO_MISSION[i], average(results_firstfit[i])) for i in range(len(PR_SAT_DO_MISSION))]
            # lprom_bestfit = [(PR_SAT_DO_MISSION[i], average(results_bestfit[i])) for i in range(len(PR_SAT_DO_MISSION))]

            # fname = 'Exp-Offline-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            # print_str_to_file(str(lprom_offline), os.path.join(OUTPUT_DIR, fname + '.txt'))
            # plot_function(lprom_offline, os.path.join(OUTPUT_DIR, fname), ftype='png')
            # #lprom_offline = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname))

            fname = 'Exp-Online-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_online), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            lprom_online = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-OnlineLP-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_offlinelp), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            lprom_offlinelp = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-OnlineRandom-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d,runs_per_scenario:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED, RUNS_PER_SCENARIO)
            #print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_online_random, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-FirstFit-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            lprom_firstfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))


            fname = 'Exp-BestFit-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            lprom_bestfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            lprom_online_normalized = [(PR_SAT_DO_MISSION[i], lprom_online[i][1] / mission_num) for i in range(len(PR_SAT_DO_MISSION))]
            lprom_offlinelp_normalized = [(PR_SAT_DO_MISSION[i], lprom_offlinelp[i][1] / mission_num) for i in range(len(PR_SAT_DO_MISSION))]
            lprom_online_random_normalized =[(PR_SAT_DO_MISSION[i], lprom_online_random[i][1] / mission_num) for i in range(len(PR_SAT_DO_MISSION))]
            lprom_firstfit_normalized = [(PR_SAT_DO_MISSION[i], lprom_firstfit[i][1] / mission_num) for i in range(len(PR_SAT_DO_MISSION))]
            lprom_bestfit_normalized = [(PR_SAT_DO_MISSION[i], lprom_bestfit[i][1] / mission_num) for i in range(len(PR_SAT_DO_MISSION))]


            fname = 'Exp-Comparison-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            plot_nfunctions([lprom_offlinelp, lprom_online, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                            xlabel='Probability of each satellite is able to fulfill a given mission',
                            ylabel='Number of total assigned missions', colors=['red', 'blue', 'lightblue','green', 'magenta'], ftype='png')
            plot_nfunctions([lprom_offlinelp, lprom_online, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                            xlabel='Probability of each satellite is able to fulfill a given mission',
                            ylabel='Number of total assigned missions', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg')

            fname = 'Exp-ComparisonNormalized-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            plot_nfunctions([lprom_offlinelp_normalized, lprom_online_normalized, lprom_online_random_normalized, lprom_firstfit_normalized, lprom_bestfit_normalized], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                            xlabel='Probability of each satellite is able to fulfill a given mission',
                            ylabel='Normalized Number of total assigned missions', colors=['red', 'blue', 'lightblue','green', 'magenta'], ftype='png', yticks=YTICKS[sat_num_index])
            plot_nfunctions([lprom_offlinelp_normalized, lprom_online_normalized, lprom_online_random_normalized, lprom_firstfit_normalized, lprom_bestfit_normalized], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                            xlabel='Probability of each satellite is able to fulfill a given mission',
                            ylabel='Normalized Number of total assigned missions', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg', yticks=YTICKS[sat_num_index])


            # lprom_offline = getListFromFile('results-30-05-2019/Exp-Offline-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d.txt'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED))
            # lprom_online = getListFromFile('results-30-05-2019/Exp-Online-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d.txt'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED))
            # lprom_offlinelp = getListFromFile('results-31-05-2019/Exp-OnlineLP-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d.txt'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED))
            # fname = 'Exp-OfflineCMP-sat_num:%d,mission_num:%d,mission_per_sat:%d,rep:%d,seed:%d'%(sat_num, mission_num, mission_per_sat, REPETITIONS, SEED)
            # plot_nfunctions([lprom_offline, lprom_offlinelp, lprom_online, lprom_online_random], os.path.join(OUTPUT_DIR, fname),
            #                 labels=['Offline assignation', 'Offline LP assignation', 'Online assignation', 'Random assignation'],
            #                 xlabel='Probability of each satellite is able to fulfill a given mission',
            #                 ylabel='Number of total assigned missions', colors=['red','green','blue', 'lightblue'], ftype='png')
            # plot_nfunctions([lprom_offline, lprom_offlinelp, lprom_online, lprom_online_random], os.path.join(OUTPUT_DIR, fname),
            #                 labels=['Offline assignation', 'Offline LP assignation', 'Online assignation', 'Random assignation'],
            #                 xlabel='Probability of each satellite is able to fulfill a given mission',
            #                 ylabel='Number of total assigned missions', colors=['red','green','blue', 'lightblue'], ftype='svg')