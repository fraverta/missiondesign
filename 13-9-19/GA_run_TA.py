import os
import sys
from random import Random
import simplejson as json
from imp_9_9_19 import  UserScenarioGenerator, TableAssigner, generate_f_download

#Create experiment
#Run
#retrieve output

assert len(sys.argv)==10, "Invoke python GA_run_TA sat_num sat_capacity pr_sat_to_do_mission t_end offered_load_prop repetitions w_brt w_info_age w_info_delay"

sat_num = int(sys.argv[1])
sat_capacity = int(sys.argv[2])
pr_sat_to_do_mission = float(sys.argv[3])
t_end = int(sys.argv[4])
offered_load_prop = float(sys.argv[5])
repetitions = int(sys.argv[6])
w_brt = float(sys.argv[7])
w_info_age = float(sys.argv[8])
w_info_delay = float(sys.argv[9])

f_mission_weight = [1]
num_of_downloads = 4
seed = 245
SEPARATOR = '@@@'

assert 0.<=offered_load_prop<=1., f"offered_load_prop must belong [0,1] but its value is {offered_load_prop}"
assert abs(w_brt + w_info_age + w_info_delay - 1) < 0.0001, f'TA parameters must sum 1 but they sums {w_brt + w_info_age + w_info_delay}'


random = Random(10)
system_capacity = sat_num * num_of_downloads * sat_capacity
f_download = [generate_f_download(num_of_downloads, t_end, random) for sat in range(sat_num)]
offered_load = round(system_capacity * offered_load_prop)
f_mission_weight = [1 for i in range(offered_load)]

scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=seed)
results = TableAssigner.run_experiment(sat_num, sat_capacity, scenario_generator, repetitions, w_brt=w_brt, w_info_age=w_info_age, w_info_delay=w_info_delay,)
print(SEPARATOR)
dict_result= results.to_dict()
dict_result['served_load'] = dict_result['f_assigned_capacity'] / system_capacity
print(json.dumps(dict_result))
print(SEPARATOR)
exit(0)