import os
from utilities.utils import print_str_to_file, getListFromFile

NUM_OF_DOWNLOADS = 4
pr = 1.0
SAT_CAPACITY = 10
TA_COEF = [{'w_brt':.16, 'w_info_age':.53, 'w_info_delay':.31},{'w_brt':.5, 'w_info_age':.39, 'w_info_delay':.11}, {'w_brt':.47, 'w_info_age':.40, 'w_info_delay':.13}]
#TA_COEF = [{'w_brt':.5, 'w_info_age':.25, 'w_info_delay':.25}, {'w_brt':.5, 'w_info_age':.3, 'w_info_delay':.2}, {'w_brt':.5, 'w_info_age':.4, 'w_info_delay':.1}, {'w_brt':.6, 'w_info_age':.2, 'w_info_delay':.2}, {'w_brt':.6, 'w_info_age':.3, 'w_info_delay':.1}, {'w_brt':.25, 'w_info_age':.5, 'w_info_delay':.25},{'w_brt':.3, 'w_info_age':.5, 'w_info_delay':.2},{'w_brt':.4, 'w_info_age':.5, 'w_info_delay':.1},{'w_brt':.2, 'w_info_age':.6, 'w_info_delay':.2},{'w_brt':.3, 'w_info_age':.6, 'w_info_delay':.1},{'w_brt':.4, 'w_info_age':.4, 'w_info_delay':.1},{'w_brt':.425, 'w_info_age':.425, 'w_info_delay':.5}
#]
ALGORITHMS = ['MILP','MO-MILP-1','MO-MILP-2','MO-MBRT-MBP-MDT','MO-MBRT-MDT-MBP', 'TA', 'MDT'] + [f"TA-brt:{coef['w_brt']}-iage:{coef['w_info_age']}-:idelay{coef['w_info_delay']}" for coef in TA_COEF] #['MILP','MO-MILP-1','MO-MILP-2','MO-MBRT-MBP-MDT','MO-MBRT-MDT-MBP', 'TA', 'MBRT', 'MBP', 'MDT']
ALGORITHM_LABELS = {**{'MILP':'MILP', 'MBP':'MBP', 'MBRT': 'MBRT', 'MDT':'MDT', 'MO-MILP-1': 'MO-MILP-1' ,'MO-MILP-2': 'MO-MILP-2' ,'MO-MBRT-MBP-MDT': 'MO-MBRT-1','MO-MBRT-MDT-MBP': 'MO-MBRT-2' , 'TA':'TA'},
                    **dict((f"TA-brt:{coef['w_brt']}-iage:{coef['w_info_age']}-:idelay{coef['w_info_delay']}", f"TA-GA-{i}") for i, coef in enumerate(TA_COEF))}
METRICS = [('served_load', 'Served Load'), ('adquisition_until_dw', 'Information Age'),('del_time', 'Delivery Time')] # ('buffer_booking_until_dw', 'Average time of Buffer Booking'),
pr_sat_to_do_mission = 0.9
REPETITIONS = 25
SEED = 245
OUTPUT_DIR = f'results-new_mission_size=1.py-GUROBI-28-01-2020-tend:100-num_of_downloads:{NUM_OF_DOWNLOADS}'  # 'results-%s-%s-tend:%d-num_of_downloads:%d' % (os.path.basename(__file__), date.today().strftime("%d-%m-%Y"), t_end, NUM_OF_DOWNLOADS)
sat_num = 50

results_by_algorithm = dict((alg, {}) for alg in ALGORITHMS)
for alogrithm in ALGORITHMS:
    for metric in METRICS:
        fname = f'Exp-{alogrithm}-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]}'
        # print_str_to_file(str(results_by_algorithm[a][m]), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        results_by_algorithm[alogrithm][metric[0]] = dict(getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt')))

#Served load
alg_max_served_load = ALGORITHMS[0]
for alg in ALGORITHMS:
    if results_by_algorithm[alg]['served_load'][pr] > results_by_algorithm[alg_max_served_load]['served_load'][pr]:
        alg_max_served_load = alg
# alg_sorted_by_served_load = sorted([(alg, results_by_algorithm[alg]['served_load'][pr] - results_by_algorithm[alg_max_served_load]['served_load'][pr]) for alg in ALGORITHMS], key=lambda x:x[1], reverse=True)
# for alg, value in alg_sorted_by_served_load:
#     print(f"{ALGORITHM_LABELS[alg]:10s}\t{value:3.4f}")

print("\n\n")
alg_sorted_by_served_load = sorted([(alg, results_by_algorithm[alg]['served_load'][pr]  * 100 / results_by_algorithm[alg_max_served_load]['served_load'][pr] - 100) for alg in ALGORITHMS], key=lambda x:x[1], reverse=True)
for alg, value in alg_sorted_by_served_load:
    print(f"{ALGORITHM_LABELS[alg]:10s}\t{value:3.4f}%")

#Info Age
alg_min_info_age = ALGORITHMS[0]
for alg in ALGORITHMS:
    if results_by_algorithm[alg]['adquisition_until_dw'][pr] < results_by_algorithm[alg_min_info_age]['adquisition_until_dw'][pr]:
        alg_min_info_age = alg

print("\n\n")
alg_sorted_info_age = sorted([(alg, results_by_algorithm[alg]['adquisition_until_dw'][pr]  * 100 / results_by_algorithm[alg_min_info_age]['adquisition_until_dw'][pr] - 100) for alg in ALGORITHMS], key=lambda x:x[1])
for alg, value in alg_sorted_info_age:
    print(f"{ALGORITHM_LABELS[alg]:10s}\t{value:3.4f}%")


#Info Delay
alg_min_info_delay = ALGORITHMS[0]
for alg in ALGORITHMS:
    if results_by_algorithm[alg]['del_time'][pr] < results_by_algorithm[alg_min_info_delay]['del_time'][pr]:
        alg_min_info_delay = alg

print("\n\n")
alg_sorted_info_age = sorted([(alg, results_by_algorithm[alg]['del_time'][pr] * 100 / results_by_algorithm[alg_min_info_delay]['del_time'][pr] - 100) for alg in ALGORITHMS], key=lambda x:x[1])
for alg, value in alg_sorted_info_age:
    print(f"{ALGORITHM_LABELS[alg]:10s}\t{value:3.4f}%")