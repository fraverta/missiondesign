from typing import Dict, List, Tuple
import random
from abc import abstractmethod
from pulp import *
import time
from copy import copy
from typing import Tuple, List, Dict
from utilities.utils import prom_llist, average
import simplejson as json

'''
Types definition
'''

#Satelites -> (Mission, Time in which satelite is able to fulfill the Mission)
Scenario = Dict[int, Tuple[int,int]]

# The list i-esim position denotes the size of mission i
MissionWeight = List[int]

# Satelite -> Times it is able to download data
DownloadTimes = Dict[int, List[int]]

# Sat -> Download Time -> [Missions]
MissionAssignement = Dict[int, Dict[int, List[int]]]

# Sat -> Mission -> Time of buffer booking
Time_Of_Buffer_Booking = Dict[int, Dict[int, int]]

'''
END Types definition
'''

class ExperimentResult():

    def __init__(self, f1, f2, f3, f4):
        self.f_assigned_capacity = average(f1)
        self.f_data_adquisition_until_download = average(f2)
        self.f_buffer_booking_until_download = average(f3)
        self.f_delivery_time = average(f4)

    def to_dict(self):
        return {'f_assigned_capacity':self.f_assigned_capacity,
                          'f_data_adquisition_until_download': self.f_data_adquisition_until_download,
                        'f_buffer_booking_until_download':self.f_buffer_booking_until_download,
                        'f_delivery_time':self.f_delivery_time}



class Scheme_13_8_19:

    _MILP = 'MILP'
    _MBP = 'MBP'
    _LBB = 'LBB'
    _FF = 'FF'
    _BF = 'BF'
    _RF = 'RF'
    _MBRT = 'MBRT'
    _MDT = 'MDT'

    _S_MBP = 'S-MBP'
    _RS_MBP = 'RS-MBP'

    _S_MBRT = 'S-MBRT'
    _RS_MBRT = 'RS-MBRT'

    _S_MDT = 'S-MDT'
    _RS_MDT = 'RS-MDT'

    _MO_MILP_1 = 'MO_MILP_1'
    _MO_MILP_2 = 'MO_MILP_2'

    _MO_MBRT_1 = 'MO_MBRT_1'
    _MO_MBRT_2 = 'MO_MBRT_2'

    _TA = 'TA'



    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator'):

        self._sat_num = sat_num
        self._sat_capacity = sat_capacity
        self._scenario_generator = scenario_generator

    @abstractmethod
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight:List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        pass

    @staticmethod
    @abstractmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator, number_of_runs, seed=0) -> List[int]:
        pass

    @staticmethod
    def _run_experiment(number_of_runs, exp: 'Scheme_13_8_19') -> ExperimentResult:
        f_assigned_capacity = []
        f_data_adquisition_until_download = []
        f_buffer_booking_until_download = []
        f_delivery_time = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = exp._scenario_generator.generate_scenario()
            assigned_capacity, mission_assignement, time_of_buffer_booking = exp._compute_mission_assignment(scenario, mission_weight, f_downloads_times)
            f_assigned_capacity.append(assigned_capacity)
            f_data_adquisition_until_download.append(Scheme_13_8_19._data_adquisition_until_download(scenario, f_downloads_times, mission_assignement))
            f_buffer_booking_until_download.append(Scheme_13_8_19._buffer_booking_until_download(mission_assignement, time_of_buffer_booking))
            f_delivery_time.append(Scheme_13_8_19._compute_delivery_time(mission_assignement))

        return ExperimentResult(f_assigned_capacity, f_data_adquisition_until_download, f_buffer_booking_until_download, f_delivery_time)

    @staticmethod
    def _data_adquisition_until_download(scenario, f_downloads_times, mission_assignment: MissionAssignement):
        metric = 0; assigned_missions = 0.;
        for sat in scenario.keys():
            for dt in f_downloads_times[sat]:
                for m in mission_assignment[sat][dt]:
                  metric += dt - next(filter(lambda x: x[0] == m, scenario[sat]))[1]
                  assigned_missions += 1

        return metric / assigned_missions if assigned_missions > 0 else 0.

    @staticmethod
    def _buffer_booking_until_download(mission_assignement: MissionAssignement, time_of_buffer_booking):
        metric = 0; assigned_missions = 0.;
        for sat in mission_assignement.keys():
            for dt in mission_assignement[sat].keys():
                for m in mission_assignement[sat][dt]:
                  metric += dt - time_of_buffer_booking[sat][m]
                  assigned_missions += 1

        return metric / assigned_missions if assigned_missions > 0 else 0.

    @staticmethod
    def _compute_delivery_time(mission_assignement: MissionAssignement):
        metric = 0; number_of_assigned_missions = 0;
        for sat in mission_assignement.keys():
            for dt in mission_assignement[sat].keys():
                metric += dt * len(mission_assignement[sat][dt])
                number_of_assigned_missions += len(mission_assignement[sat][dt])

        return metric / number_of_assigned_missions if number_of_assigned_missions > 0 else 0

    @staticmethod
    def choose_and_run(alg:str, sat_num: int, sat_capacity: int, scenario_generator, number_of_runs, seed=0) -> ExperimentResult:
        if alg == Scheme_13_8_19._MILP:
            return MILP.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._MBP:
            return MBP.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg ==Scheme_13_8_19._LBB:
            return LBB.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._FF:
            return FirstFit.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._BF:
            return BestFit.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._RF:
            return RandomFit.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs, seed=seed)
        elif alg == Scheme_13_8_19._MBRT:
            return MBRT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._MDT:
            return MDT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._S_MBP:
            return SortedMBP.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._RS_MBP:
            return ReverseSortedMBP.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._S_MBRT:
            return SortedMBRT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._RS_MBRT:
            return ReverseSortedMBRT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._S_MDT:
            return SortedMDT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._RS_MDT:
            return ReverseSortedMDT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._MO_MILP_1:
            return MO_MILP_1.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._MO_MILP_2:
            return MO_MILP_2.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._MO_MBRT_1:
            return MO_MBRT_MBP_MDT.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._MO_MBRT_2:
            return MO_MBRT_MDT_MBP.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        elif alg == Scheme_13_8_19._TA:
            return TableAssigner.run_experiment(sat_num, sat_capacity, scenario_generator, number_of_runs)
        else:
            assert False, f"Invalid algorithm: {alg}"

class MILP(Scheme_13_8_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        mission_num = len(mission_weight)
        if all(len(scenario[sat]) == 0 for sat in range(self._sat_num)):
            empty_assignent = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
            return 0, empty_assignent, 0
        else:
            # vars
            sat_mission_idx = []
            for sat in range(self._sat_num):
                for m, t in scenario[sat]:
                    previous_download_time = 0
                    for download_time in f_download_times[sat]:
                        if previous_download_time <= t < download_time:
                            sat_mission_idx.append('Sat%d_M%d_D%d' % (sat, m, download_time))
                            break
                        previous_download_time = download_time

            lp_problem = LpProblem("The Mission Problem", LpMaximize)
            sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)

            #Objetive Function
            f_obj_terms = []
            for sat in range(self._sat_num):
                for m, t in scenario[sat]:
                    previous_download_time = 0
                    for download_time in f_download_times[sat]:
                        if previous_download_time <= t < download_time:
                            f_obj_terms.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' %(sat, m, download_time)])
                            break
                        previous_download_time = download_time
            lp_problem += lpSum(f_obj_terms), "Total assigned capacity"

            # CONSTRAINT: Mission are assigned to atmost 1 satellite buffer
            for m in range(mission_num):
                sat_able_to_fulfill_m_vars = [sat_mission_vars['Sat%d_M%d_D%d'%(sat, m, dt)] for sat in range(self._sat_num) for dt in f_download_times[sat] if 'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys()]
                lp_problem += lpSum([x for x in sat_able_to_fulfill_m_vars]) <= 1, 'Atmost 1 satelite assigned to mission %d'% m

            # CONSTRAINT: Satellite buffer are not overloaded
            for sat in range(self._sat_num):
                for dt in f_download_times[sat]:
                    mission_per_sat_buffer_vars = []
                    for m, _ in scenario[sat]:
                        if 'Sat%d_M%d_D%d'%(sat, m, dt) in sat_mission_vars.keys():
                            mission_per_sat_buffer_vars.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d'%(sat, m, dt)])
                    lp_problem += lpSum(mission_per_sat_buffer_vars) <= self._sat_capacity, 'Sat %d - download %d - capacity constraint'%(sat, dt)

            #lp_problem.solve(CPLEX())
            #lp_problem.solve(CPLEX_CMD(options=["CUTPASSES=-1"]))
            lp_problem.solve()
            #lp_problem.solve(GUROBI())
            if lp_problem.status != LpStatusOptimal:
                raise ValueError("MILP solutions is not optimal")

            sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
            time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))
            for v in sat_mission_vars.values():
                if v.varValue:
                    sat = int(v.name[4: v.name.index('M') - 1])
                    mission = int(v.name[v.name.rindex('M') + 1: v.name.index('D') - 1])
                    download_time = int(v.name[v.name.rindex('D') + 1:])

                    sat_assignment[sat][download_time].append(mission)
                    time_of_buffer_booking[sat][mission] = f_download_times[sat][f_download_times[sat].index(download_time) - 1] if f_download_times[sat].index(download_time) > 0 else 0


            elapsed_time = time.time() - start_time
            print("MILP:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
            return value(lp_problem.objective), sat_assignment, time_of_buffer_booking

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MILP(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class MBP(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator: 'ScenarioGenerator', sorted_=False, reverse=False):
        super(MBP, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__sorted = sorted_
        self.__reverse = reverse

    def __compute_permanence(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i] - t,  f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement,Time_Of_Buffer_Booking]:
        start_time = time.time()
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        if self.__sorted:
            missions = mission_sort_by_metric(range(len(mission_weight)), scenario, f_download_times, self.__compute_permanence, reverse=self.__reverse)
        else:
            missions = range(len(mission_weight))

        for m in missions:
            m_weight = mission_weight[m]
            sat_ordered_by_buffer_permanence = []
            for sat in range(self._sat_num):
                try:
                    _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
                except StopIteration:
                    continue
                permanence_time, buffer = self.__compute_permanence(t, f_download_times[sat])
                sat_ordered_by_buffer_permanence.append((sat, permanence_time, buffer))

            sat_ordered_by_buffer_permanence.sort(key=lambda x: x[1])
            for sat, permanence_time, buffer in sat_ordered_by_buffer_permanence:
                if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity:
                    sat_assignment[sat][buffer].append(m)
                    assigned_capacity += m_weight
                    time_of_buffer_booking[sat][m] = f_download_times[sat][f_download_times[sat].index(buffer) - 1] if f_download_times[sat].index(buffer) > 0 else 0
                    break

        elapsed_time = time.time() - start_time
        print("MBP:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, sat_assignment, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBP(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)

class LBB(Scheme_13_8_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        sat_assignment = [{'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])} for sat in range(self._sat_num)]
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))
        to_assign_missions = list(range(len(mission_weight)))
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1], reverse=True)
                sat_assigned = False
                for sat, remained_capacity in sat_ordered_by_busy_buffer:
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[sat] if m == mm and t <= tt]
                    if len(time_able_to_fulfill_m) > 0 and remained_capacity - mission_weight[m] >= 0:
                        m_dt = [dt for dt in f_download_times[sat] if dt > time_able_to_fulfill_m[0]][0]
                        sat_assignment[sat]['assigned_missions'][m_dt].append(m)
                        sat_assignment[sat]['remained_capacity'] -= m_weight
                        assigned_capacity += m_weight
                        to_assign_missions.pop(i)
                        sat_assigned = True
                        time_of_buffer_booking[sat][m] = t
                        break

                if not sat_assigned:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("LBB:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, res, time_of_buffer_booking

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = LBB(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class FirstFit(Scheme_13_8_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])}) for sat in range(self._sat_num))
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        to_assign_missions = list(range(len(mission_weight)))
        assigned_capacity = 0
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                sat_assigned = False
                for sat in range(self._sat_num):
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[sat] if m == mm and t <= tt]
                    if len(time_able_to_fulfill_m) > 0 and sat_assignment[sat]['remained_capacity'] - mission_weight[m] >= 0:
                        m_dt = [dt for dt in f_download_times[sat] if dt > time_able_to_fulfill_m[0]][0]
                        sat_assignment[sat]['assigned_missions'][m_dt].append(m)
                        sat_assignment[sat]['remained_capacity'] -= m_weight
                        assigned_capacity += m_weight
                        to_assign_missions.pop(i)
                        sat_assigned = True
                        time_of_buffer_booking[sat][m] = t
                        break

                if not sat_assigned:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return assigned_capacity, res, time_of_buffer_booking

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = FirstFit(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class BestFit(Scheme_13_8_19):
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_assignment = [{'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])} for sat in range(self._sat_num)]
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        to_assign_missions = list(range(len(mission_weight)))
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1])
                sat_assigned = False
                for sat, remained_capacity in sat_ordered_by_busy_buffer:
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[sat] if m == mm and t <= tt]
                    if len(time_able_to_fulfill_m) > 0 and remained_capacity - mission_weight[m] >= 0:
                        m_dt = [dt for dt in f_download_times[sat] if dt > time_able_to_fulfill_m[0]][0]
                        sat_assignment[sat]['assigned_missions'][m_dt].append(m)
                        sat_assignment[sat]['remained_capacity'] -= m_weight
                        assigned_capacity += m_weight
                        to_assign_missions.pop(i)
                        sat_assigned = True
                        time_of_buffer_booking[sat][m] = t
                        break

                if not sat_assigned:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("BestFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return assigned_capacity, res, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = BestFit(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)

class RandomFit(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', seed=0):
        super(RandomFit, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__random = random.Random(seed)


    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        sat_assignment = [{'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])} for sat in range(self._sat_num)]
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))
        assigned_capacity = 0

        to_assign_missions = list(range(len(mission_weight)))
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                choseable_sats = [sat for sat in range(self._sat_num) if len([tt for mm, tt in scenario[sat] if m == mm and t <= tt]) > 0 and sat_assignment[sat]['remained_capacity'] >= m_weight]
                if len(choseable_sats) > 0:
                    chosed_sat = self.__random.choice(choseable_sats)
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[chosed_sat] if m == mm and t <= tt][0]
                    m_dt = [dt for dt in f_download_times[chosed_sat] if dt > time_able_to_fulfill_m][0]
                    sat_assignment[chosed_sat]['assigned_missions'][m_dt].append(m)
                    sat_assignment[chosed_sat]['remained_capacity'] -= m_weight
                    assigned_capacity += m_weight
                    to_assign_missions.pop(i)
                    time_of_buffer_booking[chosed_sat][m] = t
                else:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("RandomFit:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, res, time_of_buffer_booking

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int, number_of_runs_per_scenario:int, seed=0) -> List[int]:
        exp = RandomFit(sat_num, sat_capacity, scenario_generator, seed)
        return exp._run_experiment(number_of_runs, number_of_runs_per_scenario, exp)

    @staticmethod
    def _run_experiment(number_of_runs:int, number_of_runs_per_scenario: int, exp: 'Online_Random_Assignment') -> ExperimentResult:
        result = []; f_data_adquisition_until_download = []; f_buffer_booking_until_download = []; f_delvery_time = [];

        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = exp._scenario_generator.generate_scenario()
            scenario_result = 0.
            scenario_result1 = 0.
            scenario_result2 = 0.
            scenario_result3 = 0.
            for j in range(number_of_runs_per_scenario):
                assigned_capacity, mission_assignement, time_of_buffer_booking = exp._compute_mission_assignment(scenario, mission_weight, f_downloads_times)
                scenario_result += assigned_capacity
                scenario_result1 += Scheme_13_8_19._data_adquisition_until_download(scenario, f_downloads_times, mission_assignement)
                scenario_result2 += Scheme_13_8_19._buffer_booking_until_download(mission_assignement, time_of_buffer_booking)
                scenario_result3 += Scheme_13_8_19._compute_delivery_time(mission_assignement)

            result.append(scenario_result / number_of_runs_per_scenario)
            f_data_adquisition_until_download.append(scenario_result1 / number_of_runs_per_scenario)
            f_buffer_booking_until_download.append(scenario_result2 / number_of_runs_per_scenario)
            f_delvery_time.append(scenario_result3/number_of_runs_per_scenario)

        return ExperimentResult(result, f_data_adquisition_until_download, f_buffer_booking_until_download, f_delvery_time)


class MBRT(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator: 'ScenarioGenerator', sorted_=False, reverse=False):
        super(MBRT, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__sorted = sorted_
        self.__reverse = reverse


    def __compute_buffer_reservation_time(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i] - (f_download_times[i-1] if i > 0 else 0),  f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement,Time_Of_Buffer_Booking]:
        start_time = time.time()
        scenario_as_dict = dict((sat, dict(scenario[sat])) for sat in scenario.keys())  # sat->mission->time of info adquisition
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        if self.__sorted:
            missions = mission_sort_by_metric(range(len(mission_weight)), scenario, f_download_times,  self.__compute_buffer_reservation_time, reverse=self.__reverse)
        else:
            missions = range(len(mission_weight))

        for m in missions:
            m_weight = mission_weight[m]
            sat_ordered_by_buffer_reservation_time = []
            for sat in range(self._sat_num):
                if m not in scenario_as_dict[sat].keys():
                    continue
                t = scenario_as_dict[sat][m]
                buffer_reservation_time, buffer = self.__compute_buffer_reservation_time(t, f_download_times[sat])
                sat_ordered_by_buffer_reservation_time.append((sat, buffer_reservation_time, buffer))

            sat_ordered_by_buffer_reservation_time.sort(key=lambda x: x[1])
            for sat, buffer_reservation_time, buffer in sat_ordered_by_buffer_reservation_time:
                if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity:
                    sat_assignment[sat][buffer].append(m)
                    assigned_capacity += m_weight
                    time_of_buffer_booking[sat][m] = f_download_times[sat][f_download_times[sat].index(buffer) - 1] if f_download_times[sat].index(buffer) > 0 else 0
                    break

        elapsed_time = time.time() - start_time
        print("MBRT:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, sat_assignment, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBRT(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class MDT(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator: 'ScenarioGenerator', sorted_=False, reverse=False):
        super(MDT, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__sorted = sorted_
        self.__reverse = reverse

    def __compute_delivery_time(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i], f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement,Time_Of_Buffer_Booking]:
        start_time = time.time()
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        if self.__sorted:
            missions = mission_sort_by_metric(range(len(mission_weight)), scenario, f_download_times,  self.__compute_delivery_time, reverse=self.__reverse)
        else:
            missions = range(len(mission_weight))

        for m in missions:
            m_weight = mission_weight[m]
            sat_ordered_by_delivery_time = []
            for sat in range(self._sat_num):
                try:
                    _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
                except StopIteration:
                    continue
                delivery_time, buffer = self.__compute_delivery_time(t, f_download_times[sat])
                sat_ordered_by_delivery_time.append((sat, delivery_time, buffer)) #buffer = delivery_time

            sat_ordered_by_delivery_time.sort(key=lambda x: x[1])
            for sat, buffer_reservation_time, buffer in sat_ordered_by_delivery_time:
                if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity:
                    sat_assignment[sat][buffer].append(m)
                    assigned_capacity += m_weight
                    time_of_buffer_booking[sat][m] = f_download_times[sat][f_download_times[sat].index(buffer) - 1] if f_download_times[sat].index(buffer) > 0 else 0
                    break

        elapsed_time = time.time() - start_time
        print("MDF:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, sat_assignment, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MDT(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class SortedMBP(Scheme_13_8_19):

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBP(sat_num, sat_capacity, scenario_generator, sorted_=True)
        return exp._run_experiment(number_of_runs, exp)


class ReverseSortedMBP(Scheme_13_8_19):

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBP(sat_num, sat_capacity, scenario_generator, sorted_=True, reverse=True)
        return exp._run_experiment(number_of_runs, exp)


class SortedMBRT(Scheme_13_8_19):

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBRT(sat_num, sat_capacity, scenario_generator, sorted_=True)
        return exp._run_experiment(number_of_runs, exp)


class ReverseSortedMBRT(Scheme_13_8_19):

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBRT(sat_num, sat_capacity, scenario_generator, sorted_=True, reverse=True)
        return exp._run_experiment(number_of_runs, exp)


class SortedMDT(Scheme_13_8_19):

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MDT(sat_num, sat_capacity, scenario_generator, sorted_=True)
        return exp._run_experiment(number_of_runs, exp)


class ReverseSortedMDT(Scheme_13_8_19):

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MDT(sat_num, sat_capacity, scenario_generator, sorted_=True, reverse=True)
        return exp._run_experiment(number_of_runs, exp)


class ScenarioGenerator():
    def __init__(self, sat_num, mission_num, seed=0):
        self._sat_num = sat_num
        self._mission_num = mission_num
        self._random = random.Random(seed)


class UserScenarioGenerator(ScenarioGenerator):
    '''
        Genate random scenarios in which cost are provided by the user class
    '''

    def __init__(self, sat_num: int, mission_num: int, pr_sat_to_do_mission: float, f_mission_weight: List[int], t_end: int, f_download: List[int], randomize_mission_cost=False, seed=0):
        super(UserScenarioGenerator, self).__init__(sat_num, mission_num, seed)
        self.__pr_sat_to_do_mission = pr_sat_to_do_mission
        self.__f_mission_weight = f_mission_weight
        self.__randomize_mission_cost = randomize_mission_cost
        self.__t_end = t_end
        self.__f_download = f_download


    def generate_scenario(self) -> (Scenario, MissionWeight, DownloadTimes):
        '''

        :return: Sat -> [(mission, t)] it is able to handle and at which time, mission -> required mission storage, sat -> number of assigned missions in system before download
        '''
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in range(self._mission_num):
                if self._random.random() < self.__pr_sat_to_do_mission:
                    t = self._random.randint(1, self.__t_end - 1)
                    result[s].append((m, t))

        if self.__randomize_mission_cost:
            return result, self._random.sample(self.__f_mission_weight, len(self.__f_mission_weight)), self.__f_download
        else:
            return result, copy(self.__f_mission_weight), self.__f_download


class CitiesScenarioGenerator(ScenarioGenerator):

    def __init__(self, sat_num, mission_num, input_file_path, seed=0):
        super(CitiesScenarioGenerator, self).__init__(sat_num, mission_num, seed)
        with open(input_file_path, 'r') as f:
            self.__sat_target_contacts = json.load(f)

        sat_ids = sorted(list(self.__sat_target_contacts.keys()))
        self.__target_ids = dict((k, i) for i, k in enumerate(sorted(set(id for startt, endt, city, id in itertools.chain.from_iterable(self.__sat_target_contacts.values())))))
        assert len(sat_ids) == sat_num, f"sat_num {sat_num} but there are {len(sat_ids)} in {input_file_path}"

        #Renaming
        self.__endt = max(endt for sat in sat_ids for start, endt, city, id in self.__sat_target_contacts[sat])
        self.__sat_target_contacts = dict((i, dict((self.__target_ids[city_id], startt) for startt, endt, city, city_id in self.__sat_target_contacts[sat_id])) for i, sat_id in enumerate(sat_ids))



    def generate_scenario(self) -> (Scenario, MissionWeight, DownloadTimes):
        '''

        :return: Sat -> [(mission, t)] it is able to handle and at which time, mission -> required mission storage, sat -> number of assigned missions in system before download
        '''
        missions = self._random.choices(range(len(self.__target_ids.values())), k=self._mission_num)
        f_mission_rename = dict((m, i) for i, m in enumerate(missions))
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in missions:
                if m in self.__sat_target_contacts[s].keys():
                    result[s].append((f_mission_rename[m], self.__sat_target_contacts[s][m]))

        return result, [1] * self._mission_num, [[self.__endt] for sat in range(self._sat_num)]



class MO_MILP(Scheme_13_8_19):

    def run_milp_served_load(self, scenario: Dict[int,Tuple[int, int]], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        mission_num = len(mission_weight)
        # vars
        sat_mission_idx = []
        for sat in range(self._sat_num):
            for m, t in scenario[sat]:
                previous_download_time = 0
                for download_time in f_download_times[sat]:
                    if previous_download_time <= t < download_time:
                        sat_mission_idx.append('Sat%d_M%d_D%d' % (sat, m, download_time))
                        break
                    previous_download_time = download_time

        lp_problem = LpProblem("The Mission Problem", LpMaximize)
        sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)

        # Objetive Function
        f_obj_terms = []
        for sat in range(self._sat_num):
            for m, t in scenario[sat]:
                previous_download_time = 0
                for download_time in f_download_times[sat]:
                    if previous_download_time <= t < download_time:
                        f_obj_terms.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])
                        break
                    previous_download_time = download_time
        lp_problem += lpSum(f_obj_terms), "Total assigned capacity"

        # CONSTRAINT: Mission are assigned to atmost 1 satellite buffer
        for m in range(mission_num):
            sat_able_to_fulfill_m_vars = [sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, dt)] for sat in
                                          range(self._sat_num) for dt in f_download_times[sat] if
                                          'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys()]
            lp_problem += lpSum([x for x in sat_able_to_fulfill_m_vars]) <= 1, 'Atmost 1 satelite assigned to mission %d' % m

        # CONSTRAINT: Satellite buffer are not overloaded
        for sat in range(self._sat_num):
            for dt in f_download_times[sat]:
                mission_per_sat_buffer_vars = []
                for m, _ in scenario[sat]:
                    if 'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys():
                        mission_per_sat_buffer_vars.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, dt)])
                lp_problem += lpSum(mission_per_sat_buffer_vars) <= self._sat_capacity, 'Sat %d - download %d - capacity constraint' % (sat, dt)

        #lp_problem.solve(CPLEX())
        #lp_problem.solve(CPLEX_CMD(options=["set mip limits cutpasses -1"]))
        lp_problem.solve()
        #lp_problem.solve(GUROBI())
        if lp_problem.status != LpStatusOptimal:
            raise ValueError("MILP solutions is not optimal")

        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))
        for v in sat_mission_vars.values():
            if v.varValue:
                sat = int(v.name[4: v.name.index('M') - 1])
                mission = int(v.name[v.name.rindex('M') + 1: v.name.index('D') - 1])
                download_time = int(v.name[v.name.rindex('D') + 1:])

                sat_assignment[sat][download_time].append(mission)
                time_of_buffer_booking[sat][mission] = f_download_times[sat][f_download_times[sat].index(download_time) - 1] if f_download_times[sat].index(download_time) > 0 else 0

        return value(lp_problem.objective), sat_assignment, time_of_buffer_booking

    def run_milp_info_age(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]], required_served_load:int= None, required_info_delay:int=None) -> int:
        mission_num = len(mission_weight)
        # vars
        sat_mission_idx = []
        for sat in range(self._sat_num):
            for m, t in scenario[sat]:
                previous_download_time = 0
                for download_time in f_download_times[sat]:
                    if previous_download_time <= t < download_time:
                        sat_mission_idx.append('Sat%d_M%d_D%d' % (sat, m, download_time))
                        break
                    previous_download_time = download_time

        lp_problem = LpProblem("The Mission Problem", LpMinimize)
        sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)

        # Objetive Function
        f_obj_terms = []
        for sat in range(self._sat_num):
            for m, t in scenario[sat]:
                previous_download_time = 0
                for download_time in f_download_times[sat]:
                    if previous_download_time <= t < download_time:
                        f_obj_terms.append((download_time - t) * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])
                        break
                    previous_download_time = download_time

        lp_problem += lpSum(f_obj_terms), "Total information age"

        # CONSTRAINT: Mission are assigned to atmost 1 satellite buffer
        for m in range(mission_num):
            sat_able_to_fulfill_m_vars = [sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, dt)] for sat in
                                          range(self._sat_num) for dt in f_download_times[sat] if
                                          'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys()]
            lp_problem += lpSum([x for x in sat_able_to_fulfill_m_vars]) <= 1, 'Atmost 1 satelite assigned to mission %d' % m

        # CONSTRAINT: Satellite buffer are not overloaded
        for sat in range(self._sat_num):
            for dt in f_download_times[sat]:
                mission_per_sat_buffer_vars = []
                for m, _ in scenario[sat]:
                    if 'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys():
                        mission_per_sat_buffer_vars.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, dt)])
                lp_problem += lpSum(mission_per_sat_buffer_vars) <= self._sat_capacity, 'Sat %d - download %d - capacity constraint' % (sat, dt)

        # CONSTRAINT: Required Served load is reached
        if required_served_load is not None:
            constraint_terms = []
            for idx in sat_mission_idx:
                sat = int(idx[3:idx.index('_')])
                m = int(idx[idx.index('M') + 1: idx.rindex('_')])
                download_time = int(idx[idx.rindex('D') + 1:])
                constraint_terms.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])

            lp_problem += lpSum(constraint_terms) == required_served_load, 'Required served load is reached'

        # CONSTRAINT: Required Info delay is reached
        if required_info_delay is not None:
            constraint_terms = []
            for idx in sat_mission_idx:
                sat = int(idx[3:idx.index('_')])
                m = int(idx[idx.index('M') + 1: idx.rindex('_')])
                download_time = int(idx[idx.rindex('D') + 1:])
                constraint_terms.append(download_time * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])

            lp_problem += lpSum(constraint_terms) == required_info_delay, 'Required delay is reached'


        #lp_problem.solve(CPLEX())
        #lp_problem.solve(CPLEX_CMD(options=["set mip limits cutpasses -1"]))
        lp_problem.solve()
        #lp_problem.solve(GUROBI())
        if lp_problem.status != LpStatusOptimal:
            raise ValueError("MILP solutions is not optimal")

        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))
        for v in sat_mission_vars.values():
            if v.varValue:
                sat = int(v.name[4: v.name.index('M') - 1])
                mission = int(v.name[v.name.rindex('M') + 1: v.name.index('D') - 1])
                download_time = int(v.name[v.name.rindex('D') + 1:])

                sat_assignment[sat][download_time].append(mission)
                time_of_buffer_booking[sat][mission] = f_download_times[sat][f_download_times[sat].index(download_time) - 1] if f_download_times[sat].index(download_time) > 0 else 0

        return value(lp_problem.objective), sat_assignment, time_of_buffer_booking

    def run_milp_info_delay(self, scenario: Dict[(int, List[int])], mission_weight: List[int],  f_download_times: List[List[int]], required_served_load: int = None, required_info_age: int = None) -> int:
        mission_num = len(mission_weight)
        # vars
        sat_mission_idx = []
        for sat in range(self._sat_num):
            for m, t in scenario[sat]:
                previous_download_time = 0
                for download_time in f_download_times[sat]:
                    if previous_download_time <= t < download_time:
                        sat_mission_idx.append('Sat%d_M%d_D%d' % (sat, m, download_time))
                        break
                    previous_download_time = download_time

        lp_problem = LpProblem("The Mission Problem", LpMinimize)
        sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)

        # Objetive Function
        f_obj_terms = []
        for idx in sat_mission_idx:
            sat = int(idx[3:idx.index('_')])
            m = int(idx[idx.index('M') + 1: idx.rindex('_')])
            download_time = int(idx[idx.rindex('D') + 1:])
            f_obj_terms.append(download_time * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])

        lp_problem += lpSum(f_obj_terms), "Total information delay"

        # CONSTRAINT: Mission are assigned to atmost 1 satellite buffer
        for m in range(mission_num):
            sat_able_to_fulfill_m_vars = [sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, dt)] for sat in
                                          range(self._sat_num) for dt in f_download_times[sat] if
                                          'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys()]
            lp_problem += lpSum([x for x in sat_able_to_fulfill_m_vars]) <= 1, 'Atmost 1 satelite assigned to mission %d' % m

        # CONSTRAINT: Satellite buffer are not overloaded
        for sat in range(self._sat_num):
            for dt in f_download_times[sat]:
                mission_per_sat_buffer_vars = []
                for m, _ in scenario[sat]:
                    if 'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys():
                        mission_per_sat_buffer_vars.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, dt)])
                lp_problem += lpSum(mission_per_sat_buffer_vars) <= self._sat_capacity, 'Sat %d - download %d - capacity constraint' % (sat, dt)

        # CONSTRAINT: Required Served load is reached
        if required_served_load is not None:
            constraint_terms = []
            for idx in sat_mission_idx:
                sat = int(idx[3:idx.index('_')])
                m = int(idx[idx.index('M') + 1: idx.rindex('_')])
                download_time = int(idx[idx.rindex('D') + 1:])
                constraint_terms.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])

            lp_problem += lpSum(constraint_terms) == required_served_load, 'Required served load is reached'

        # CONSTRAINT: Required information age is reached
        if required_info_age is not None:
            constraint_terms = []
            for sat in range(self._sat_num):
                for m, t in scenario[sat]:
                    previous_download_time = 0
                    for download_time in f_download_times[sat]:
                        if previous_download_time <= t < download_time:
                            constraint_terms.append((download_time - t) * sat_mission_vars['Sat%d_M%d_D%d' % (sat, m, download_time)])
                            break
                        previous_download_time = download_time

            lp_problem += lpSum(constraint_terms) == required_info_age, 'Required info age is reached'

        #lp_problem.solve(CPLEX())
        #lp_problem.solve(CPLEX_CMD(options=["set mip limits cutpasses -1"]))
        lp_problem.solve()
        #lp_problem.solve(GUROBI())
        if lp_problem.status != LpStatusOptimal:
            raise ValueError("MILP solutions is not optimal")

        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))
        for v in sat_mission_vars.values():
            if v.varValue:
                sat = int(v.name[4: v.name.index('M') - 1])
                mission = int(v.name[v.name.rindex('M') + 1: v.name.index('D') - 1])
                download_time = int(v.name[v.name.rindex('D') + 1:])

                sat_assignment[sat][download_time].append(mission)
                time_of_buffer_booking[sat][mission] = f_download_times[sat][f_download_times[sat].index(download_time) - 1] if f_download_times[sat].index(download_time) > 0 else 0

        return value(lp_problem.objective), sat_assignment, time_of_buffer_booking

    def check_obtained_solution(self, solution: Dict[int, Dict[int,List[int]]], scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]],
                                expected_served_load:int = None, expected_info_delay:int = None, expected_info_age:int = None) -> (bool, str):
        '''

        :param solution: Sat->dt->[missions]
        :param scenario:
        :param mission_weight:
        :param f_download_times:
        :param expected_served_load:
        :param expected_info_delay:
        :param expected_info_age:
        :return:
        '''
        report = []
        scenario_as_dict = dict((sat, dict(scenario[sat])) for sat in scenario.keys())

        #Check sat capacity is not violated
        for sat in scenario.keys():
            for dt in f_download_times[sat]:
                obtained_sat_capacity = sum(mission_weight[m] for m in solution[sat][dt])
                if self._sat_capacity < obtained_sat_capacity:
                    report.append(f'Capacity of sat {sat} at dt {dt} has been violated. Expected sat capacaity was {self._sat_capacity} but {obtained_sat_capacity} was assigned')

        #Check mission assignement is a feasible solution
        for sat in scenario.keys():
            for dt in f_download_times[sat]:
                for m in solution[sat][dt]:
                    if len([t for mm, t in scenario[sat] if mm == m and t < dt]) == 0:
                        report.append(f'Mission {m} was assigned to sat {sat} to be downloaded at time {dt} but it was not feasible')

        #Check mission are assigned atmost to one satellite
        for m in range(max(m for sat in scenario.keys() for m,t in scenario[sat])):
            cant = 0
            for sat in scenario.keys():
                for dt in f_download_times[sat]:
                    if m in solution[sat][dt]:
                        cant+=1
            if cant > 1:
                report.append(f'Mission {m} was assigned to more than 1 satellite')

        if expected_served_load is not None:
            obtained_server_load = sum(mission_weight[m] for sat in scenario.keys() for dt in solution[sat].keys() for m in solution[sat][dt])
            if expected_served_load != obtained_server_load:
                report.append(f"Expected Server load was violated: Expected {expected_served_load} - Get {obtained_server_load}")

        if expected_info_delay is not None:
            obtained_info_delay = sum(dt for sat in scenario_as_dict.keys() for dt in solution[sat].keys() for m in solution[sat][dt])
            if expected_info_delay != obtained_info_delay:
                report.append(f"Expected info delay was violated: Expected {expected_info_delay} - Get {obtained_info_delay}")

        if expected_info_age is not None:
            obtained_info_age = sum(dt - scenario_as_dict[sat][m] for sat in scenario_as_dict.keys() for dt in solution[sat].keys() for m in solution[sat][dt])
            if expected_info_age != obtained_info_age:
                report.append(f"Expected info age was violated: Expected {expected_info_age} - Get {obtained_info_age}")

        return len(report) == 0, '\n'.join(report)

class MO_MILP_1(MO_MILP):
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        mission_num = len(mission_weight)
        if all(len(scenario[sat]) == 0 for sat in range(self._sat_num)):
            empty_assignent = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
            return 0, empty_assignent, 0
        else:
            scenario_as_dict = dict((sat,dict(scenario[sat])) for sat in scenario.keys())
            optimal_served_load, sat_assignement, _ = self.run_milp_served_load(scenario, mission_weight, f_download_times)
            is_valid, report = self.check_obtained_solution(sat_assignement, scenario, mission_weight, f_download_times, expected_served_load=optimal_served_load)
            #assert is_valid, report


            optimal_info_age, sat_assignement, time_of_buffer_booking = self.run_milp_info_age(scenario, mission_weight, f_download_times, required_served_load=optimal_served_load)
            is_valid, report = self.check_obtained_solution(sat_assignement, scenario, mission_weight, f_download_times, expected_served_load=optimal_served_load, expected_info_age=optimal_info_age)
            #assert is_valid, report

            optimal_info_delay, sat_assignement, time_of_buffer_booking = self.run_milp_info_delay(scenario, mission_weight, f_download_times, required_served_load=optimal_served_load, required_info_age=optimal_info_age)
            is_valid, report = self.check_obtained_solution(sat_assignement, scenario, mission_weight, f_download_times, expected_served_load=optimal_served_load, expected_info_age=optimal_info_age, expected_info_delay=optimal_info_delay)
            #assert is_valid, report

            elapsed_time = time.time() - start_time
            print("MO_MILP_1:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
            return optimal_served_load, sat_assignement, time_of_buffer_booking

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MO_MILP_1(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class MO_MILP_2(MO_MILP):
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement, Time_Of_Buffer_Booking]:
        start_time = time.time()
        mission_num = len(mission_weight)
        if all(len(scenario[sat]) == 0 for sat in range(self._sat_num)):
            empty_assignent = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
            return 0, empty_assignent, 0
        else:
            scenario_as_dict = dict((sat, dict(scenario[sat])) for sat in scenario.keys())
            optimal_served_load, sat_assignement, _ = self.run_milp_served_load(scenario, mission_weight, f_download_times)
            is_valid, report = self.check_obtained_solution(sat_assignement, scenario, mission_weight, f_download_times, expected_served_load=optimal_served_load)
            #assert is_valid, report

            optimal_info_delay, sat_assignement, _ = self.run_milp_info_delay(scenario, mission_weight, f_download_times, required_served_load=optimal_served_load)
            is_valid, report = self.check_obtained_solution(sat_assignement, scenario, mission_weight, f_download_times, expected_served_load=optimal_served_load, expected_info_delay=optimal_info_delay)
            #assert is_valid, report

            optimal_info_age, sat_assignement, time_of_buffer_booking = self.run_milp_info_age(scenario, mission_weight, f_download_times, required_served_load=optimal_served_load, required_info_delay=optimal_info_delay)
            is_valid, report = self.check_obtained_solution(sat_assignement, scenario, mission_weight, f_download_times, expected_served_load=optimal_served_load, expected_info_delay=optimal_info_delay, expected_info_age=optimal_info_age)
            #assert is_valid, report

            elapsed_time = time.time() - start_time
            print("MO_MILP_2:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
            return optimal_served_load, sat_assignement, time_of_buffer_booking

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MO_MILP_2(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class MO_MBRT_MBP_MDT(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator: 'ScenarioGenerator', sorted_=False, reverse=False):
        super(MO_MBRT_MBP_MDT, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__sorted = sorted_
        self.__reverse = reverse


    def __compute_buffer_reservation_time(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i] - (f_download_times[i-1] if i > 0 else 0),  f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement,Time_Of_Buffer_Booking]:
        start_time = time.time()
        scenario_as_dict = dict((sat, dict(scenario[sat])) for sat in scenario.keys())  # sat->mission->time of info adquisition
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        if self.__sorted:
            missions = mission_sort_by_metric(range(len(mission_weight)), scenario, f_download_times,  self.__compute_buffer_reservation_time, reverse=self.__reverse)
        else:
            missions = range(len(mission_weight))

        for m in missions:
            m_weight = mission_weight[m]
            sat_ordered_by_buffer_reservation_time = []
            for sat in range(self._sat_num):
                try:
                    _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
                except StopIteration:
                    continue
                buffer_reservation_time, buffer = self.__compute_buffer_reservation_time(t, f_download_times[sat])
                sat_ordered_by_buffer_reservation_time.append((sat, buffer_reservation_time, buffer))

            # Min buffer permanence time optimization
            sat_ordered_by_buffer_reservation_time.sort(key=lambda x: x[1])
            eligible_satellites = [(sat, buffer_reservation_time, buffer) for sat, buffer_reservation_time, buffer in sat_ordered_by_buffer_reservation_time
                                   if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity]
            if len(eligible_satellites) == 0:
                continue # Any satellite is able to fulfill mission m
            eligible_satellites.sort(key=lambda x: x[1])
            eligible_satellites = list(filter(lambda x: x[1] == eligible_satellites[0][1], eligible_satellites))


            #Info age optimization (MBP)
            eligible_satellites_ordered_by_info_age = sorted([(sat, buffer_reservation_time, dt, dt - scenario_as_dict[sat][m]) for sat, buffer_reservation_time, dt in eligible_satellites], key=lambda x: x[3])
            eligible_satellites = list(filter(lambda x: x[3] == eligible_satellites_ordered_by_info_age[0][3], eligible_satellites_ordered_by_info_age))

            #Info delay optimization (MDT)
            eligible_satellites_ordered_by_info_delay = sorted([(sat, dt) for sat, buffer_reservation_time, dt, info_age in eligible_satellites], key=lambda x: x[1])
            eligible_satellites = list(sorted(filter(lambda x: x[1] == eligible_satellites_ordered_by_info_delay[0][1], eligible_satellites_ordered_by_info_delay), key=lambda x: x[0]))

            #Assign mission to lowest index satellite
            sat=eligible_satellites[0][0]
            dt = eligible_satellites[0][1]
            sat_assignment[sat][dt].append(m)
            assigned_capacity += m_weight
            time_of_buffer_booking[sat][m] = f_download_times[sat][f_download_times[sat].index(dt) - 1] if f_download_times[sat].index(dt) > 0 else 0


        elapsed_time = time.time() - start_time
        print(f"{self.__class__.__name__}:_compute_mission_assignment it takes {elapsed_time:.2f} - Sat:{self._sat_num}-SatCapacity:{self._sat_capacity}")
        return assigned_capacity, sat_assignment, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MO_MBRT_MBP_MDT(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)

class MO_MBRT_MDT_MBP(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator: 'ScenarioGenerator', sorted_=False, reverse=False):
        super(MO_MBRT_MDT_MBP, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__sorted = sorted_
        self.__reverse = reverse


    def __compute_buffer_reservation_time(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i] - (f_download_times[i-1] if i > 0 else 0),  f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement,Time_Of_Buffer_Booking]:
        start_time = time.time()
        scenario_as_dict = dict((sat, dict(scenario[sat])) for sat in scenario.keys())  # sat->mission->time of info adquisition
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        if self.__sorted:
            missions = mission_sort_by_metric(range(len(mission_weight)), scenario, f_download_times,  self.__compute_buffer_reservation_time, reverse=self.__reverse)
        else:
            missions = range(len(mission_weight))

        for m in missions:
            m_weight = mission_weight[m]
            sat_ordered_by_buffer_reservation_time = []
            for sat in range(self._sat_num):
                try:
                    _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
                except StopIteration:
                    continue
                buffer_reservation_time, buffer = self.__compute_buffer_reservation_time(t, f_download_times[sat])
                sat_ordered_by_buffer_reservation_time.append((sat, buffer_reservation_time, buffer))

            # Min buffer permanence time optimization
            sat_ordered_by_buffer_reservation_time.sort(key=lambda x: x[1])
            eligible_satellites = [(sat, buffer_reservation_time, buffer) for sat, buffer_reservation_time, buffer in sat_ordered_by_buffer_reservation_time
                                   if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity]
            if len(eligible_satellites) == 0:
                continue # Any satellite is able to fulfill mission m
            eligible_satellites.sort(key=lambda x: x[1])
            eligible_satellites = list(filter(lambda x: x[1] == eligible_satellites[0][1], eligible_satellites))


            #Info delay optimization (MDT)
            eligible_satellites_ordered_by_info_delay = sorted([(sat, dt) for sat, buffer_reservation_time, dt in eligible_satellites], key=lambda x: x[1])
            eligible_satellites = list(filter(lambda x: x[1] == eligible_satellites_ordered_by_info_delay[0][1], eligible_satellites_ordered_by_info_delay))

            #Info age optimization (MBP)
            eligible_satellites_ordered_by_info_age = sorted([(sat, dt, dt - scenario_as_dict[sat][m]) for sat, dt in eligible_satellites], key=lambda x: x[2])
            eligible_satellites = list(sorted(filter(lambda x: x[2] == eligible_satellites_ordered_by_info_age[0][2], eligible_satellites_ordered_by_info_age), key=lambda x: x[0]))

            #Assign mission to lowest index satellite
            sat=eligible_satellites[0][0]
            dt = eligible_satellites[0][1]
            sat_assignment[sat][dt].append(m)
            assigned_capacity += m_weight
            time_of_buffer_booking[sat][m] = f_download_times[sat][f_download_times[sat].index(dt) - 1] if f_download_times[sat].index(dt) > 0 else 0


        elapsed_time = time.time() - start_time
        print(f"{self.__class__.__name__}:_compute_mission_assignment it takes {elapsed_time:.2f} - Sat:{self._sat_num}-SatCapacity:{self._sat_capacity}")
        return assigned_capacity, sat_assignment, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MO_MBRT_MDT_MBP(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class TableAssigner(Scheme_13_8_19):
    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator: 'ScenarioGenerator', sorted_=False, reverse=False, w_brt:float=1., w_info_age:float=1., w_info_delay=1.):
        super(TableAssigner, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__sorted = sorted_
        self.__reverse = reverse
        self.__w_brt = w_brt
        self.__w_iage = w_info_age
        self.__w_idelay = w_info_delay


    def __compute_buffer_reservation_time(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i] - (f_download_times[i-1] if i > 0 else 0),  f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> Tuple[int, MissionAssignement,Time_Of_Buffer_Booking]:
        start_time = time.time()
        scenario_as_dict = dict((sat, dict(scenario[sat])) for sat in scenario.keys())  # sat->mission->time of info adquisition
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        time_of_buffer_booking = dict((sat, {}) for sat in range(self._sat_num))

        if self.__sorted:
            missions = mission_sort_by_metric(range(len(mission_weight)), scenario, f_download_times,  self.__compute_buffer_reservation_time, reverse=self.__reverse)
        else:
            missions = range(len(mission_weight))

        for m in missions:
            m_weight = mission_weight[m]
            sat_ordered_by_buffer_reservation_time = []
            for sat in range(self._sat_num):
                try:
                    _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
                except StopIteration:
                    continue
                buffer_reservation_time, buffer = self.__compute_buffer_reservation_time(t, f_download_times[sat])
                sat_ordered_by_buffer_reservation_time.append((sat, buffer_reservation_time, buffer))

            # Min buffer permanence time optimization
            eligible_satellites = [(sat, buffer_reservation_time, buffer) for sat, buffer_reservation_time, buffer in sat_ordered_by_buffer_reservation_time
                                   if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity]
            if len(eligible_satellites) == 0:
                continue # Any satellite is able to fulfill mission m

            eligible_satellites_ordered_by_min_buffer_reservation_time = list(map(lambda x: x[0], sorted(eligible_satellites, key=lambda x: x[1])))

            #Info delay optimization (MDT)
            eligible_satellites_ordered_by_info_delay = list(map(lambda x: x[0], sorted([(sat, dt) for sat, buffer_reservation_time, dt in eligible_satellites], key=lambda x: x[1])))

            #Info age optimization (MBP)
            eligible_satellites_ordered_by_info_age = list(map(lambda x: x[0], sorted([(sat, dt, dt - scenario_as_dict[sat][m]) for sat, _, dt in eligible_satellites], key=lambda x: x[2])))

            ranking = sorted([(sat, dt, self.__w_brt * eligible_satellites_ordered_by_min_buffer_reservation_time.index(sat)
                               + self.__w_idelay * eligible_satellites_ordered_by_info_delay.index(sat)
                               + self.__w_iage * eligible_satellites_ordered_by_info_age.index(sat))
                            for sat, _, dt in eligible_satellites],
                            key=lambda x: x[2])

            #Assign mission to lowest index satellite
            sat=ranking[0][0]
            dt = ranking[0][1]
            sat_assignment[sat][dt].append(m)
            assigned_capacity += m_weight
            time_of_buffer_booking[sat][m] = f_download_times[sat][f_download_times[sat].index(dt) - 1] if f_download_times[sat].index(dt) > 0 else 0


        elapsed_time = time.time() - start_time
        print(f"{self.__class__.__name__}:_compute_mission_assignment it takes {elapsed_time:.2f} - Sat:{self._sat_num}-SatCapacity:{self._sat_capacity}")
        return assigned_capacity, sat_assignment, time_of_buffer_booking


    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int, w_brt:float=1., w_info_age:float=1., w_info_delay=1.) -> List[int]:
        #print(f'{w_brt} {w_info_age} {w_info_delay}')
        exp = TableAssigner(sat_num, sat_capacity, scenario_generator, w_brt=w_brt, w_info_age=w_info_age, w_info_delay=w_info_delay)
        return exp._run_experiment(number_of_runs, exp)

def generate_f_download(num_of_downloads:int, t_end:int, random:random.Random):
    '''
    Auxiliary function
    Generates a list with a given number of downloads.
    A download list is a set of integer values, each one
    belonging to [1, t_end]
    :param num_of_downloads: >= 1
    :param t_end:
    :return:
    '''
    if num_of_downloads < 1:
        raise ValueError('num_of_downloads must be greater than 1')
    downloads = []
    dt = 0
    for i in range(1, num_of_downloads):
        dt = random.randint(dt + 1, t_end - num_of_downloads + i)
        downloads.append(dt)

    downloads.append(t_end)
    assert all([downloads[x] < downloads[x+1] for x in range(len(downloads) - 1)])
    return downloads


def mission_sort_by_metric(missions: List[int], scenario:Scenario, download_times:DownloadTimes, f_metric, reverse=False):
    to_sort_missions = []
    for m in missions:
        metric_evaluation_on_sats = []
        for sat in sorted(scenario.keys()):
            try:
                _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
            except StopIteration:
                continue
            metric_evaluation_on_sats.append(f_metric(t, download_times[sat])[0])
        metric = min(metric_evaluation_on_sats, default=sys.maxsize)
        to_sort_missions.append((m,metric))

    return map(lambda m: m[0], sorted(to_sort_missions, key = lambda x: x[1], reverse=reverse))


