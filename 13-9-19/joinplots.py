import os
from svgtogrid import combine_into_table_and_save

METRICS = [('served_load', 'Served Load'), ('adquisition_until_dw', 'Information Age'),
           ('buffer_booking_until_dw', 'Average time of Buffer Booking'), ('del_time', 'Delivery Time')]


os.makedirs('joinplots', exist_ok=True)
for metric in METRICS:
    files_m1 = [
        f'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.1,rep:25,seed:245,metric:{metric[0]}.svg',
        f'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.5,rep:25,seed:245,metric:{metric[0]}.svg',
        f'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.9,rep:25,seed:245,metric:{metric[0]}.svg',

        f'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.1,rep:25,seed:245,metric:{metric[0]}.svg',
        f'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.5,rep:25,seed:245,metric:{metric[0]}.svg',
        f'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.9,rep:25,seed:245,metric:{metric[0]}.svg',

        f'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.1,rep:25,seed:245,metric:{metric[0]}.svg',
        f'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.5,rep:25,seed:245,metric:{metric[0]}.svg',
        f'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.9,rep:25,seed:245,metric:{metric[0]}.svg',
    ]

    fname = f'joinplots/comparison-sat_num:[10,25,50],pr:[0.1,0.5,0.9],metric:{metric[0]}'
    combine_into_table_and_save(3, 3, 0, 0,files_m1, fname + '.svg')
    os.system('inkscape %s &'% (fname + '.svg'))
    os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

metric = ('served_load', 'Served Load')
files_m1 = [
    f'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.1,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
    f'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.5,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
    f'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.9,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',

    f'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.1,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
    f'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.5,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
    f'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.9,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',

    f'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.1,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
    f'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.5,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
    f'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.9,rep:25,seed:245,metric:{metric[0]},xrange:[0.7:1.0].svg',
]

fname = f'joinplots/comparison-sat_num:[10,25,50],pr:[0.1,0.5,0.9],metric:{metric[0]},xrange:[0.7:1.0]'
combine_into_table_and_save(3, 3, 0, 0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
