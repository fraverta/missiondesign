import os
from typing import List
from datetime import date
from utilities.utils import print_str_to_file
from imp_9_9_19 import Scheme_13_8_19, CitiesScenarioGenerator, MILP, MBP, LBB, FirstFit, FirstFit, BestFit, RandomFit, generate_f_download, ExperimentResult, ScenarioGenerator, Scheme_13_8_19, MBRT, MDT, SortedMBRT, SortedMDT, SortedMBP, ReverseSortedMBP, ReverseSortedMBRT, ReverseSortedMDT, MO_MILP_1,MO_MILP_2,  MO_MBRT_MBP_MDT, MO_MBRT_MDT_MBP, TableAssigner

SEED = 0
ALGORITHMS = [Scheme_13_8_19._MILP, Scheme_13_8_19._MO_MILP_1, Scheme_13_8_19._MO_MILP_2, Scheme_13_8_19._MBRT, Scheme_13_8_19._MO_MBRT_1, Scheme_13_8_19._MO_MBRT_2, Scheme_13_8_19._MBP, Scheme_13_8_19._MDT, Scheme_13_8_19._TA]
REPETITIONS = 25
FPATH = 'resources/sat_target_contacts_sats_per_plane:1_tend:86400.json'
SAT_NUM = 25
SAT_CAPACITY = 500/SAT_NUM
OUTPUT_DIR = 'results-%s-%s' % (os.path.basename(__file__), date.today().strftime("%d-%m-%Y"))
METRICS = [('served_load', 'Served Load'), ('adquisition_until_dw','Information Age'),('buffer_booking_until_dw', 'Average time of Buffer Booking'), ('del_time','Delivery Time')]
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]

def compute_metrics(results:List[ExperimentResult]):
    m1 = [(MISSION_PROPORTION[i], results[i].f_assigned_capacity / (SAT_CAPACITY * SAT_NUM)) for i in range(len(MISSION_PROPORTION))]
    m2 = [(MISSION_PROPORTION[i], results[i].f_data_adquisition_until_download) for i in range(len(MISSION_PROPORTION))]
    m3 = [(MISSION_PROPORTION[i], results[i].f_buffer_booking_until_download) for i in range(len(MISSION_PROPORTION))]
    m4 = [(MISSION_PROPORTION[i], results[i].f_delivery_time) for i in range(len(MISSION_PROPORTION))]

    return [m1, m2, m3, m4]

os.makedirs(OUTPUT_DIR, exist_ok=True)
os.makedirs(os.path.join(OUTPUT_DIR, 'txt'), exist_ok=True)
for alogrithm in ALGORITHMS:
    results = []
    for prop in MISSION_PROPORTION:
        mission_num = int(500 * prop)

        scenario_generator = CitiesScenarioGenerator(SAT_NUM, mission_num, FPATH,seed=SEED)
        results.append(Scheme_13_8_19.choose_and_run(alogrithm, SAT_NUM, SAT_CAPACITY, scenario_generator, REPETITIONS))

    metrics = compute_metrics(results)
    for m, metric in enumerate(METRICS):
        fname = f'Exp-{alogrithm}-sat_num:{SAT_NUM},sat_capacity:{SAT_CAPACITY},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]}'
        print_str_to_file(str(metrics[m]), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))