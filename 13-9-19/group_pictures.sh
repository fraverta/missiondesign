#!/usr/bin/env bash

SOURCE_PATH="results-new_mission_size=1.py-GUROBI-12-02-2020-tend:100-num_of_downloads"
OUTPUT_PATH="join_by_metric_1"
METRICS=('served_load' 'adquisition_until_dw' 'buffer_booking_until_dw' 'del_time' 'served_load,xrange:[0.7:1.0]')

mkdir "${OUTPUT_PATH}"
for metric in ${METRICS[@]}; do
    join_destination_path="${OUTPUT_PATH}/${metric}";
    mkdir ${join_destination_path};
    for i in {1..4}; do
        f_source_path="${SOURCE_PATH}:$i/joinplots/comparison-MILP,MBP,MBRT,MDT,MO_MILP_1,MO_MILP_2,MO-MBRT-MBP-MDT,MO-MBRT-MDT-MBP,TA-sat_num:[10,25,50],pr:[0.1,0.5,0.9],metric:$metric.png";
        echo "cp ${f_source_path} ${join_destination_path}"
        cp ${f_source_path} "${join_destination_path}/num_of_downloads:$i.png";
    done;
done;


