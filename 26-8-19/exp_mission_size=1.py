import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from imp_13_8_19 import  MILP, UserScenarioGenerator, MBP, LBB, FirstFit, FirstFit, BestFit, RandomFit, generate_f_download
import time
from random import Random
from fractions import gcd
start_time = time.time()


SAT_NUM_RANGE = [10, 25, 50]
PR_SAT_DO_A_MISISON_RANGE = [0.1, 0.5, 0.9]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
REPETITIONS = 25
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 5 #VALID FOR RANDOM ASSIGNMENT
NUM_OF_DOWNLOADS = 1
T_END_RANGE = [100, 1000]
random = Random(10)


for t_end in T_END_RANGE:
    OUTPUT_DIR = 'results-%s-%s-tend:%d-num_of_downloads:%d' % (os.path.basename(__file__), date.today().strftime("%d-%m-%Y"), t_end, NUM_OF_DOWNLOADS)
    os.system('mkdir ' + OUTPUT_DIR)
    os.system('mkdir ' + OUTPUT_DIR + '/png')
    os.system('mkdir ' + OUTPUT_DIR + '/svg')
    os.system('mkdir ' + OUTPUT_DIR + '/txt')
    for pr_sat_to_do_mission in PR_SAT_DO_A_MISISON_RANGE:
        for sat_num in SAT_NUM_RANGE:
            system_capacity = sat_num * NUM_OF_DOWNLOADS * SAT_CAPACITY
            results_milp = []
            results_mbp = []
            results_lbb = []
            results_ra = []
            results_ff = []
            results_bf = []
            #results_mission_aware = []
            f_download = [generate_f_download(NUM_OF_DOWNLOADS, t_end, random) for sat in range(sat_num)]
            for offered_load_prop in MISSION_PROPORTION:
                print("[Running] tend:%d,sat_num:%d,sat_capacity:%d,offeredload:%.2f,pr:%.2f,rep:%d " % (t_end,sat_num, SAT_CAPACITY, offered_load_prop, pr_sat_to_do_mission, REPETITIONS))
                offered_load = round(system_capacity * offered_load_prop)
                f_mission_weight = [1 for i in range(offered_load)]

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_milp.append(MILP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mbp.append(MBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_lbb.append(LBB.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_ra.append(RandomFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, RUNS_PER_SCENARIO, seed= SEED + 10))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_ff.append(FirstFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_bf.append(BestFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, T_END, randomize_mission_cost=True, seed=SEED)
                # results_mission_aware.append(MissionAwareFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))


            lprom_milp = [(MISSION_PROPORTION[i], average(results_milp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_mbp = [(MISSION_PROPORTION[i], average(results_mbp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_lbb = [(MISSION_PROPORTION[i], average(results_lbb[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_ra =[(MISSION_PROPORTION[i], average(results_ra[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_ff = [(MISSION_PROPORTION[i], average(results_ff[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            lprom_bf = [(MISSION_PROPORTION[i], average(results_bf[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            #lprom_mission_aware = [(MISSION_PROPORTION[i], average(results_mission_aware[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]

            fname = 'Exp-MILP-sat_num:%d,sat_capacity:%d,point:%s,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            print_str_to_file(str(lprom_milp), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_milp, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')
            #lprom_milp = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-MBP-sat_num:%d,sat_capacity:%d,point:%s,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            print_str_to_file(str(lprom_mbp), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_mbp, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')
            #lprom_mbp = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-LBB-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            print_str_to_file(str(lprom_lbb), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_lbb, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            #lprom_lbb = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-RA-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            print_str_to_file(str(lprom_ra), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_ra, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')
            #lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]

            fname = 'Exp-FF-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            print_str_to_file(str(lprom_ff), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_ff, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            #lprom_ff = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            fname = 'Exp-BF-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            print_str_to_file(str(lprom_bf), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_bf, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            #lprom_bf = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))

            #fname = 'Exp-missionAware-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            #print_str_to_file(str(l), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            ##plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            #lprom_mission_aware = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]


            fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            plot_nfunctions([lprom_milp, lprom_mbp, lprom_lbb, lprom_ff, lprom_bf], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP','MBP','LBB', 'FF', 'BF'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'orange', 'blue', 'green', 'magenta'], ftype='png', yticks=(0.,1.1,.2))
            plot_nfunctions([lprom_milp, lprom_mbp, lprom_lbb, lprom_ff, lprom_bf], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP','MBP','LBB', 'FF', 'BF'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'orange', 'blue', 'green', 'magenta'], ftype='svg', yticks=(0.,1.1,.2))

            fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,pr:%.2f,rep:%d,seed:%d-[0.7:1.0]'%(sat_num, SAT_CAPACITY, pr_sat_to_do_mission, REPETITIONS, SEED)
            plot_nfunctions([lprom_milp[6:], lprom_mbp[6:], lprom_lbb[6:], lprom_ff[6:], lprom_bf[6:]], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP','MBP','LBB', 'FF', 'BF'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'orange', 'blue', 'green', 'magenta'], ftype='png', yticks=(0.5, 1.05, .1))
            plot_nfunctions([lprom_milp[6:], lprom_mbp[6:], lprom_lbb[6:], lprom_ff[6:], lprom_bf[6:]], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP','MBP','LBB', 'FF', 'BF'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'orange', 'blue', 'green', 'magenta'], ftype='svg', yticks=(0.5, 1.05, .1))

print("Elapsed time: %f seconds"%(time.time() - start_time))

