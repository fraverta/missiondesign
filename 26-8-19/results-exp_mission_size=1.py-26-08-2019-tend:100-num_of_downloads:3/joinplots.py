import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.10,rep:25,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.50,rep:25,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.90,rep:25,seed:245.svg',

    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.10,rep:25,seed:245.svg',
    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.50,rep:25,seed:245.svg',
    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.90,rep:25,seed:245.svg',

    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.10,rep:25,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.50,rep:25,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.90,rep:25,seed:245.svg',
]



os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,25,50],pr:[0.1,0.5,0.9]'
combine_into_table_and_save(3, 3, 0, 0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))


files_m1 = [
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.10,rep:25,seed:245-[0.7:1.0].svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.50,rep:25,seed:245-[0.7:1.0].svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,pr:0.90,rep:25,seed:245-[0.7:1.0].svg',

    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.10,rep:25,seed:245-[0.7:1.0].svg',
    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.50,rep:25,seed:245-[0.7:1.0].svg',
    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,pr:0.90,rep:25,seed:245-[0.7:1.0].svg',

    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.10,rep:25,seed:245-[0.7:1.0].svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.50,rep:25,seed:245-[0.7:1.0].svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,pr:0.90,rep:25,seed:245-[0.7:1.0].svg',

]



os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,25,50],pr:[0.1,0.5,0.9]-[0.7:1.0]'
combine_into_table_and_save(3, 3, 0, 0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
