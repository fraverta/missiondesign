#!/usr/bin/env bash

virtualenv venv -p /usr/bin/python3.6 &&
source venv/bin/activate &&
pip install -r requeriments.txt &&
working_dir=$PWD
cd /opt/ibm/ILOG/CPLEX_Studio129/cplex/python/3.6/x86-64_linux/  &&
$working_dir/venv/bin/python3.6 setup.py install
cd $working_dir
