import  sys
sys.path.insert(0,'../')
from utilities.utils import getListFromFile

SAT_NUM = 100

milp = getListFromFile('txt/Exp-OfflineLP-sat_num:%d,sat_capacity:10,point:AB,rep:35,seed:245.txt'%(SAT_NUM))
lbb  = getListFromFile('txt/Exp-OnlineGreedy-sat_num:%d,sat_capacity:10,point:AB,rep:35,seed:245.txt'%(SAT_NUM))
rf   = getListFromFile('txt/Exp-OnlineRandom-sat_num:%d,sat_capacity:10,point:AB,rep:35,seed:245.txt'%(SAT_NUM))
ff   = getListFromFile('txt/Exp-firstfit-sat_num:%d,sat_capacity:10,point:AB,rep:35,seed:245.txt'%(SAT_NUM))
bf   = getListFromFile('txt/Exp-bestfit-sat_num:%d,sat_capacity:10,point:AB,rep:35,seed:245.txt'%(SAT_NUM))

results = zip(milp, lbb, rf, ff, bf)
print('%4s  %5s  %5s  %5s  %5s  %5s'%('', 'milp','lbb', 'rf', 'ff', 'bf'))
print ('-' * 34)
for f1, f2, f3, f4, f5 in results:
    print('%4.2f  %4.3f  %4.3f  %4.3f  %4.3f  %4.3f' % (f1[0], f1[1], f2[1], f3[1], f4[1], f5[1]))
