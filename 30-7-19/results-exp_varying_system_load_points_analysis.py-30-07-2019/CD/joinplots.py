import os
from svgtogrid import combine_into_table_and_save
import shutil

shutil.rmtree('joinplots')
os.makedirs('joinplots', exist_ok=True)

f1 = [
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,point:CD,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,point:CD,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,point:CD,rep:35,seed:245.svg',
]


fname = 'joinplots/comparison-sat_num:[10,50,100],sat_capacity:10,varying_load'
combine_into_table_and_save(1, 3, 0, 0,f1, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

f2 = [
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,point:CD,rep:35,seed:245,[0.8:1.0].svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,point:CD,rep:35,seed:245,[0.8:1.0].svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,point:CD,rep:35,seed:245,[0.8:1.0].svg',
]


fname = 'joinplots/comparison-sat_num:[10,50,100],sat_capacity:10,varying_load,[0.8:1.0]'
combine_into_table_and_save(1, 3, 0, 0,f2, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

f3 = f1 + f2
fname = 'joinplots/comparison-sat_num:[10,50,100],sat_capacity:10,varying_load-ZOOM'
combine_into_table_and_save(2, 3, 0, 0,f3, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
