import os
from svgtogrid import combine_into_table_and_save


files_m1 = [
    #'svg/Exp-Comparison-sat_num:25,sat_capacity:1,pr:%.2f,rep:35,seed:245.svg'%pr,
    # 'prop_A_prop_B/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:0.50,prop_B:0.50,rep:35,seed:245.svg',
    # 'prop_A_prop_B/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:0.50,prop_B:0.50,rep:35,seed:245.svg',
    # 'prop_A_prop_B/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:0.50,prop_B:0.50,rep:35,seed:245.svg',

    'prop_A_prop_AB/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    'prop_A_prop_AB/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    'prop_A_prop_AB/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    #
    # 'prop_B_prop_AB/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    # 'prop_B_prop_AB/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    # 'prop_B_prop_AB/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.50,prop_AB:0.50,rep:35,seed:245.svg',
]

outputdir = 'joinplots-A-AB-B'
os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_A:0.50,prop_AB:0.50')
combine_into_table_and_save(1, 3, 0, 0, files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

SCENARIO_PROPORTION = [(0.5, 0.5), (.9,.1), (.1,.9), (.7,.3), (.3,.7)]
for prop_1, prop_2 in SCENARIO_PROPORTION:
    files_m1 = [
        'prop_A_prop_B/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:%.2f,prop_B:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
        'prop_A_prop_B/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:%.2f,prop_B:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
        'prop_A_prop_B/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:%.2f,prop_B:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),

        'prop_A_prop_AB/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:%.2f,prop_AB:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
        'prop_A_prop_AB/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:%.2f,prop_AB:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
        'prop_A_prop_AB/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:%.2f,prop_AB:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),

        'prop_B_prop_AB/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:%.2f,prop_AB:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
        'prop_B_prop_AB/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:%.2f,prop_AB:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
        'prop_B_prop_AB/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:%.2f,prop_AB:%.2f,rep:35,seed:245.svg'%(prop_1,prop_2),
    ]

    fname = os.path.join(outputdir, 'comparison-sat_num:[10,50,100],%.2f,%.2f'%(prop_1,prop_2))
    combine_into_table_and_save(3, 3, 0, 0, files_m1, fname + '.svg')
    os.system('inkscape %s &' % (fname + '.svg'))
    os.system('inkscape --export-png=%s %s' % (fname + '.png', fname + '.svg'))

