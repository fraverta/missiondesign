import os
from svgtogrid import combine_into_table_and_save


files_m1 = [
    # 'prop_B_prop_I/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.50,prop_I:0.50,rep:35,seed:245.svg',
    # 'prop_B_prop_I/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.50,prop_I:0.50,rep:35,seed:245.svg',
    # 'prop_B_prop_I/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.50,prop_I:0.50,rep:35,seed:245.svg',
    #
    # 'prop_C_prop_I/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_C:0.50,prop_I:0.50,rep:35,seed:245.svg',
    # 'prop_C_prop_I/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_C:0.50,prop_I:0.50,rep:35,seed:245.svg',
    # 'prop_C_prop_I/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_C:0.50,prop_I:0.50,rep:35,seed:245.svg',


    'prop_BC_prop_AD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_BC:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_BC_prop_AD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_BC:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_BC_prop_AD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_BC:0.50,prop_AD:0.50,rep:35,seed:245.svg',

    # 'prop_I_prop_AD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    # 'prop_I_prop_AD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    # 'prop_I_prop_AD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    #
    # 'prop_I_prop_BC/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
    # 'prop_I_prop_BC/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
    # 'prop_I_prop_BC/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',

]

outputdir = 'joinplots-I-AB-CD'
os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_BC:0.50,prop_AD:0.50')
combine_into_table_and_save(1, 3, 0, 0, files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

