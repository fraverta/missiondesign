import os
from svgtogrid import combine_into_table_and_save
import shutil

outputdir = 'joinplots-presentacion-05-08-19'
shutil.rmtree(outputdir)

A_B_files = [
    'prop_A_prop_B/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:0.50,prop_B:0.50,rep:35,seed:245.svg',
    'prop_A_prop_B/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:0.50,prop_B:0.50,rep:35,seed:245.svg',
    'prop_A_prop_B/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:0.50,prop_B:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_A:0.50,prop_B:0.50')
combine_into_table_and_save(1, 3, 0, 0, A_B_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del A_B_files

AD_BC_files = [
    'prop_BC_prop_AD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_BC:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_BC_prop_AD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_BC:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_BC_prop_AD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_BC:0.50,prop_AD:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_AD:0.50,prop_BC:0.50')
combine_into_table_and_save(1, 3, 0, 0, AD_BC_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del AD_BC_files


C_D_files = [
    'prop_C_prop_D/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_C:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'prop_C_prop_D/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_C:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'prop_C_prop_D/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_C:0.50,prop_D:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_C:0.50,prop_D:0.50')
combine_into_table_and_save(1, 3, 0, 0, C_D_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del C_D_files

A_AB_files = [
    'prop_A_prop_AB/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    'prop_A_prop_AB/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    'prop_A_prop_AB/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:0.50,prop_AB:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_A:0.50,prop_AB:0.50')
combine_into_table_and_save(1, 3, 0, 0, A_AB_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del A_AB_files

B_AB_files = [
    'prop_B_prop_AB/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    'prop_B_prop_AB/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.50,prop_AB:0.50,rep:35,seed:245.svg',
    'prop_B_prop_AB/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.50,prop_AB:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_B:0.50,prop_AB:0.50')
combine_into_table_and_save(1, 3, 0, 0, B_AB_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del B_AB_files

I_AD_files = [
    'prop_I_prop_AD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_I_prop_AD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_I_prop_AD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_I:0.50,prop_AD:0.50')
combine_into_table_and_save(1, 3, 0, 0, I_AD_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del I_AD_files

I_BC_files = [
    'prop_I_prop_BC/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
    'prop_I_prop_BC/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
    'prop_I_prop_BC/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_I:0.50,prop_BC:0.50')
combine_into_table_and_save(1, 3, 0, 0, I_BC_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del I_BC_files

D_CD_files = [
    'prop_D_prop_CD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'prop_D_prop_CD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'prop_D_prop_CD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_D:0.50,prop_CD:0.50')
combine_into_table_and_save(1, 3, 0, 0, D_CD_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del D_CD_files

C_CD_files = [
    'prop_C_prop_CD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_C:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'prop_C_prop_CD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_C:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'prop_C_prop_CD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_C:0.50,prop_CD:0.50,rep:35,seed:245.svg',
]


os.makedirs(outputdir, exist_ok=True)
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_C:0.50,prop_CD:0.50')
combine_into_table_and_save(1, 3, 0, 0, C_CD_files, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del C_CD_files



IxA_AD_D = [

    'prop_A_prop_I/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_A:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_A_prop_I/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_A:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_A_prop_I/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_A:0.50,prop_I:0.50,rep:35,seed:245.svg',

    'prop_I_prop_AD/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_I_prop_AD/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',
    'prop_I_prop_AD/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_I:0.50,prop_AD:0.50,rep:35,seed:245.svg',

    'prop_D_prop_I/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_D:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_D_prop_I/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_D:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_D_prop_I/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_D:0.50,prop_I:0.50,rep:35,seed:245.svg',
]

os.makedirs(outputdir, exist_ok=True)
#fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_I:0.50,prop_AB:0.50')
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_I:0.50x[prop_A:0.50,prop_AD:0.50,prop_D:0.50]')
combine_into_table_and_save(3, 3, 0, 0, IxA_AD_D, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del IxA_AD_D

IxB_BC_C = [
    'prop_B_prop_I/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_B_prop_I/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_B_prop_I/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.50,prop_I:0.50,rep:35,seed:245.svg',

    'prop_I_prop_BC/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
    'prop_I_prop_BC/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',
    'prop_I_prop_BC/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_I:0.50,prop_BC:0.50,rep:35,seed:245.svg',

    'prop_C_prop_I/svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_C:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_C_prop_I/svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_C:0.50,prop_I:0.50,rep:35,seed:245.svg',
    'prop_C_prop_I/svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_C:0.50,prop_I:0.50,rep:35,seed:245.svg',
]

os.makedirs(outputdir, exist_ok=True)
#fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_I:0.50,prop_AB:0.50')
fname = os.path.join(outputdir,'comparison-sat_num:[10,50,100],prop_I:0.50x[prop_B:0.50,prop_BC:0.50,prop_C:0.50]')
combine_into_table_and_save(3, 3, 0, 0, IxB_BC_C, fname + '.svg')
#os.system('inkscape %s &'% (fname + '.svg'))
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
del IxB_BC_C