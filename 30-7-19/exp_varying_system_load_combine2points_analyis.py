import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from varying_mission_size.imp_19_7_19 import OnlineRandomAssignment, Offline_LP, Online, FirstFit, BestFit, SmoothExtremmeMissionGenerator
import time
from itertools import  combinations
start_time = time.time()



OUTPUT_DIR = 'results-%s-%s'%(os.path.basename(__file__), date.today().strftime("30-07-2019")) #'results-%s-%s'%(os.path.basename(__file__), date.today().strftime("%d-%m-%Y"))
POINTS = ['prop_A', 'prop_B', 'prop_C', 'prop_D', 'prop_I', 'prop_AB', 'prop_BC', 'prop_CD', 'prop_AD']
TWO_POINTS_CASES = list(combinations(list(POINTS), 2))
if len(sys.argv) > 1:
    slice = int(sys.argv[1])
    TWO_POINTS_CASES = TWO_POINTS_CASES[slice * 9: (slice + 1) * 9]
    print("[?] It will run from [%d: %d]"%(slice * 9, (slice + 1) * 9))
    print('Press a key or Cancel')
    input('Press Key')

SAT_NUM_RANGE = [10, 50, 100]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
SCENARIO_PROPORTION = [(0.5, 0.5), (.9,.1), (.1,.9), (.7,.3), (.3,.7)]
REPETITIONS = 35
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT

os.system('mkdir ' + OUTPUT_DIR)

for p1, p2 in TWO_POINTS_CASES:
    OUTPUT_DIR_CASE = os.path.join(OUTPUT_DIR, p1 + '_' + p2)
    os.system('mkdir ' + OUTPUT_DIR_CASE)
    os.system('mkdir ' + OUTPUT_DIR_CASE + '/png')
    os.system('mkdir ' + OUTPUT_DIR_CASE + '/svg')
    os.system('mkdir ' + OUTPUT_DIR_CASE + '/txt')
    for mission_proportion in SCENARIO_PROPORTION:
        mission_proportion_case = dict(zip([p1,p2], mission_proportion))
        for sat_num in SAT_NUM_RANGE:
            system_capacity = sat_num * SAT_CAPACITY
            # results_offline_lp = []
            # results_online_greedy = []
            # results_online_random = []
            # results_firstfit = []
            # results_bestfit = []
            for offered_load_prop in MISSION_PROPORTION:
                print("[Running] sat_num:%d,sat_capacity:%d,offeredload:%.2f,%s:%.2f,%s:%.2f,rep:%d " % (sat_num, SAT_CAPACITY, offered_load_prop, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS))
                offered_load = system_capacity * offered_load_prop

            #     scenario_generator = SmoothExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            #     results_offline_lp.append(Offline_LP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
            #
            #     scenario_generator = SmoothExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            #     results_online_greedy.append(Online.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
            #
            #     scenario_generator = SmoothExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            #     results_online_random.append(OnlineRandomAssignment.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, RUNS_PER_SCENARIO, seed= SEED + 10))
            #
            #     scenario_generator = SmoothExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            #     results_firstfit.append(FirstFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
            #
            #     scenario_generator = SmoothExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            #     results_bestfit.append(BestFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
            #
            #
            # lprom_offline_lp = [(MISSION_PROPORTION[i], average(results_offline_lp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_online_greedy = [(MISSION_PROPORTION[i], average(results_online_greedy[i])/ system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_online_random =[(MISSION_PROPORTION[i], average(results_online_random[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_firstfit = [(MISSION_PROPORTION[i], average(results_firstfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_bestfit = [(MISSION_PROPORTION[i], average(results_bestfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            try:
                fname = 'Exp-OfflineLP-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS, SEED)
                #print_str_to_file(str(lprom_offline_lp), os.path.join(OUTPUT_DIR_CASE, 'txt' , fname + '.txt'))
                #plot_function(lprom_offline, os.path.join(OUTPUT_DIR_CASE, 'png' , fname), ftype='png')
                lprom_offline_lp = getListFromFile(os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))

                fname = 'Exp-OnlineGreedy-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS, SEED)
                #print_str_to_file(str(lprom_online_greedy), os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))
                #plot_function(lprom_online, os.path.join(OUTPUT_DIR_CASE, 'png' ,fname), ftype='png')
                lprom_online_greedy = getListFromFile(os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))

                fname = 'Exp-OnlineRandom-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS, SEED)
                #print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))
                #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR_CASE, 'png', fname), ftype='png')
                lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))

                fname = 'Exp-firstfit-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS, SEED)
                #print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))
                #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR_CASE, 'png' ,fname), ftype='png')
                lprom_firstfit = getListFromFile(os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))

                fname = 'Exp-bestfit-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS, SEED)
                #print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))
                #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR_CASE, 'png' ,fname), ftype='png')
                lprom_bestfit = getListFromFile(os.path.join(OUTPUT_DIR_CASE, 'txt', fname + '.txt'))


                fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, p1, mission_proportion[0], p2, mission_proportion[1], REPETITIONS, SEED)
                plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR_CASE, 'png', fname),
                                labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                                xlabel='Offered Load',
                                ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='png', yticks=(0,1.1,0.1))
                plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR_CASE, 'svg', fname),
                                labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
                                xlabel='Offered Load',
                                ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg', yticks=(0,1.2,0.2))
            except:
                print("Unexpected error:", sys.exc_info()[0])
                print("[Exception] CASE: sat_num:%d,sat_capacity:%d,offeredload:%.2f,%s:%.2f,%s:%.2f,rep:%d " % (sat_num, SAT_CAPACITY, offered_load_prop, p1, mission_proportion[0], p2, mission_proportion[1],  REPETITIONS))

print("Elapsed time: %f seconds"%(time.time() - start_time))