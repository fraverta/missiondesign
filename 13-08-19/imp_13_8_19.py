from typing import Dict, List
import random
from abc import abstractmethod
from pulp import *
import time
from copy import copy
from typing import Tuple, List

class Scheme_13_8_19:

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator'):

        self._sat_num = sat_num
        self._sat_capacity = sat_capacity
        self._scenario_generator = scenario_generator

    @abstractmethod
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight:List[int], f_download_times: List[List[int]]) -> int:
        pass

    @staticmethod
    @abstractmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator, number_of_runs, seed=0) -> List[int]:
        pass

    @staticmethod
    def _run_experiment(number_of_runs, exp: 'Scheme_13_8_19') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = exp._scenario_generator.generate_scenario()
            result.append(exp._compute_mission_assignment(scenario, mission_weight, f_downloads_times)[0])

        return result


class MILP(Scheme_13_8_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> int:
        start_time = time.time()
        mission_num = len(mission_weight)
        if all(len(scenario[sat]) == 0 for sat in range(self._sat_num)):
            return 0, dict((sat, []) for sat in range(self._sat_num))
        else:
            # vars
            sat_mission_idx = []
            for sat in range(self._sat_num):
                for m, t in scenario[sat]:
                    previous_download_time = 0
                    for download_time in f_download_times[sat]:
                        if previous_download_time <= t < download_time:
                            sat_mission_idx.append('Sat%d_M%d_D%d' % (sat, m, download_time))
                            break
                        previous_download_time = download_time

            lp_problem = LpProblem("The Mission Problem", LpMaximize)
            sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)

            #Objetive Function
            f_obj_terms = []
            for sat in range(self._sat_num):
                for m, t in scenario[sat]:
                    previous_download_time = 0
                    for download_time in f_download_times[sat]:
                        if previous_download_time <= t < download_time:
                            f_obj_terms.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d' %(sat, m, download_time)])
                            break
                        previous_download_time = download_time
            lp_problem += lpSum(f_obj_terms), "Total assigned capacity"

            # CONSTRAINT: Mission are assigned to atmost 1 satellite buffer
            for m in range(mission_num):
                sat_able_to_fulfill_m_vars = [sat_mission_vars['Sat%d_M%d_D%d'%(sat, m, dt)] for sat in range(self._sat_num) for dt in f_download_times[sat] if 'Sat%d_M%d_D%d' % (sat, m, dt) in sat_mission_vars.keys()]
                lp_problem += lpSum([x for x in sat_able_to_fulfill_m_vars]) <= 1, 'Atmost 1 satelite assigned to mission %d'% m

            # CONSTRAINT: Satellite buffer are not overloaded
            for sat in range(self._sat_num):
                for dt in f_download_times[sat]:
                    mission_per_sat_buffer_vars = []
                    for m, _ in scenario[sat]:
                        if 'Sat%d_M%d_D%d'%(sat, m, dt) in sat_mission_vars.keys():
                            mission_per_sat_buffer_vars.append(mission_weight[m] * sat_mission_vars['Sat%d_M%d_D%d'%(sat, m, dt)])
                    lp_problem += lpSum(mission_per_sat_buffer_vars) <= self._sat_capacity, 'Sat %d - download %d - capacity constraint'%(sat, dt)

            lp_problem.solve(CPLEX())
            if lp_problem.status != LpStatusOptimal:
                raise ValueError("MILP solutions is not optimal")

            sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
            for v in sat_mission_vars.values():
                if v.varValue:
                    sat = int(v.name[4: v.name.index('M') - 1])
                    mission = int(v.name[v.name.rindex('M') + 1: v.name.index('D') - 1])
                    download_time = int(v.name[v.name.rindex('D') + 1:])

                    sat_assignment[sat][download_time].append(mission)

            elapsed_time = time.time() - start_time
            print("MILP:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
            return value(lp_problem.objective), sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MILP(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class MBP(Scheme_13_8_19):

    def __compute_permanence(self, t: int, f_download_times: List[int]) -> Tuple[int, int]:
        '''

        :param t:
        :param f_download_times:
        :return: (permanence time, buffer)
        '''
        i = 0
        while t >= f_download_times[i]:
            i += 1

        return f_download_times[i] - t,  f_download_times[i]

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, dict((dt, []) for dt in f_download_times[sat])) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(len(mission_weight)):
            m_weight = mission_weight[m]
            sat_ordered_by_busy_permanence = []
            for sat in range(self._sat_num):
                try:
                    _, t = next(filter(lambda x: x[0] == m, scenario[sat]))
                except StopIteration:
                    continue
                permanence_time, buffer = self.__compute_permanence(t, f_download_times[sat])
                sat_ordered_by_busy_permanence.append((sat, permanence_time, buffer))

            sat_ordered_by_busy_permanence.sort(key=lambda x: x[1])
            for sat, permanence_time, buffer in sat_ordered_by_busy_permanence:
                if sum(mission_weight[am] for am in sat_assignment[sat][buffer]) + mission_weight[m] <= self._sat_capacity:
                    sat_assignment[sat][buffer].append(m)
                    assigned_capacity += m_weight
                    break

        elapsed_time = time.time() - start_time
        print("MBP:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = MBP(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)

class LBB(Scheme_13_8_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> int:
        start_time = time.time()
        sat_assignment = [{'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])} for sat in range(self._sat_num)]
        assigned_capacity = 0

        to_assign_missions = list(range(len(mission_weight)))
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1], reverse=True)
                sat_assigned = False
                for sat, remained_capacity in sat_ordered_by_busy_buffer:
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[sat] if m == mm and t <= tt]
                    if len(time_able_to_fulfill_m) > 0 and remained_capacity - mission_weight[m] >= 0:
                        m_dt = [dt for dt in f_download_times[sat] if dt > time_able_to_fulfill_m[0]][0]
                        sat_assignment[sat]['assigned_missions'][m_dt].append(m)
                        sat_assignment[sat]['remained_capacity'] -= m_weight
                        assigned_capacity += m_weight
                        to_assign_missions.pop(i)
                        sat_assigned = True
                        break

                if not sat_assigned:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("LBB:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = LBB(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class FirstFit(Scheme_13_8_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> int:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])}) for sat in range(self._sat_num))

        to_assign_missions = list(range(len(mission_weight)))
        assigned_capacity = 0
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                sat_assigned = False
                for sat in range(self._sat_num):
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[sat] if m == mm and t <= tt]
                    if len(time_able_to_fulfill_m) > 0 and sat_assignment[sat]['remained_capacity'] - mission_weight[m] >= 0:
                        m_dt = [dt for dt in f_download_times[sat] if dt > time_able_to_fulfill_m[0]][0]
                        sat_assignment[sat]['assigned_missions'][m_dt].append(m)
                        sat_assignment[sat]['remained_capacity'] -= m_weight
                        assigned_capacity += m_weight
                        to_assign_missions.pop(i)
                        sat_assigned = True
                        break

                if not sat_assigned:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = FirstFit(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)


class BestFit(Scheme_13_8_19):
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int], f_download_times: List[List[int]]) -> int:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_assignment = [{'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])} for sat in range(self._sat_num)]
        assigned_capacity = 0

        to_assign_missions = list(range(len(mission_weight)))
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1])
                sat_assigned = False
                for sat, remained_capacity in sat_ordered_by_busy_buffer:
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[sat] if m == mm and t <= tt]
                    if len(time_able_to_fulfill_m) > 0 and remained_capacity - mission_weight[m] >= 0:
                        m_dt = [dt for dt in f_download_times[sat] if dt > time_able_to_fulfill_m[0]][0]
                        sat_assignment[sat]['assigned_missions'][m_dt].append(m)
                        sat_assignment[sat]['remained_capacity'] -= m_weight
                        assigned_capacity += m_weight
                        to_assign_missions.pop(i)
                        sat_assigned = True
                        break

                if not sat_assigned:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("BestFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = BestFit(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(number_of_runs, exp)

class RandomFit(Scheme_13_8_19):

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', seed=0):
        super(RandomFit, self).__init__(sat_num, sat_capacity, scenario_generator)
        self.__random = random.Random(seed)


    def _compute_mission_assignment(self, scenario: Dict[(int, List[Tuple[int,int]])], mission_weight: List[int], f_download_times: List[List[int]]) -> int:
        start_time = time.time()
        sat_assignment = [{'remained_capacity': self._sat_capacity, 'assigned_missions': dict((dt, []) for dt in f_download_times[sat])} for sat in range(self._sat_num)]
        assigned_capacity = 0

        to_assign_missions = list(range(len(mission_weight)))
        for t in range(max([dt for sat in range(self._sat_num) for dt in f_download_times[sat]])):
            for sat in range(self._sat_num):
                if t in f_download_times[sat]:
                    # Satellite download missions that was already fullfil but buffer is still busy for missions it plans to adquire in the future
                    sat_assignment[sat]['remained_capacity'] = self._sat_capacity
                    for x in f_download_times[sat][f_download_times[sat].index(t) + 1: ]:
                        sat_assignment[sat]['remained_capacity'] -= sum(mission_weight[m] for m in sat_assignment[sat]['assigned_missions'][x])

            i = 0
            while i < len(to_assign_missions):
                m = to_assign_missions[i]
                m_weight = mission_weight[m]
                choseable_sats = [sat for sat in range(self._sat_num) if len([tt for mm, tt in scenario[sat] if m == mm and t <= tt]) > 0 and sat_assignment[sat]['remained_capacity'] >= m_weight]
                if len(choseable_sats) > 0:
                    chosed_sat = self.__random.choice(choseable_sats)
                    time_able_to_fulfill_m = [tt for mm, tt in scenario[chosed_sat] if m == mm and t <= tt][0]
                    m_dt = [dt for dt in f_download_times[chosed_sat] if dt > time_able_to_fulfill_m][0]
                    sat_assignment[chosed_sat]['assigned_missions'][m_dt].append(m)
                    sat_assignment[chosed_sat]['remained_capacity'] -= m_weight
                    assigned_capacity += m_weight
                    to_assign_missions.pop(i)
                else:
                    i += 1

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("RandomFit:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int, number_of_runs_per_scenario:int, seed=0) -> List[int]:
        exp = RandomFit(sat_num, sat_capacity, scenario_generator, seed)
        return exp._run_experiment(number_of_runs, number_of_runs_per_scenario, exp)

    @staticmethod
    def _run_experiment(number_of_runs:int, number_of_runs_per_scenario: int, exp: 'Online_Random_Assignment') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = exp._scenario_generator.generate_scenario()
            scenario_result = 0.
            for j in range(number_of_runs_per_scenario):
                scenario_result += exp._compute_mission_assignment(scenario, mission_weight, f_downloads_times)[0]
            result.append(scenario_result / number_of_runs_per_scenario)

        return result


class ScenarioGenerator():
    def __init__(self, sat_num, mission_num, seed=0):
        self._sat_num = sat_num
        self._mission_num = mission_num
        self._random = random.Random(seed)


class UserScenarioGenerator(ScenarioGenerator):
    '''
        Genate random scenarios in which cost are provided by the user class
    '''

    def __init__(self, sat_num: int, mission_num: int, pr_sat_to_do_mission: float, f_mission_weight: List[int], t_end: int, f_download: List[int], randomize_mission_cost=False, seed=0):
        super(UserScenarioGenerator, self).__init__(sat_num, mission_num, seed)
        self.__pr_sat_to_do_mission = pr_sat_to_do_mission
        self.__f_mission_weight = f_mission_weight
        self.__randomize_mission_cost = randomize_mission_cost
        self.__t_end = t_end
        self.__f_download = f_download


    def generate_scenario(self) -> (Dict[(int, List[int])], [int], [int]):
        '''

        :return: Sat -> [(t, mission)] it is able to handle and at which time, mission -> required mission storage, sat -> number of assigned missions in system before download
        '''
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in range(self._mission_num):
                if self._random.random() < self.__pr_sat_to_do_mission:
                    t = self._random.randint(1, self.__t_end - 1)
                    result[s].append((m, t))

        if self.__randomize_mission_cost:
            return result, self._random.sample(self.__f_mission_weight, len(self.__f_mission_weight)), self.__f_download
        else:
            return result, copy(self.__f_mission_weight), self.__f_download



def generate_f_download(num_of_downloads:int, t_end:int, random:random.Random):
    '''
    Auxiliary function
    Generates a list with a given number of downloads.
    A download list is a set of integer values, each one
    belonging to [1, t_end]
    :param num_of_downloads: >= 1
    :param t_end:
    :return:
    '''
    if num_of_downloads < 1:
        raise ValueError('num_of_downloads must be greater than 1')
    downloads = []
    dt = 0
    for i in range(1, num_of_downloads):
        dt = random.randint(dt + 1, t_end - num_of_downloads + i)
        downloads.append(dt)

    downloads.append(t_end)
    assert [downloads[x] < downloads[x+1] for x in range(len(downloads) - 1)]
    return downloads