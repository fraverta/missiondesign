import unittest
import sys
from functools import reduce
import operator
import sys
from random import Random
sys.path.insert(0,'../')
from imp_13_8_19 import  MILP, UserScenarioGenerator, MBP, LBB, FirstFit, FirstFit, BestFit, RandomFit, generate_f_download


class Test_19_7_19(unittest.TestCase):


    '''
        TEST SCENARIO GENERATORS
    '''
    def test_UserScenarioGenerator_same_seed_generates_same_missions(self):
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(100)]
        mgen1 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], 100, f_dowload_time, seed=10)
        mgen2 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], 100, f_dowload_time, seed=10)

        mgen3 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], 100, f_dowload_time, seed=10, randomize_mission_cost=True)
        mgen4 = UserScenarioGenerator(10, 100, 0.5, [x for x in range(100, 0, -1)], 100, f_dowload_time, seed=10, randomize_mission_cost=True)

        self.assertEqual([mgen1.generate_scenario() for i in range(10)], [mgen2.generate_scenario() for i in range(10)])
        self.assertEqual([mgen3.generate_scenario() for i in range(10)], [mgen4.generate_scenario() for i in range(10)])
        self.assertFalse([mgen1.generate_scenario() for i in range(10)] == [mgen3.generate_scenario() for i in range(10)])

    def test_UserScenarioGenerator1(self):
        sat_num = 100; mission_num = 10000; end_time = 100; random = Random(10);
        f_weight_original = [x for x in range(100, 0, -1)]
        f_dowload_time_original = [[random.randint(1, 99), 100] for sat in range(100)]

        mgen1 = UserScenarioGenerator(sat_num, mission_num, 0.5, f_weight_original, end_time, f_dowload_time_original, seed=10)
        scenario, f_weight, f_dowload_time = mgen1.generate_scenario()
        self.assertEqual(f_weight, f_weight_original)
        self.assertEqual(f_dowload_time, f_dowload_time_original)

        for sat in range(sat_num):
               for m, t in scenario[sat]:
                   self.assertTrue(0 <= m < mission_num, "Valid mission identifier")
                   self.assertTrue( 0 <= t < end_time, "Valid missions time")

    '''
        TEST MILP
    '''
    def test_milp_assignement1(self):
        sat_num = 2; mission_num = 8; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 2; f_weight = [1] * mission_num; sat_download_times = {0: [1,2], 1: [1,2]}
        milp = MILP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,0), (2,0), (3,0), (6,1), (7,1)], 1: [(4,0), (5,0), (6,0), (7,0), (2,1), (3,1)]}
        expected_assigned_missions = {0: {1:[0,1], 2:[6,7]}, 1:{1:[4,5], 2:[2,3]}}
        assigned_capacity, mission_assignation = milp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(8, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_milp_assignement2(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        milp = MILP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 1), (9, 0), (4, 1), (10, 3), (11, 2), (6, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [6, 7]},
                                        2: {2: [8, 9], 4: [10, 11]},
                                        3: {1: [12], 4: [13]}
                                  }
        assigned_capacity, mission_assignation = milp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(16, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_milp_assignement3(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        milp = MILP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 1), (9, 0), (4, 1), (11, 2), (6, 3), (13, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [6, 7]},
                                        2: {2: [8, 9], 4: [13]},
                                        3: {1: [12], 4: [10, 11]}
                                  }
        assigned_capacity, mission_assignation = milp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(16, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_milp_assignement4(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2; pr_sat_to_do_mission = 0.; t_end = 4;
        f_weight = [1] * 12 + [2, 2];
        sat_download_times = {0: [2, 4], 1: [1, 4], 2: [2, 4], 3: [1, 4]}
        milp = MILP(sat_num, sat_capacity, None)
        random = Random(0)
        scenario = {0: [(0, 0), (1, 1), (12, 0), (2, 2), (3, 3), (13, 3)],
                    1: [(4, 0), (5, 0), (12, 0), (6, 1), (7, 2), (13, 3)],
                    2: [(8, 1), (9, 0), (4, 1), (11, 2), (6, 3), (13, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        for sat in range(sat_num):
            random.shuffle(scenario[sat])

        expected_assigned_missions = {
            0: {2: [0, 1], 4: [2, 3]},
            1: {1: [4, 5], 4: [6, 7]},
            2: {2: [8, 9], 4: [13]},
            3: {1: [12], 4: [10, 11]}
        }
        assigned_capacity, mission_assignation = milp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        for sat in range(sat_num):
            for dt in sat_download_times[sat]:
                mission_assignation[sat][dt].sort()

        self.assertEqual(16, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    '''
        TEST MBP
    '''
    def test_MBP_compute_permanence(self):
        mbp = MBP(0, 0, None)
        self.assertEqual((1, 1), mbp._MBP__compute_permanence(0, [1, 4]))
        self.assertEqual((3, 4), mbp._MBP__compute_permanence(1, [1, 4]))
        self.assertEqual((1, 4), mbp._MBP__compute_permanence(3, [1, 2, 3, 4]))
        self.assertEqual((1, 3), mbp._MBP__compute_permanence(2, [1, 3, 4]))

    def test_MBP_assignement1(self):
        sat_num = 2; mission_num = 8; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 2; f_weight = [1] * mission_num; sat_download_times = {0: [1,2], 1: [1,2]}
        mbp = MBP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,0), (2,0), (3,0), (6,1), (7,1)], 1: [(4,0), (5,0), (6,0), (7,0), (2,1), (3,1)]}
        expected_assigned_missions = {0: {1: [0,1], 2: [6,7]}, 1:{1: [4,5], 2: [2,3]}}
        assigned_capacity, mission_assignation = mbp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(8, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_MBP_assignement2(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        mbp = MBP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [7]},
                                        2: {2: [], 4: [6, 10]},
                                        3: {1: [8,9], 4: [11]}
                                  }
        assigned_capacity, mission_assignation = mbp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(12, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_MBP_assignement3(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        mbp = MBP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [7]},
                                        2: {2: [13], 4: [6, 10]},
                                        3: {1: [8,9], 4: [11]}
                                  }
        assigned_capacity, mission_assignation = mbp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_MBP_assignement4(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        mbp = MBP(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [12]},
                                        2: {2: [13], 4: [6, 10]},
                                        3: {1: [8,9], 4: [7,11]}
                                  }
        assigned_capacity, mission_assignation = mbp._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(16, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    '''
        TEST LBB
    '''
    def test_LBB_assignement1(self):
        sat_num = 2; mission_num = 8; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 2; f_weight = [1] * mission_num; sat_download_times = {0: [1,2], 1: [1,2]}
        lbb = LBB(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,0), (2,0), (3,0), (6,1), (7,1)], 1: [(4,0), (5,0), (6,0), (7,0), (2,1)]}
        expected_assigned_missions = {0: {1: [0,1], 2: [6,7]}, 1:{1: [4], 2: [2]}}
        assigned_capacity, mission_assignation = lbb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        #self.assertEqual(6, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_LBB_assignement2(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        lbb = LBB(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0,1], 4: [2,3]},
                                        1: {1: [4, 5], 4: [7]},
                                        2: {2: [9], 4: [6]},
                                        3: {1: [8], 4: [10,11]}
                                  }
        assigned_capacity, mission_assignation = lbb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(12, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_LBB_assignement3(self):
        sat_num = 4;
        mission_num = 14;
        sat_capacity = 2;
        pr_sat_to_do_mission = 0.;
        t_end = 4;
        f_weight = [1] * 12 + [2, 2];
        sat_download_times = {0: [2, 4], 1: [1, 4], 2: [2, 4], 3: [1, 4]}
        lbb = LBB(sat_num, sat_capacity, None)
        scenario = {
                        0: [(0, 0), (1, 1), (2, 2), (3, 3), (11, 3), (12, 0), (13, 3)],
                        1: [(4, 0), (5, 0), (6, 1), (7, 2), (12, 0), (13, 3)],
                        2: [(5, 0), (6, 3), (8, 0), (9, 0), (10, 3), (11, 2)],
                        3: [(8, 0), (9, 0), (10, 0), (12, 0), (13, 1)]
                    }
        expected_assigned_missions = {
            0: {2: [0, 1], 4: [2, 3]},
            1: {1: [4], 4: [6,7]},
            2: {2: [5,9], 4: [11]},
            3: {1: [8,10], 4: [13]}
        }
        assigned_capacity, mission_assignation = lbb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_LBB_assignement4(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        lbb = LBB(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [12]},
                                        2: {2: [8], 4: [6, 11]},
                                        3: {1: [9], 4: [7,10]}
                                  }
        assigned_capacity, mission_assignation = lbb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    '''
        TEST FirstFit
    '''
    def test_FF_assignement1(self):
        sat_num = 2; mission_num = 8; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 2; f_weight = [1] * mission_num; sat_download_times = {0: [1,2], 1: [1,2]}
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,0), (2,0), (3,0), (6,1), (7,1)], 1: [(4,0), (5,0), (6,0), (7,0), (2,1)]}
        expected_assigned_missions = {0: {1: [0,1], 2: [6,7]}, 1:{1: [4], 2: [2]}}
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
        #self.assertEqual(6, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_FF_assignement2(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0,1], 4: [2,3]},
                                        1: {1: [4, 5], 4: [7]},
                                        2: {2: [8], 4: [6]},
                                        3: {1: [9], 4: [10,11]}
                                  }
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(12, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_FF_assignement3(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2; pr_sat_to_do_mission = 0.; t_end = 4;
        f_weight = [1] * 12 + [2, 2]; sat_download_times = {0: [2, 4], 1: [1, 4], 2: [2, 4], 3: [1, 4]}
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {
                        0: [(0, 0), (1, 1), (2, 2), (3, 3), (11, 3), (12, 0), (13, 3)],
                        1: [(4, 0), (5, 0), (6, 1), (7, 2), (12, 0), (13, 3)],
                        2: [(5, 0), (6, 3), (8, 0), (9, 0), (10, 3), (11, 2)],
                        3: [(8, 0), (9, 0), (10, 0), (12, 0), (13, 1)]
                    }
        expected_assigned_missions = {
            0: {2: [0, 1], 4: [2, 3]},
            1: {1: [4,5], 4: [7]},
            2: {2: [8], 4: [6,11]},
            3: {1: [9,10], 4: [13]}
        }
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_FF_assignement4(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [12]},
                                        2: {2: [8], 4: [6, 11]},
                                        3: {1: [9], 4: [7,10]}
                                  }
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_FF_assignement5(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [12]},
                                        2: {2: [8], 4: [6, 11]},
                                        3: {1: [9], 4: [7,10]}
                                  }
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_FF_assignement6(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2; pr_sat_to_do_mission = 0.; t_end = 4;
        f_weight = [1] * 13 + [2]; sat_download_times = {0: [2, 4], 1: [1, 4], 2: [2, 4], 3: [1, 4]}
        ff = FirstFit(sat_num, sat_capacity, None)
        scenario = {0: [(0, 0), (1, 1), (12, 0), (2, 2), (3, 3), (13, 3)],
                    1: [(4, 0), (5, 0), (6, 1), (7, 2), (12, 1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13, 1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7, 3)]
                    }
        expected_assigned_missions = {
            0: {2: [0, 1], 4: [2, 3]},
            1: {1: [4, 5], 4: [12]},
            2: {2: [8], 4: [6, 11]},
            3: {1: [9], 4: [7, 10]}
        }
        assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(13, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

        def test_FF_assignement7(self):
            sat_num = 4; mission_num = 14; sat_capacity = 2; pr_sat_to_do_mission = 0.; t_end = 4;
            f_weight = [1] * 12 + [2, 2];  sat_download_times = {0: [2, 4], 1: [1, 4], 2: [2, 4], 3: [1, 4]}
            ff = FirstFit(sat_num, sat_capacity, None)
            scenario = {0: [(0, 0), (1, 1), (12, 0), (2, 2), (3, 3), (13, 3)],
                        1: [(4, 0), (5, 0), (12, 1), (6, 1), (7, 2), (13, 1), (10, 1)],
                        2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13, 1)],
                        3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7, 3)]
                        }
            expected_assigned_missions = {
                0: {2: [0, 1], 4: [2, 3]},
                1: {1: [4, 5], 4: [10]},
                2: {2: [8], 4: [6]},
                3: {1: [9], 4: [7, 11]}
            }
            assigned_capacity, mission_assignation = ff._compute_mission_assignment(scenario, f_weight, sat_download_times)
            self.assertEqual(12, assigned_capacity, '')
            self.assertEqual(expected_assigned_missions, mission_assignation, '')

    '''
        TEST BestFit
    '''
    def test_BB_assignement1(self):
        sat_num = 2; mission_num = 8; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 2; f_weight = [1] * mission_num; sat_download_times = {0: [1,2], 1: [1,2]}
        bb = BestFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,0), (2,0), (3,0), (6,1), (7,1)], 1: [(4,0), (5,0), (6,0), (7,0), (2,1)]}
        expected_assigned_missions = {0: {1: [0,1], 2: [6,7]}, 1:{1: [4], 2: [2]}}
        assigned_capacity, mission_assignation = bb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        #self.assertEqual(6, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_BB_assignement2(self):
        sat_num = 2; mission_num = 8; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 2; f_weight = [1] * mission_num; sat_download_times = {0: [1,2], 1: [1,2]}
        bb = BestFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,0), (2,0), (3,0), (6,1), (7,1)], 1: [(4,0), (5,0), (6,1), (7,0), (2,1)]}
        expected_assigned_missions = {0: {1: [0,1], 2: [7]}, 1:{1: [4], 2: [2,6]}}
        assigned_capacity, mission_assignation = bb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        #self.assertEqual(6, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_BB_assignement3(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        bb = BestFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,0), (6,1), (7,2), (13,3)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0,1], 4: [2,3]},
                                        1: {1: [4, 5], 4: [7]},
                                        2: {2: [8], 4: [6]},
                                        3: {1: [9], 4: [10,11]}
                                  }
        assigned_capacity, mission_assignation = bb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(12, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_BB_assignement4(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2; pr_sat_to_do_mission = 0.; t_end = 4;
        f_weight = [1] * 12 + [2, 2]; sat_download_times = {0: [2, 4], 1: [1, 4], 2: [2, 4], 3: [1, 4]}
        bb = BestFit(sat_num, sat_capacity, None)
        scenario = {
                        0: [(0, 0), (1, 1), (2, 2), (3, 3), (11, 3), (12, 0), (13, 3)],
                        1: [(4, 0), (5, 0), (6, 1), (7, 2), (12, 0), (13, 3)],
                        2: [(5, 0), (6, 3), (8, 0), (9, 0), (10, 3), (11, 2)],
                        3: [(8, 0), (9, 0), (10, 0), (12, 0), (13, 1)]
                    }
        expected_assigned_missions = {
            0: {2: [0, 1], 4: [2, 3]},
            1: {1: [4, 5], 4: [7]},
            2: {2: [8], 4: [6, 11]},
            3: {1: [9, 10], 4: [13]}
        }
        assigned_capacity, mission_assignation = bb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_BB_assignement5(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        bb = BestFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [12]},
                                        2: {2: [8], 4: [6, 11]},
                                        3: {1: [9], 4: [7,10]}
                                  }
        assigned_capacity, mission_assignation = bb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')

    def test_BB_assignement6(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        bb = BestFit(sat_num, sat_capacity, None)
        scenario = {0: [(0,0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1), (10,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        expected_assigned_missions = {
                                        0: {2: [0, 1], 4: [2, 3]},
                                        1: {1: [4, 5], 4: [12]},
                                        2: {2: [8], 4: [6, 11]},
                                        3: {1: [9], 4: [7,10]}
                                  }
        assigned_capacity, mission_assignation = bb._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(14, assigned_capacity, '')
        self.assertEqual(expected_assigned_missions, mission_assignation, '')


    '''
        TEST RandomFit
    '''
    def test_RF_multipleRF_same_results(self):
        sat_num = 4; mission_num = 14; sat_capacity = 2;  pr_sat_to_do_mission = 0.; t_end = 4; f_weight = [1] * 12 + [2,2];
        sat_download_times = {0: [2, 4], 1: [1,4], 2: [2,4], 3: [1,4]}
        rf1 = RandomFit(sat_num, sat_capacity, None, seed=10)
        rf2 = RandomFit(sat_num, sat_capacity, None, seed=10)
        scenario = {0: [(0, 0), (1,1), (12,0), (2,2), (3,3), (13,3)],
                    1: [(4,0), (5,0), (12,1), (6,1), (7,2), (13,1), (10,1)],
                    2: [(8, 0), (9, 0), (4, 0), (10, 3), (11, 2), (6, 3), (13,1)],
                    3: [(12, 0), (8, 0), (9, 0), (13, 1), (10, 3), (11, 3), (7,3)]
                    }
        assigned_capacity1, mission_assignation1= rf1._compute_mission_assignment(scenario, f_weight, sat_download_times)
        assigned_capacity2, mission_assignation2 = rf2._compute_mission_assignment(scenario, f_weight, sat_download_times)
        self.assertEqual(assigned_capacity1, assigned_capacity2)
        self.assertEqual(assigned_capacity1, assigned_capacity2)
        for sat in range(sat_num):
            self.assertEqual(list(mission_assignation1[sat].keys()), sat_download_times[sat])
            for dt in sat_download_times[sat]:
                assigned_capacity = 0
                for m in mission_assignation1[sat][dt]:
                    self.assertTrue(len([ddt for mm, ddt in scenario[sat] if m == mm and ddt < dt]) > 0, f'[Fail info]: Sat {sat} - Mission {m} - dt {dt}')
                    assigned_capacity += f_weight[m]
                self.assertLessEqual( assigned_capacity, sat_capacity)

    '''
    Integration Tests
    
    Checks that calling Algorithm.run_experiment gives the same result than calling compute_mission_assignement
    '''

    def test_MILP_integration(self):
        sat_num = 10; mission_num = 100; pr_sat_to_do_mission = .2; t_end=100; sat_capacity = 10; number_of_runs=10
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(sat_num)]
        mgen1 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)

        mgen2 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)
        milp = MILP(sat_num, sat_capacity, None)

        res1 = MILP.run_experiment(sat_num, sat_capacity, mgen1, number_of_runs)
        res2 = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = mgen2.generate_scenario()
            res2.append(milp._compute_mission_assignment(scenario, mission_weight, f_dowload_time)[0])

        self.assertEqual(len(res1), number_of_runs)
        self.assertEqual(res1, res2)

    def test_LBB_integration(self):
        sat_num = 10; mission_num = 100; pr_sat_to_do_mission = .2; t_end=100; sat_capacity = 10; number_of_runs=10
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(sat_num)]
        mgen1 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)

        mgen2 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)
        lbb = LBB(sat_num, sat_capacity, None)

        res1 = LBB.run_experiment(sat_num, sat_capacity,mgen1, number_of_runs)
        res2 = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = mgen2.generate_scenario()
            res2.append(lbb._compute_mission_assignment(scenario, mission_weight, f_dowload_time)[0])

        self.assertEqual(len(res1), number_of_runs)
        self.assertEqual(res1, res2)

    def test_MBP_integration(self):
        sat_num = 10; mission_num = 100; pr_sat_to_do_mission = .2; t_end=100; sat_capacity = 10; number_of_runs=10
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(sat_num)]
        mgen1 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)

        mgen2 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)
        mbp = MBP(sat_num, sat_capacity, None)

        res1 = MBP.run_experiment(sat_num, sat_capacity,mgen1, number_of_runs)
        res2 = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = mgen2.generate_scenario()
            res2.append(mbp._compute_mission_assignment(scenario, mission_weight, f_dowload_time)[0])

        self.assertEqual(len(res1), number_of_runs)
        self.assertEqual(res1, res2)

    def test_FF_integration(self):
        sat_num = 10; mission_num = 100; pr_sat_to_do_mission = .2; t_end=100; sat_capacity = 10; number_of_runs=10
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(sat_num)]
        mgen1 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)

        mgen2 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)
        ff = FirstFit(sat_num, sat_capacity, None)

        res1 = FirstFit.run_experiment(sat_num, sat_capacity,mgen1, number_of_runs)
        res2 = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = mgen2.generate_scenario()
            res2.append(ff._compute_mission_assignment(scenario, mission_weight, f_dowload_time)[0])

        self.assertEqual(len(res1), number_of_runs)
        self.assertEqual(res1, res2)

    def test_BF_integration(self):
        sat_num = 10; mission_num = 100; pr_sat_to_do_mission = .2; t_end=100; sat_capacity = 10; number_of_runs=10
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(sat_num)]
        mgen1 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)

        mgen2 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)
        bf = BestFit(sat_num, sat_capacity, None)

        res1 = BestFit.run_experiment(sat_num, sat_capacity,mgen1, number_of_runs)
        res2 = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = mgen2.generate_scenario()
            res2.append(bf._compute_mission_assignment(scenario, mission_weight, f_dowload_time)[0])

        self.assertEqual(len(res1), number_of_runs)
        self.assertEqual(res1, res2)

    def test_RF_integration(self):
        sat_num = 10; mission_num = 100; pr_sat_to_do_mission = .2; t_end=100; sat_capacity = 10; number_of_runs=3;
        number_of_runs_per_scenario = 10;
        random = Random(10)
        f_dowload_time = [[random.randint(1, 99), 100] for sat in range(sat_num)]
        mgen1 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)

        mgen2 = UserScenarioGenerator(sat_num, mission_num, pr_sat_to_do_mission, [x for x in range(100, 0, -1)], t_end, f_dowload_time, seed=10)
        rf = RandomFit(sat_num, sat_capacity, None)

        res1 = RandomFit.run_experiment(sat_num, sat_capacity,mgen1, number_of_runs, number_of_runs_per_scenario)
        res2 = []
        for i in range(number_of_runs):
            scenario, mission_weight, f_downloads_times = mgen2.generate_scenario()
            scenario_average = 0
            for rep in range(number_of_runs_per_scenario):
                scenario_average += rf._compute_mission_assignment(scenario, mission_weight, f_dowload_time)[0]
            res2.append(scenario_average / number_of_runs_per_scenario)

        self.assertEqual(len(res1), number_of_runs)
        self.assertEqual(res1, res2)


    def test_generate_f_download1(self):
        random1 = Random(10); random2 = Random(10); sat_num = 3; num_of_downloads = 20; t_end = 100;
        f_download1 = [generate_f_download(num_of_downloads, t_end, random1) for sat in range(sat_num)]
        f_download2 = [generate_f_download(num_of_downloads, t_end, random2) for sat in range(sat_num)]

        self.assertEqual(f_download1, f_download2)
        for sat in range(sat_num):
            self.assertEqual(len(f_download1[sat]), num_of_downloads)
            self.assertEqual(f_download1[sat][num_of_downloads - 1], t_end)
            self.assertTrue(f_download1[sat][i] < f_download1[sat][i+1] for i in range(num_of_downloads - 1))

    def test_generate_f_download2(self):
        random = Random(10); sat_num = 10; num_of_downloads = 100; t_end = 100;
        f_download = [generate_f_download(num_of_downloads, t_end, random) for sat in range(sat_num)]

        for sat in range(sat_num):
            self.assertEqual(len(f_download[sat]), num_of_downloads)
            self.assertEqual(f_download[sat][num_of_downloads - 1], t_end)
            self.assertTrue(f_download[sat][i] < f_download[sat][i+1] for i in range(num_of_downloads - 1))
