import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from varying_mission_size.imp_19_7_19 import OnlineRandomAssignment, Offline_LP, Online, FirstFit, BestFit, ExtremmeMissionGenerator
import time
import random
from fractions import gcd
start_time = time.time()


OUTPUT_DIR = 'results-%s-%s'%(os.path.basename(__file__), date.today().strftime("%d-%m-%Y"))
SAT_NUM_RANGE = [10, 50, 100]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
SCENARIO_KEYS = ['prop_B', 'prop_D']
SCENARIO_PROPORTION = [(0.5, 0.5), (.9,.1), (.1,.9), (.7,.3), (.3,.7)]
REPETITIONS = 35
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + OUTPUT_DIR + '/png')
os.system('mkdir ' + OUTPUT_DIR + '/svg')
os.system('mkdir ' + OUTPUT_DIR + '/txt')
for mission_proportion in SCENARIO_PROPORTION:
    mission_proportion_case = dict(zip(SCENARIO_KEYS, mission_proportion))
    for sat_num in SAT_NUM_RANGE:
        system_capacity = sat_num * SAT_CAPACITY
        results_offline_lp = []
        results_online_greedy = []
        results_online_random = []
        results_firstfit = []
        results_bestfit = []
        for offered_load_prop in MISSION_PROPORTION:
            print("[Running] sat_num:%d,sat_capacity:%d,offeredload:%.2f,%s:%.2f,%s:%.2f,rep:%d " % (sat_num, SAT_CAPACITY, offered_load_prop, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS))
            offered_load = system_capacity * offered_load_prop

            scenario_generator = ExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            results_offline_lp.append(Offline_LP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

            scenario_generator = ExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            results_online_greedy.append(Online.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

            scenario_generator = ExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            results_online_random.append(OnlineRandomAssignment.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, RUNS_PER_SCENARIO, seed= SEED + 10))

            scenario_generator = ExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            results_firstfit.append(FirstFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

            scenario_generator = ExtremmeMissionGenerator(sat_num, SAT_CAPACITY, offered_load, seed=SEED, **mission_proportion_case)
            results_bestfit.append(BestFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))


        lprom_offline_lp = [(MISSION_PROPORTION[i], average(results_offline_lp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        lprom_online_greedy = [(MISSION_PROPORTION[i], average(results_online_greedy[i])/ system_capacity) for i in range(len(MISSION_PROPORTION))]
        lprom_online_random =[(MISSION_PROPORTION[i], average(results_online_random[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        lprom_firstfit = [(MISSION_PROPORTION[i], average(results_firstfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
        lprom_bestfit = [(MISSION_PROPORTION[i], average(results_bestfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]

        fname = 'Exp-OfflineLP-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS, SEED)
        print_str_to_file(str(lprom_offline_lp), os.path.join(OUTPUT_DIR, 'txt' , fname + '.txt'))
        #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')


        fname = 'Exp-OnlineGreedy-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS, SEED)
        print_str_to_file(str(lprom_online_greedy), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')

        fname = 'Exp-OnlineRandom-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS, SEED)
        print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')

        fname = 'Exp-firstfit-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS, SEED)
        print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')

        fname = 'Exp-bestfit-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS, SEED)
        print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
        #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')


        fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,%s:%.2f,%s:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, SCENARIO_KEYS[0], mission_proportion[0], SCENARIO_KEYS[1], mission_proportion[1], REPETITIONS, SEED)
        plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
                        labels=['Offline LP assignation', 'Online Greedy assignation', 'Online Random assignation', 'FirstFit', 'BestFit'],
                        xlabel='Offered Load',
                        ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='png')
        plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
                        labels=['Offline LP assignation', 'Online Greedy assignation', 'Online Random assignation', 'FirstFit', 'BestFit'],
                        xlabel='Offered Load',
                        ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg')


print("Elapsed time: %f seconds"%(time.time() - start_time))
