from typing import Dict, List
import random
import networkx as nx
from abc import abstractmethod
from pulp import *
import time
from copy import copy


class Scheme_19_7_19:

    def __init__(self, sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator'):

        self._sat_num = sat_num
        self._sat_capacity = sat_capacity
        self._scenario_generator = scenario_generator

    # def _generate_scenario(self) -> Dict[(int, List[int])]:
    #     '''
    #
    #     :return: Sat -> missions it is able to handle
    #     '''
    #     result = {}
    #     for s in range(self._sat_num):
    #         result[s] = []
    #         for m in range(self._mission_num):
    #             if self._random.random() < self._pr_sat_to_do_mission:
    #                 result[s].append(m)
    #
    #     self.__randomize_f_mission_cost()
    #     return result

    @abstractmethod
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight:List[int]) -> int:
        pass

    @staticmethod
    @abstractmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator, number_of_runs, seed=0) -> List[int]:
        pass

    @staticmethod
    def _run_experiment(sat_num: int, sat_capacity: int, number_of_runs, exp: 'Scheme_19_7_19') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario, mission_weight = exp._scenario_generator.generate_scenario()
            result.append(exp._compute_mission_assignment(scenario, mission_weight)[0])

        return result


class Online(Scheme_19_7_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight:List[int]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(len(mission_weight)):
            m_weight = mission_weight[m]
            sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1], reverse=True)
            for sat, remained_capacity in sat_ordered_by_busy_buffer:
                if remained_capacity - mission_weight[m] >= 0 and m in scenario[sat]:
                    sat_assignment[sat]['assigned_missions'].append(m)
                    sat_assignment[sat]['remained_capacity'] -=m_weight
                    assigned_capacity += m_weight
                    break

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("Online_Assignment:_compute_mission_assignment it takes %.2f - Sat:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = Online(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(sat_num, sat_capacity, number_of_runs, exp)


class OnlineRandomAssignment(Scheme_19_7_19):

    def __init__(self, sat_num, mission_num, scenario_generator, seed: int = 0):
        super(OnlineRandomAssignment, self).__init__(sat_num, mission_num, scenario_generator)
        self.__random = random.Random(seed) # to choose candidates at random

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int]) -> int:
        start_time = time.time()
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(len(mission_weight)):
            m_weight = mission_weight[m]
            eligible_sats = [sat for sat in range(self._sat_num) if sat_assignment[sat]['remained_capacity'] >= m_weight and m in scenario[sat]]

            if len(eligible_sats) > 0:
                sat = self.__random.choice(eligible_sats)
                sat_assignment[sat]['assigned_missions'].append(m)
                sat_assignment[sat]['remained_capacity'] -= m_weight
                assigned_capacity += m_weight

        assert all(sum([mission_weight[m] for m in sat_assignment[k]['assigned_missions']]) <= self._sat_capacity for k in range(self._sat_num)), 'Each satellite must have atmost %d missions assigned' % (self._sat_capacity)
        assert all(m in scenario[sat] for sat in range(self._sat_num) for m in sat_assignment[sat]['assigned_missions']), 'Each satellite must be able to perform an assigned mission in current scenario'

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("Online_Random_Assignment:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d"%(elapsed_time, self._sat_num, len(mission_weight), self._sat_capacity))

        return assigned_capacity, res

    @staticmethod
    def _run_experiment(number_of_runs: int, number_of_runs_per_scenario: int, exp: 'OnlineRandomAssignment') -> List[int]:
        result = []
        for i in range(number_of_runs):
            scenario, mission_weights = exp._scenario_generator.generate_scenario()
            scenario_result = 0.
            for j in range(number_of_runs_per_scenario):
                scenario_result += exp._compute_mission_assignment(scenario, mission_weights)[0]
            result.append(scenario_result / number_of_runs_per_scenario)

        return result

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int, runs_per_scenario:int, seed=0) -> List[int]:
        exp = OnlineRandomAssignment(sat_num, sat_capacity, scenario_generator, seed=seed)
        return exp._run_experiment(number_of_runs, runs_per_scenario, exp)


class Offline_LP(Scheme_19_7_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int]) -> int:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_mission_idx = ['Sat%d_M%d' % (sat, m) for sat in range(self._sat_num) for m in scenario[sat]]

        lp_problem = LpProblem("The Mission Problem", LpMaximize)
        sat_capacity_vars = LpVariable.dicts("S_Sat", list(range(self._sat_num)), 0, self._sat_capacity, LpInteger)
        sat_mission_vars = LpVariable.dicts("", sat_mission_idx, cat=LpBinary)
        mission_vars = LpVariable.dicts("M", list(range(mission_num)), cat=LpBinary)

        lp_problem += lpSum([ mission_weight[m] * mission_vars[m] for m in range(mission_num)]), "Total assigned capacity"
        for sat in range(self._sat_num):
            lp_problem += sat_capacity_vars[sat] == lpSum([mission_weight[m] * sat_mission_vars['Sat%d_M%d'%(sat, m)] for m in scenario[sat]]), 'Capacity assigned Sat %d equal to mission assigned'%(sat)
        for m in range(mission_num):
            sat_able_to_fulfill_m = [sat for sat in range(self._sat_num) if m in scenario[sat]]
            lp_problem += mission_vars[m] == lpSum([sat_mission_vars['Sat%d_M%d'%(sat, m)] for sat in sat_able_to_fulfill_m]), 'Capacity assigned equal to mission %d assigned'%(m)

        lp_problem.solve(CPLEX())
        assert lp_problem.status == LpStatusOptimal

        sat_assignment = dict((sat, []) for sat in range(self._sat_num))
        for v in sat_mission_vars.values():
            if v.varValue:
                sat = int(v.name[4: v.name.rindex('_')])
                mission = int(v.name[v.name.rindex('M') + 1:])
                sat_assignment[sat].append(mission)

        elapsed_time = time.time() - start_time
        print("Offline_LP:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return value(lp_problem.objective), sat_assignment

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = Offline_LP(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(sat_num, sat_capacity, number_of_runs, exp)


class FirstFit(Scheme_19_7_19):

    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int]) -> int:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(mission_num):
            m_weight = mission_weight[m]
            for sat in range(self._sat_num):
                if sat_assignment[sat]['remained_capacity'] - m_weight >= 0 and m in scenario[sat]:
                    sat_assignment[sat]['assigned_missions'].append(m)
                    sat_assignment[sat]['remained_capacity'] -= m_weight
                    assigned_capacity += m_weight
                    break

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = FirstFit(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(sat_num, sat_capacity, number_of_runs, exp)



class BestFit(Scheme_19_7_19):
    def _compute_mission_assignment(self, scenario: Dict[(int, List[int])], mission_weight: List[int]) -> int:
        start_time = time.time()
        mission_num = len(mission_weight)
        sat_assignment = dict((sat, {'remained_capacity': self._sat_capacity, 'assigned_missions': []}) for sat in range(self._sat_num))
        assigned_capacity = 0
        for m in range(mission_num):
            m_weight = mission_weight[m]
            sat_ordered_by_busy_buffer = sorted([(sat, sat_assignment[sat]['remained_capacity']) for sat in range(self._sat_num)], key=lambda x: x[1])
            for sat, remained_capacity in sat_ordered_by_busy_buffer:
                if remained_capacity - m_weight >= 0 and m in scenario[sat]:
                    sat_assignment[sat]['assigned_missions'].append(m)
                    sat_assignment[sat]['remained_capacity'] -= m_weight
                    assigned_capacity += m_weight
                    break

        res = {}
        for sat_id in range(self._sat_num):
            res[sat_id] = sat_assignment[sat_id]['assigned_missions']

        elapsed_time = time.time() - start_time
        print("FirstFit:_compute_mission_assignment it takes %.2f - Sat:%d-Missions:%d-SatCapacity:%d" % (elapsed_time, self._sat_num, mission_num, self._sat_capacity))
        return assigned_capacity, res

    @staticmethod
    def run_experiment(sat_num: int, sat_capacity: int, scenario_generator:'ScenarioGenerator', number_of_runs:int) -> List[int]:
        exp = BestFit(sat_num, sat_capacity, scenario_generator)
        return exp._run_experiment(sat_num, sat_capacity, number_of_runs, exp)

class ScenarioGenerator():
    def __init__(self, sat_num, mission_num, seed=0):
        self._sat_num = sat_num
        self._mission_num = mission_num
        self._random = random.Random(seed)

class GenericScenarioGenerator(ScenarioGenerator):
    '''
    Generate random scenarios in which missions volumes are divided into a given number of categories C (ie. 1, 2, .., C)
    Satellites are able to perform a mission with a given probability


    '''

    def __init__(self, sat_num, mission_num, pr_sat_to_do_mission: float, mission_categories_num: int, randomize_mission_cost = False, seed=0):
        super(GenericScenarioGenerator, self).__init__(sat_num, mission_num, seed)
        self.__pr_sat_to_do_mission = pr_sat_to_do_mission
        self.__mission_categories_num = mission_categories_num

        self.__f_mission_weight = [m % self.__mission_categories_num + 1 for m in range(self._mission_num)]
        self.__randomize_mission_cost = randomize_mission_cost

    def generate_scenario(self) -> (Dict[(int, List[int])], [int]):
        '''

        :return: Sat -> missions it is able to handle, mission -> required mission storage
        '''
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in range(self._mission_num):
                if self._random.random() < self.__pr_sat_to_do_mission:
                    result[s].append(m)

        if self.__randomize_mission_cost:
            return result, self._random.sample(self.__f_mission_weight, len(self.__f_mission_weight))
        else:
            return result, copy(self.__f_mission_weight)


class UserScenarioGenerator(ScenarioGenerator):
    '''
        Genate random scenarios in which cost are provided by the user class
    '''

    def __init__(self, sat_num, mission_num, pr_sat_to_do_mission: float, f_mission_weight, randomize_mission_cost=False, seed=0):
        super(UserScenarioGenerator, self).__init__(sat_num, mission_num, seed)
        self.__pr_sat_to_do_mission = pr_sat_to_do_mission
        self.__f_mission_weight = f_mission_weight
        self.__randomize_mission_cost = randomize_mission_cost

    def generate_scenario(self) -> (Dict[(int, List[int])], [int]):
        '''

        :return: Sat -> missions it is able to handle, mission -> required mission storage
        '''
        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for m in range(self._mission_num):
                if self._random.random() < self.__pr_sat_to_do_mission:
                    result[s].append(m)

        if self.__randomize_mission_cost:
            return result, self._random.sample(self.__f_mission_weight, len(self.__f_mission_weight))
        else:
            return result, copy(self.__f_mission_weight)


class ExtremmeMissionGenerator(ScenarioGenerator):
    '''
        X axis -> Storage (from 1 to sat_capacity)
        Y axis -> How probablier is finding a satellite which can perform the mission

        We have 4 quadrant numerying in the counter clockwise
            1 -> High Storage, Easy Missions
            2 -> Low Storage, Easy Missions
            3 -> Low Storage, Hard Missions
            4 -> High Storage, Hard Missions

        We define for each cuadrant an extremme point which highlight the features of the quadrant.
        Again, counter clockwise:
            A -> Storage = Sat capacity - Probability = 0.9
            B -> Storage = 1 - Probability = 0.9
            C -> Storage = 1 - Probability = 0.1
            D -> Storage = Sat capacity - Probability = 0.1

    '''

    def __init__(self, sat_num, sat_capacity:int, volumen_to_generate:int, prop_A:float=0, prop_B:float=0, prop_C:float=0, prop_D:float=0, seed=0):
        assert round(prop_A + prop_B + prop_C + prop_D, 2) == 1, 'Mission proportion must sum 1'
        super(ExtremmeMissionGenerator, self).__init__(sat_num, 0, seed)

        self.__volumen_to_generate = volumen_to_generate
        self.__sat_capacity = sat_capacity
        self.__propA = prop_A
        self.__propB = prop_B
        self.__propC = prop_C
        self.__propD = prop_D

    def generate_scenario(self) -> (Dict[(int, List[int])], [int]):
        '''

        :return: Sat -> missions it is able to handle, mission -> required mission storage
        '''
        max_mission_capacity = max(i for i in range(self.__sat_capacity, 0, -1) if round(self.__volumen_to_generate * self.__propA) % i == 0)
        missions = [{'pr': 0.9, 'size': max_mission_capacity} for i in range(round(self.__volumen_to_generate * self.__propA) // max_mission_capacity)]
        missions += [{'pr': 0.9, 'size': 1} for i in range(round(self.__volumen_to_generate * self.__propB))]
        missions += [{'pr': 0.1, 'size': 1} for i in range(round(self.__volumen_to_generate * self.__propC))]
        max_mission_capacity = max(i for i in range(self.__sat_capacity, 0, -1) if round(self.__volumen_to_generate * self.__propD) % i == 0)
        missions += [{'pr': 0.1, 'size': max_mission_capacity} for i in range(round(self.__volumen_to_generate * self.__propD) // max_mission_capacity)]

        missions = self._random.sample(missions, len(missions))

        result = {}
        for s in range(self._sat_num):
            result[s] = []
            for i, m in enumerate(missions):
                if self._random.random() < m['pr']:
                    result[s].append(i)

        return result, [m['size'] for m in missions]


