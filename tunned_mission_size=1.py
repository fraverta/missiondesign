import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from imp_9_9_19 import  MILP, UserScenarioGenerator, MBP, LBB, FirstFit, FirstFit, BestFit, RandomFit, generate_f_download, ExperimentResult, ScenarioGenerator, Scheme_13_8_19, MBRT, MDT, SortedMBRT, SortedMDT, SortedMBP, ReverseSortedMBP, ReverseSortedMBRT, ReverseSortedMDT, MO_MILP_1,MO_MILP_2,  MO_MBRT_MBP_MDT, MO_MBRT_MDT_MBP, TableAssigner
from imp_9_9_19 import _MILP, _MBP, _LBB, _FF, _BF, _RF, _MBRT, _MDT, _S_MBT, _RS_MBT, _S_MBRT, _RS_MBRT, _S_MDT, _RS_MDT, _MO_MILP_1, _MO_MILP_2, _MO_MBRT_1, _MO_MBRT_2, _TA
import time
from random import Random
from typing import List

start_time = time.time()

# def run_experiment(generator:ScenarioGenerator, algorithm:Scheme_13_8_19, sat_num:int, sat_capacity:int, repetitions:int) -> ExperimentResult:
#     return algorithm.run_experiment(sat_num, sat_capacity, generator, repetitions)

def compute_metrics(results:List[ExperimentResult]):
    m1 = [(MISSION_PROPORTION[i], results[i].f_assigned_capacity / system_capacity) for i in range(len(MISSION_PROPORTION))]
    m2 = [(MISSION_PROPORTION[i], results[i].f_data_adquisition_until_download) for i in range(len(MISSION_PROPORTION))]
    m3 = [(MISSION_PROPORTION[i], results[i].f_buffer_booking_until_download) for i in range(len(MISSION_PROPORTION))]
    m4 = [(MISSION_PROPORTION[i], results[i].f_delivery_time) for i in range(len(MISSION_PROPORTION))]

    return [m1, m2, m3, m4]


ALGORITHMS = []
SAT_NUM_RANGE = [10, 25, 50]
PR_SAT_DO_A_MISISON_RANGE = [0.1, 0.5, 0.9, 1.] #[0.1, 0.5, 0.9]
SAT_CAPACITY = 1
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
REPETITIONS = 25
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 5 #VALID FOR RANDOM ASSIGNMENT
NUM_OF_DOWNLOADS = int(sys.argv[1])
T_END_RANGE = [100]
random = Random(10)

TA_COEF = [{'w_brt':.5, 'w_info_age':.25, 'w_info_delay':.25}, {'w_brt':.5, 'w_info_age':.3, 'w_info_delay':.2}, {'w_brt':.5, 'w_info_age':.4, 'w_info_delay':.1}, {'w_brt':.6, 'w_info_age':.2, 'w_info_delay':.2}, {'w_brt':.6, 'w_info_age':.3, 'w_info_delay':.1}, {'w_brt':.25, 'w_info_age':.5, 'w_info_delay':.25},{'w_brt':.3, 'w_info_age':.5, 'w_info_delay':.2},{'w_brt':.4, 'w_info_age':.5, 'w_info_delay':.1},{'w_brt':.2, 'w_info_age':.6, 'w_info_delay':.2},{'w_brt':.3, 'w_info_age':.6, 'w_info_delay':.1},{'w_brt':.4, 'w_info_age':.4, 'w_info_delay':.1},{'w_brt':.425, 'w_info_age':.425, 'w_info_delay':.5}
]

for t_end in T_END_RANGE:
    OUTPUT_DIR = f'results-new_mission_size=1.py-GUROBI-{date.today().strftime("%d-%m-%Y")}-tend:100-num_of_downloads:{NUM_OF_DOWNLOADS}'#'results-%s-%s-tend:%d-num_of_downloads:%d' % (os.path.basename(__file__), date.today().strftime("%d-%m-%Y"), t_end, NUM_OF_DOWNLOADS)
    os.system('mkdir ' + OUTPUT_DIR)
    os.system('mkdir ' + OUTPUT_DIR + '/png')
    os.system('mkdir ' + OUTPUT_DIR + '/svg')
    os.system('mkdir ' + OUTPUT_DIR + '/txt')
    for pr_sat_to_do_mission in PR_SAT_DO_A_MISISON_RANGE:
        for sat_num in SAT_NUM_RANGE:
            system_capacity = sat_num * NUM_OF_DOWNLOADS * SAT_CAPACITY
            results = {}

            f_download = [generate_f_download(NUM_OF_DOWNLOADS, t_end, random) for sat in range(sat_num)]
            for offered_load_prop in MISSION_PROPORTION:
                print("[Running] tend:%d,sat_num:%d,sat_capacity:%d,offeredload:%.2f,pr:%.2f,rep:%d " % (t_end,sat_num, SAT_CAPACITY, offered_load_prop, pr_sat_to_do_mission, REPETITIONS))
                offered_load = round(system_capacity * offered_load_prop)
                f_mission_weight = [1 for i in range(offered_load)]

                for alg in ALGORITHMS:

                    scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                    results[alg].append(MILP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                
                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mbp.append(MBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                
                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_lbb.append(LBB.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                
                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_ra.append(RandomFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, RUNS_PER_SCENARIO, seed=SEED + 10))
                
                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_ff.append(FirstFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                
                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_bf.append(BestFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mbrt.append(MBRT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mdt.append(MDT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_smbp.append(SortedMBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_smbrt.append(SortedMBRT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                #scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                #results_smdt.append(SortedMDT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_rsmbp.append(ReverseSortedMBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_rsmbrt.append(ReverseSortedMBRT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_rsmdt.append(ReverseSortedMDT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mo_milp_1.append(MO_MILP_1.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mo_milp_2.append(MO_MILP_2.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mbrt_mbp_mdt.append(MO_MBRT_MBP_MDT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mbrt_mdt_mbp.append(MO_MBRT_MDT_MBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_ta.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_0.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[0]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_1.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[1]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_2.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[2]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_3.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[3]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_4.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[4]))

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_5.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[5]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_6.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[6]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_7.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[7]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_8.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[8]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_9.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[9]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_10.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[10]))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_coef_11.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, **TA_COEF[11]))

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ta_ga.append(TableAssigner.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, w_brt=.16, w_info_age=.53, w_info_delay=.31))

            ALGORITHMS = ['MILP', 'MBP', 'MBRT', 'MDT', 'MO_MILP_1', 'MO_MILP_2', 'MO-MBRT-MBP-MDT','MO-MBRT-MDT-MBP', 'TA'] #[f"TA-brt:{coef['w_brt']}-iage:{coef['w_info_age']}-:idelay{coef['w_info_delay']}" for coef in TA_COEF[5:]]#['MILP','MO-MILP-1','MO-MILP-2','MO-MBRT-MBP-MDT','MO-MBRT-MDT-MBP'] #['MILP','MO-MILP-1','MO-MILP-2'] #['MILP', 'MDT', 'S-MDT','RS-MDT'] #['MILP', 'MBRT', 'S-MBRT','RS-MBRT'] #['RS-MBRT', 'RS-MDT']#['MILP','MBP','LBB', 'FF', 'BF', 'MBRT', 'MDT'] #['MILP','MBP','S-MBP', 'RS-MBP'] #['MILP','MBP','LBB', 'FF', 'BF'] #['MILP','MBP','LBB', 'FF', 'BF', 'MBRT', 'MDT', 'SMBP', 'S-MBRT', 'S-MDT'] #['MILP','MBP','LBB', 'FF', 'BF'] #['MBRT', 'MDT', 'SMBP', 'S-MBRT', 'S-MDT'] #['MILP','MBP','LBB', 'FF', 'BF', 'MBRT', 'MDT', 'SLBB', 'S-MBRT', 'S-MDT'] #['MILP','MBP','LBB', 'RA', 'FF', 'BF']
            METRICS = [('served_load', 'Served Load'), ('adquisition_until_dw','Information Age'),('buffer_booking_until_dw', 'Average time of Buffer Booking'), ('del_time','Delivery Time')]
            COLORS = ['red', 'orange', 'blue', 'green', 'magenta', 'olive', 'gray', 'purple', 'black', 'lightgreen'] #['red', 'orange', 'blue', 'lightblue', 'green', 'magenta']
            # #YTICKS = {'served_load':(0.1,1.01,0.2), 'adquisition_until_dw':(0,70,10),'buffer_booking_until_dw':(20,101,20), 'del_time':(50,101,10)}
            YTICKS = {'served_load': (0.1, 1.01, 0.2), 'adquisition_until_dw': (0, 70, 10),'buffer_booking_until_dw': (0, 101, 20), 'del_time': (0, 101, 20)}
            #
            #results_by_algorithm = [[] for alg in ALGORITHMS]
            results_by_algorithm = []
            results_by_algorithm.append(compute_metrics(results_milp))
            results_by_algorithm.append(compute_metrics(results_mbp))
            #results_by_algorithm.append(compute_metrics(results_lbb))
            #results_by_algorithm.append(compute_metrics(results_ra))
            #results_by_algorithm.append(compute_metrics(results_ff))
            #results_by_algorithm.append(compute_metrics(results_bf))
            results_by_algorithm.append(compute_metrics(results_mbrt))
            results_by_algorithm.append(compute_metrics(results_mdt))
            #results_by_algorithm.append(compute_metrics(results_smbp))
            #results_by_algorithm.append(compute_metrics(results_smbrt))
            #results_by_algorithm.append(compute_metrics(results_smdt))
            # results_by_algorithm.append(compute_metrics(results_rsmbp))
            # results_by_algorithm.append(compute_metrics(results_rsmbrt))
            #results_by_algorithm.append(compute_metrics(results_rsmdt))
            results_by_algorithm.append(compute_metrics(results_mo_milp_1))
            results_by_algorithm.append(compute_metrics(results_mo_milp_2))
            results_by_algorithm.append(compute_metrics(results_mbrt_mbp_mdt))
            results_by_algorithm.append(compute_metrics(results_mbrt_mdt_mbp))
            results_by_algorithm.append(compute_metrics(results_ta))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_0))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_1))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_2))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_3))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_4))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_5))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_6))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_7))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_8))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_9))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_10))
            # results_by_algorithm.append(compute_metrics(results_ta_coef_11))

            for a, alogrithm in enumerate(ALGORITHMS):
                for m, metric in enumerate(METRICS):
                    fname = f'Exp-{alogrithm}-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]}'
                    print_str_to_file(str(results_by_algorithm[a][m]), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
                    #results_by_algorithm[a].append(getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt')))



            # for m, metric in enumerate(METRICS):
            #     for format in ['svg', 'png']:
            #         fname = f'Exp-Comparison-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]}'
            #         plot_nfunctions(map(lambda ra: ra[m], results_by_algorithm), os.path.join(OUTPUT_DIR, format, fname),
            #                         labels=ALGORITHMS,
            #                         xlabel='Offered Load',
            #                         ylabel=f'{metric[1]}', colors=COLORS, ftype=format, yticks=YTICKS[metric[0]])
            #
            # for format in ['svg', 'png']:
            #      m=0
            #      metric = ('served_load', 'Served Load')
            #      fname = f'Exp-Comparison-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]},xrange:[0.7:1.0]'
            #      plot_nfunctions(map(lambda ra: ra[m][6:], results_by_algorithm), os.path.join(OUTPUT_DIR, format, fname),
            #                      labels=ALGORITHMS,
            #                      xlabel='Offered Load',
            #                      ylabel=f'{metric[1]}', colors=COLORS, ftype=format, yticks=(0.4,1.05,0.1))


print("Elapsed time: %f seconds"%(time.time() - start_time))

