import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from imp_9_9_19 import  MILP, UserScenarioGenerator, MBP, LBB, FirstFit, FirstFit, BestFit, RandomFit, generate_f_download, ExperimentResult, ScenarioGenerator, Scheme_13_8_19, MBRT, MDT, SortedMBRT, SortedMDT, SortedMBP
import time
from random import Random
from typing import  List

start_time = time.time()

# def run_experiment(generator:ScenarioGenerator, algorithm:Scheme_13_8_19, sat_num:int, sat_capacity:int, repetitions:int) -> ExperimentResult:
#     return algorithm.run_experiment(sat_num, sat_capacity, generator, repetitions)

def compute_metrics(results:List[ExperimentResult]):
    m1 = [(MISSION_PROPORTION[i], results[i].f_assigned_capacity / system_capacity) for i in range(len(MISSION_PROPORTION))]
    m2 = [(MISSION_PROPORTION[i], results[i].f_data_adquisition_until_download) for i in range(len(MISSION_PROPORTION))]
    m3 = [(MISSION_PROPORTION[i], results[i].f_buffer_booking_until_download) for i in range(len(MISSION_PROPORTION))]
    m4 = [(MISSION_PROPORTION[i], results[i].f_deliverty_time) for i in range(len(MISSION_PROPORTION))]

    return [m1, m2, m3, m4]



SAT_NUM_RANGE = [5] #[10, 25, 50]
PR_SAT_DO_A_MISISON_RANGE = [0.1] #[0.1, 0.5, 0.9] #[0.1, 0.5, 0.9]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
REPETITIONS = 25
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 5 #VALID FOR RANDOM ASSIGNMENT
NUM_OF_DOWNLOADS = 4
T_END_RANGE = [100]
random = Random(10)


for t_end in T_END_RANGE:
    OUTPUT_DIR = f'results-new_mission_size=1.py-10-09-2019-tend:100-num_of_downloads:{NUM_OF_DOWNLOADS}'#'results-%s-%s-tend:%d-num_of_downloads:%d' % (os.path.basename(__file__), date.today().strftime("%d-%m-%Y"), t_end, NUM_OF_DOWNLOADS)
    os.system('mkdir ' + OUTPUT_DIR)
    os.system('mkdir ' + OUTPUT_DIR + '/png')
    os.system('mkdir ' + OUTPUT_DIR + '/svg')
    os.system('mkdir ' + OUTPUT_DIR + '/txt')
    for pr_sat_to_do_mission in PR_SAT_DO_A_MISISON_RANGE:
        for sat_num in SAT_NUM_RANGE:
            system_capacity = sat_num * NUM_OF_DOWNLOADS * SAT_CAPACITY
            results_milp = []
            results_mbp = []
            results_lbb = []
            results_ra = []
            results_ff = []
            results_bf = []
            results_mbrt = []
            results_mdt = []
            results_smbp = []
            results_smbrt = []
            results_smdt = []
            f_download = [generate_f_download(NUM_OF_DOWNLOADS, t_end, random) for sat in range(sat_num)]
            for offered_load_prop in MISSION_PROPORTION:
                print("[Running] tend:%d,sat_num:%d,sat_capacity:%d,offeredload:%.2f,pr:%.2f,rep:%d " % (t_end,sat_num, SAT_CAPACITY, offered_load_prop, pr_sat_to_do_mission, REPETITIONS))
                offered_load = round(system_capacity * offered_load_prop)
                f_mission_weight = [1 for i in range(offered_load)]

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_milp.append(MILP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_mbp.append(MBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_lbb.append(LBB.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ra.append(RandomFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, RUNS_PER_SCENARIO, seed=SEED + 10))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_ff.append(FirstFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                # results_bf.append(BestFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mbrt.append(MBRT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_mdt.append(MDT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_smbp.append(SortedMBP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_smbrt.append(SortedMBRT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))

                scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, t_end, f_download, randomize_mission_cost=True, seed=SEED)
                results_smdt.append(SortedMDT.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))


            ALGORITHMS = ['MBRT', 'MDT', 'SMBP', 'S-MBRT', 'S-MDT'] #['MILP','MBP','LBB', 'FF', 'BF', 'MBRT', 'MDT', 'SLBB', 'S-MBRT', 'S-MDT'] #['MILP','MBP','LBB', 'RA', 'FF', 'BF']
            METRICS = [('served_load', 'Served Load'), ('adquisition_until_dw','Information Age'),
                       ('buffer_booking_until_dw', 'Average time of Buffer Booking'), ('del_time','Delivery Time')]
            COLORS = ['red', 'orange', 'blue', 'green', 'magenta'] #['red', 'orange', 'blue', 'lightblue', 'green', 'magenta']
            YTICKS = {'served_load':(0.1,1.01,0.2), 'adquisition_until_dw':(0,50,5),'buffer_booking_until_dw':(20,101,20), 'del_time':(50,101,10)}


            #results_by_algorithm = [[] for alg in ALGORITHMS]
            results_by_algorithm = []
            # results_by_algorithm.append(compute_metrics(results_milp))
            # results_by_algorithm.append(compute_metrics(results_mbp))
            # results_by_algorithm.append(compute_metrics(results_lbb))
            # results_by_algorithm.append(compute_metrics(results_ra))
            # results_by_algorithm.append(compute_metrics(results_ff))
            # results_by_algorithm.append(compute_metrics(results_bf))
            results_by_algorithm.append(compute_metrics(results_mbrt))
            results_by_algorithm.append(compute_metrics(results_mdt))
            results_by_algorithm.append(compute_metrics(results_smbp))
            results_by_algorithm.append(compute_metrics(results_smbrt))
            results_by_algorithm.append(compute_metrics(results_smdt))


            for a, alogrithm in enumerate(ALGORITHMS):
                for m, metric in enumerate(METRICS):
                    fname = f'Exp-{alogrithm}-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]}'
                    print_str_to_file(str(results_by_algorithm[a][m]), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
                    #results_by_algorithm[a].append(getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt')))

            for m, metric in enumerate(METRICS):
                for format in ['svg', 'png']:
                    fname = f'Exp-Comparison-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]}'
                    plot_nfunctions(map(lambda ra: ra[m], results_by_algorithm), os.path.join(OUTPUT_DIR, format, fname),
                                    labels=ALGORITHMS,
                                    xlabel='Offered Load',
                                    ylabel=f'{metric[1]}', colors=COLORS, ftype=format, yticks=YTICKS[metric[0]])
            #
            # for format in ['svg', 'png']:
            #     m=0
            #     metric = ('served_load', 'Served Load')
            #     fname = f'Exp-Comparison-sat_num:{sat_num},sat_capacity:{SAT_CAPACITY},pr:{pr_sat_to_do_mission},rep:{REPETITIONS},seed:{SEED},metric:{metric[0]},xrange:[0.7:1.0]'
            #     plot_nfunctions(map(lambda ra: ra[m][6:], results_by_algorithm), os.path.join(OUTPUT_DIR, format, fname),
            #                     labels=ALGORITHMS,
            #                     xlabel='Offered Load',
            #                     ylabel=f'{metric[1]}', colors=COLORS, ftype=format, yticks=(0.4,1.05,0.1))


print("Elapsed time: %f seconds"%(time.time() - start_time))

