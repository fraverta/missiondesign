import  sys
sys.path.insert(0,'../')
from utilities.utils import plot_function, print_str_to_file, average, plot_nfunctions, getListFromFile
import os
from datetime import date
from varying_mission_size.imp_19_7_19 import OnlineRandomAssignment, Offline_LP, Online, FirstFit, BestFit, MissionAwareFit, UserScenarioGenerator
import time
import random
from fractions import gcd
start_time = time.time()


MISSION_SIZE = range(1,6)
SAT_NUM_RANGE = [10, 25, 50, 75, 100]
PR_SAT_DO_A_MISISON_RANGE = [0.1, 0.2, 0.5, 0.7, 0.9]
SAT_CAPACITY = 10
MISSION_PROPORTION = [x/100 for x in range(10,110,10)]
REPETITIONS = 35
#SEED = random.randint(0, 1000)
SEED = 245
RUNS_PER_SCENARIO = 10 #VALID FOR RANDOM ASSIGNMENT

OUTPUT_DIR = 'results-%s-%s' % (os.path.basename(__file__), '05-08-2019') #'results-%s-%s' % (os.path.basename(__file__), date.today().strftime("%d-%m-%Y"))

os.system('mkdir ' + OUTPUT_DIR)
os.system('mkdir ' + OUTPUT_DIR + '/png')
os.system('mkdir ' + OUTPUT_DIR + '/svg')
os.system('mkdir ' + OUTPUT_DIR + '/txt')

for pr_sat_to_do_mission in PR_SAT_DO_A_MISISON_RANGE:
    for msize in MISSION_SIZE:
        for sat_num in SAT_NUM_RANGE:
            system_capacity = sat_num * SAT_CAPACITY
            results_offline_lp = []
            results_online_greedy = []
            results_online_random = []
            results_firstfit = []
            results_bestfit = []
            results_mission_aware = []
            for offered_load_prop in MISSION_PROPORTION:
                print("[Running] sat_num:%d,sat_capacity:%d,msize:%d,offeredload:%.2f,pr:%.2f,rep:%d " % (sat_num, SAT_CAPACITY, msize, offered_load_prop, pr_sat_to_do_mission, REPETITIONS))
                offered_load = round((system_capacity * offered_load_prop) // msize)
                f_mission_weight = [ msize for i in range(offered_load)]

                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, randomize_mission_cost=True, seed=SEED)
                # results_offline_lp.append(Offline_LP.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, randomize_mission_cost=True, seed=SEED)
                # results_online_greedy.append(Online.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, randomize_mission_cost=True, seed=SEED)
                # results_online_random.append(OnlineRandomAssignment.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS, RUNS_PER_SCENARIO, seed= SEED + 10))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight, randomize_mission_cost=True, seed=SEED)
                # results_firstfit.append(FirstFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight,randomize_mission_cost=True, seed=SEED)
                # results_bestfit.append(BestFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))
                #
                # scenario_generator = UserScenarioGenerator(sat_num, offered_load, pr_sat_to_do_mission, f_mission_weight,randomize_mission_cost=True, seed=SEED)
                # results_mission_aware.append(MissionAwareFit.run_experiment(sat_num, SAT_CAPACITY, scenario_generator, REPETITIONS))


            # lprom_offline_lp = [(MISSION_PROPORTION[i], average(results_offline_lp[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_online_greedy = [(MISSION_PROPORTION[i], average(results_online_greedy[i])/ system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_online_random =[(MISSION_PROPORTION[i], average(results_online_random[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_firstfit = [(MISSION_PROPORTION[i], average(results_firstfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_bestfit = [(MISSION_PROPORTION[i], average(results_bestfit[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]
            # lprom_mission_aware = [(MISSION_PROPORTION[i], average(results_mission_aware[i]) / system_capacity) for i in range(len(MISSION_PROPORTION))]

            fname = 'Exp-OfflineLP-sat_num:%d,sat_capacity:%d,msize:%d,point:%s,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_offline_lp), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # #plot_function(lprom_offline, os.path.join(OUTPUT_DIR, 'png' , fname), ftype='png')
            lprom_offline_lp = getListFromFile( os.path.join(OUTPUT_DIR, 'txt', fname + '.txt')) [6:]

            fname = 'Exp-OnlineGreedy-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_online_greedy), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            #plot_function(lprom_online, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            lprom_online_greedy = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]

            fname = 'Exp-OnlineRandom-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            # #print_str_to_file(str(lprom_online_random), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # #plot_function(lprom_offlinelp, os.path.join(OUTPUT_DIR, 'png', fname), ftype='png')}
            lprom_online_random = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]

            fname = 'Exp-firstfit-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_firstfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # #plot_function(lprom_firstfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            lprom_firstfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]

            fname = 'Exp-bestfit-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_bestfit), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            lprom_bestfit = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]

            fname = 'Exp-missionAware-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            #print_str_to_file(str(lprom_mission_aware), os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))
            # #plot_function(lprom_bestfit, os.path.join(OUTPUT_DIR, 'png' ,fname), ftype='png')
            lprom_mission_aware = getListFromFile(os.path.join(OUTPUT_DIR, 'txt', fname + '.txt'))[6:]



            fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit, lprom_mission_aware, 'Mission Aware'], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF', 'MA'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta', 'black'], ftype='png', yticks=(0.65,1.06,.05))
            plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit, lprom_mission_aware], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF', 'MA'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta', 'black'], ftype='svg', yticks=(0.65,1.06,.05))

            fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit, lprom_mission_aware, 'Mission Aware'], os.path.join(OUTPUT_DIR, 'png', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF', 'MA'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta', 'black'], ftype='png', yticks=(0.65,1.06,.05))
            plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit, lprom_mission_aware], os.path.join(OUTPUT_DIR, 'svg', fname),
                            labels=['MILP', 'LBB', 'RF', 'FF', 'BF', 'MA'],
                            xlabel='Offered Load',
                            ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta', 'black'], ftype='svg', yticks=(0.65,1.06,.05))

            # fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            # plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
            #                 labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
            #                 xlabel='Offered Load',
            #                 ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='png', yticks=(0.65,1.06,.05))
            # plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_online_random, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
            #                 labels=['MILP', 'LBB', 'RF', 'FF', 'BF'],
            #                 xlabel='Offered Load',
            #                 ylabel='Served Load', colors=['red', 'blue','lightblue', 'green', 'magenta'], ftype='svg', yticks=(0.65,1.06,.05))
            #
            # fname = 'Exp-Comparison-sat_num:%d,sat_capacity:%d,msize:%d,pr:%.2f,rep:%d,seed:%d'%(sat_num, SAT_CAPACITY, msize, pr_sat_to_do_mission, REPETITIONS, SEED)
            # plot_nfunctions([lprom_offline_lp, lprom_online_greedy, lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'png', fname),
            #                 labels=['MILP', 'LBB', 'FF', 'BF'],
            #                 xlabel='Offered Load',
            #                 ylabel='Served Load', colors=['red', 'blue', 'green', 'magenta'], ftype='png', yticks=(0.65,1.06,.05))
            # plot_nfunctions([lprom_offline_lp, lprom_online_greedy,  lprom_firstfit, lprom_bestfit], os.path.join(OUTPUT_DIR, 'svg', fname),
            #                 labels=['MILP', 'LBB', 'FF', 'BF'],
            #                 xlabel='Offered Load',
            #                 ylabel='Served Load', colors=['red', 'blue', 'green', 'magenta'], ftype='svg', yticks=(0.65,1.06,.05))
print("Elapsed time: %f seconds"%(time.time() - start_time))