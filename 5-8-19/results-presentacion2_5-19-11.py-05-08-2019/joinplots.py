import os
from svgtogrid import combine_into_table_and_save

sat_num = 25
files_m1 = [
    #'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.10,rep:35,seed:245.svg'%sat_num,
    'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.20,rep:35,seed:245.svg'%sat_num,
    'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.20,rep:35,seed:245.svg'%sat_num,
    #'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.70,rep:35,seed:245.svg'%sat_num,
    'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.90,rep:35,seed:245.svg'%sat_num,
]

sat_num = 50
files_m1 += [
    #'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.10,rep:35,seed:245.svg'%sat_num,
    'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.20,rep:35,seed:245.svg'%sat_num,
    'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.50,rep:35,seed:245.svg'%sat_num,
    #'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.70,rep:35,seed:245.svg'%sat_num,
    'svg/Exp-Comparison-sat_num:%d,sat_capacity:10,pr:0.90,rep:35,seed:245.svg'%sat_num,
]



os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[25,50],pr:[0.2,0.5,0.9]'
combine_into_table_and_save(2, 3, 0, 0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

