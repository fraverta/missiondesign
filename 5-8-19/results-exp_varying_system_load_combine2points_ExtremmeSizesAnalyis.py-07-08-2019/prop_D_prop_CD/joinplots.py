import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'svg/Exp-Comparison-sat_num:150,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:200,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:250,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:300,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:350,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:400,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:450,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:450,sat_capacity:10,prop_D:0.50,prop_CD:0.50,rep:35,seed:245.svg',
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[150,200,250,300,350,400,450,500],[propD_probCD]'
combine_into_table_and_save(1,8,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
