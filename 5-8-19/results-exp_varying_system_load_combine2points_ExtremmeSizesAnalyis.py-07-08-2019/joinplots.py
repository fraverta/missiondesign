import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'prop_A_prop_B/joinplots/comparison-sat_num:[150,200,250,300,350,400,450,500],[propA_probB].svg',
    'prop_C_prop_D/joinplots/comparison-sat_num:[150,200,250,300,350,400,450,500],[propC_probCD].svg'
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[150,200,250,300,350,400,450,500],[propA_probAB,propC_probD]'
combine_into_table_and_save(2,1,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape  --export-png=%s %s'%(fname + '.png', fname + '.svg'))
