import os
from svgtogrid import combine_into_table_and_save

pr = .2
files_m1 = [
    # 'svg/Exp-Comparison-sat_num:10,sat_capacity:10,msize:1,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:10,sat_capacity:10,msize:2,pr:%.2f,rep:35,seed:245.svg' % pr,
    # # 'svg/Exp-Comparison-sat_num:10,sat_capacity:10,msize:3,pr:%.2f,rep:35,seed:245.svg' % pr,
    # # 'svg/Exp-Comparison-sat_num:10,sat_capacity:10,msize:4,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:10,sat_capacity:10,msize:5,pr:%.2f,rep:35,seed:245.svg' % pr,

    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,msize:1,pr:%.2f,rep:35,seed:245.svg' % pr,
    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,msize:2,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:25,sat_capacity:10,msize:3,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:25,sat_capacity:10,msize:4,pr:%.2f,rep:35,seed:245.svg' % pr,
    'svg/Exp-Comparison-sat_num:25,sat_capacity:10,msize:5,pr:%.2f,rep:35,seed:245.svg' % pr,

    # 'svg/Exp-Comparison-sat_num:50,sat_capacity:10,msize:1,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:50,sat_capacity:10,msize:2,pr:%.2f,rep:35,seed:245.svg' % pr,
    # # 'svg/Exp-Comparison-sat_num:50,sat_capacity:10,msize:3,pr:%.2f,rep:35,seed:245.svg' % pr,
    # # 'svg/Exp-Comparison-sat_num:50,sat_capacity:10,msize:4,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:50,sat_capacity:10,msize:5,pr:%.2f,rep:35,seed:245.svg' % pr,

    # 'svg/Exp-Comparison-sat_num:75,sat_capacity:10,msize:1,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:75,sat_capacity:10,msize:2,pr:%.2f,rep:35,seed:245.svg' % pr,
    # # 'svg/Exp-Comparison-sat_num:75,sat_capacity:10,msize:3,pr:%.2f,rep:35,seed:245.svg' % pr,
    # # 'svg/Exp-Comparison-sat_num:75,sat_capacity:10,msize:4,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:75,sat_capacity:10,msize:5,pr:%.2f,rep:35,seed:245.svg' % pr,
    #
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,msize:1,pr:%.2f,rep:35,seed:245.svg' % pr,
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,msize:2,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:100,sat_capacity:10,msize:3,pr:%.2f,rep:35,seed:245.svg' % pr,
    # 'svg/Exp-Comparison-sat_num:100,sat_capacity:10,msize:4,pr:%.2f,rep:35,seed:245.svg' % pr,
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,msize:5,pr:%.2f,rep:35,seed:245.svg' % pr,
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[25,50,75,100],mission_size:[1,2,3,4,5],pr:%.2f'%pr
combine_into_table_and_save(2, 3, 0, 0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

