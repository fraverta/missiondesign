import os
from svgtogrid import combine_into_table_and_save

files_m1 = [
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.10,prop_D:0.90,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.30,prop_D:0.70,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.70,prop_D:0.30,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.90,prop_D:0.10,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.10,prop_D:0.90,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.30,prop_D:0.70,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.70,prop_D:0.30,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.90,prop_D:0.10,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.10,prop_D:0.90,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.30,prop_D:0.70,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.70,prop_D:0.30,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.90,prop_D:0.10,rep:35,seed:245.svg',
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,50,100],[0.1,0.3,0.5,0.7,0.9]x[propB_prop_D],varyingload'
combine_into_table_and_save(3,5,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))

files_m1 = [
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.10,prop_D:0.90,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:10,sat_capacity:10,prop_B:0.90,prop_D:0.10,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.10,prop_D:0.90,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:50,sat_capacity:10,prop_B:0.90,prop_D:0.10,rep:35,seed:245.svg',

    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.10,prop_D:0.90,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.50,prop_D:0.50,rep:35,seed:245.svg',
    'svg/Exp-Comparison-sat_num:100,sat_capacity:10,prop_B:0.90,prop_D:0.10,rep:35,seed:245.svg',
]

os.makedirs('joinplots', exist_ok=True)
fname = 'joinplots/comparison-sat_num:[10,50,100],[0.1,0.5,0.9]x[propB_prop_D],varyingload'
combine_into_table_and_save(3,3,0,0,files_m1, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))